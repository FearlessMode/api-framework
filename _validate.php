<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// @Note
	//  When needed or ideal we run a _validate file to check for all the classes or functions we need
	//  Specifically this is for performance regarding our classes
	//  This way every time we instantiate our class we don't need a class_exists() check
	//   This way we can do only one check
	// ----------------------------------------------------------------------------------------------------
	
	// Set our various context class variables
	
	$this_class = 'CustomTheme_';
	$backend    = 'Backend_';
	$frontend   = 'Frontend_';
	$global     = 'Global_';
	
	// ----------------------------------------------------------------------------------------------------
	// If our required functions do not exist
	// ----------------------------------------------------------------------------------------------------
	
	if  (!function_exists('api') 
	and (!function_exists('instantiate')) 
	and (!function_exists('is')) 
	and (!function_exists('notice')) 
	and (!function_exists('product')) 
	and (!function_exists('theme_data')) 
	and (!function_exists('theme_status')) 
	and (!function_exists('theme')) 
	and (!function_exists('user')) 
	and (!function_exists('version')) 
	and (!function_exists('this_path')) 
	and (!function_exists('this_url')) 
	
	and (!function_exists('__sfc_is_valid_callback')) 
	and (!function_exists('safe_call')) 
	and (!function_exists('safe_message')) 
	and (!function_exists('safe_fallback')) 
	and (!function_exists('safe_echo')) 
	
	// ----------------------------------------------------------------------------------------------------
	// If our required classes do not exist
	// ----------------------------------------------------------------------------------------------------
	
	// If our global classes do not exist
	
	and (!class_exists($this_class .           '_API')) 
	and (!class_exists($this_class . $global . 'Data_API')) 
	and (!class_exists($this_class . $global . 'Developer_API')) 
	and (!class_exists($this_class . $global . 'Helpers_API')) 
	and (!class_exists($this_class . $global . 'Hooks_API')) 
	and (!class_exists($this_class . $global . 'Is_API')) 
	and (!class_exists($this_class . $global . 'Theme_API')) 
	and (!class_exists($this_class . $global . 'Third_Party_API')) 
	and (!class_exists($this_class . $global . 'User_API')) 
	
	// If our frontend classes do not exist
	
	and (!class_exists($this_class . $frontend . 'Asset_API')) 
	and (!class_exists($this_class . $frontend . 'Content_API')) 
	and (!class_exists($this_class . $frontend . 'Helpers_API')) 
	and (!class_exists($this_class . $frontend . 'Output_API')) 
	and (!class_exists($this_class . $frontend . 'Template_API')) 
	
	// If our backend classes do not exist
	
	and (!class_exists($this_class . $backend . 'Advanced_Array_API')) 
	and (!class_exists($this_class . $backend . 'Helpers_API')) 
	and (!class_exists($this_class . $backend . 'Product_API')) 
	and (!class_exists($this_class . $backend . 'Simple_Array_API'))) {
		
		// ----------------------------------------------------------------------------------------------------
		// Load our this-* function files
		// ----------------------------------------------------------------------------------------------------
		
		require_once('this-path.php');
		require_once('this-url.php');
		
		// ----------------------------------------------------------------------------------------------------
		// Load our _load file which officially starts loading our custom theme
		// ----------------------------------------------------------------------------------------------------
		
		require_once('deliver/setup/_load.php');
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
