<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// ->this_path()
	// @Description
	//  This function 
	// @Note
	// @Usage
	//  this_path();
	//  this_path(__FILE__);
	//  $this->path = this_path(__FILE__);
	// @Reference
	// ----------------------------------------------------------------------------------------------------
	
	function this_path($file_path = '') {
		
		// Set our $this_path variable
		
		$this_path = (!empty($file_path)) ? untrailingslashit(plugin_dir_path($file_path)) : untrailingslashit(plugin_dir_path(__FILE__));
		
		// If our $this_url is not empty let's return it otherwise FALSE
		
		return (!empty($this_path)) ? $this_path : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
