<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// ->this_url()
	// @Description
	//  This function 
	// @Note
	// @Usage
	//  this_url();
	//  this_url(__DIR__);
	//  $this->url = this_url(__DIR__);
	// @Reference
	// ----------------------------------------------------------------------------------------------------
	
	function this_url($dir_path = '') {
		
		// Set our $this_url variable
		
		$this_url = (!empty($dir_path)) ? substr($dir_path, strlen($_SERVER['DOCUMENT_ROOT'])) : substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT']));
		
		// If our $this_url is not empty let's return it otherwise FALSE
		
		return (!empty($this_url)) ? $this_url : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
