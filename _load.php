<?php

// ----------------------------------------------------------------------------------------------------
// Plugin Name: * API Framework
// Plugin URI: 
// Description: <strong>** This is required for your custom theme to work.</strong> This is your theme's API (application programming interface) framework which holds your theme's features and settings. This plugin can only be deactivated when you deactivate your custom theme.
// Author: in-house development.
// Author URI: 
// Network: 
// Version: INF-MST-1.0.0.
// ----------------------------------------------------------------------------------------------------

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// If our get_template() function equals 'custom-theme'
	// @Note
	//  This simply detects that if our custom theme is not active then there is no need to load our plugin's routines
	
	if (get_template() == 'custom-theme') {
		
		// Set our various class variables
		
		$this_class = 'CustomTheme_Plugin';
		
		// If our $this_class variable does not exist
		
		if (!class_exists($this_class)) {
			
			// ----------------------------------------------------------------------------------------------------
			// Create our CustomTheme_Plugin class
			// ----------------------------------------------------------------------------------------------------
			
			class CustomTheme_Plugin {
				
				// ----------------------------------------------------------------------------------------------------
				// ::__construct()
				// @Description
				//  This function is our constructor function that handles auto loading any
				//   functions, variables and constructors upon class instantiation
				// @Reference
				//   http://php.net/manual/en/language.oop5.decon.php
				// ----------------------------------------------------------------------------------------------------
				
				// @Version
				public $func_construct = 1.0;
				public function __construct() {
					
					// Set our $custom_option_theme variable
					
					$custom_option_theme = get_theme_root() . '/custom-option/functions.php';
					
					// If file exists
					
					if (file_exists($custom_option_theme)) {
						
						// Load our $custom_option_theme
						
						require_once($custom_option_theme);
						
						// Load our custom action
						
						do_action('CustomOption');
						
					}
					
					// Set our various filter variables
					// @Todo->double_check_filters_here_may_not_run_in_time_for_theme_based_filters_only_lugin_methods
					
					$action   = apply_filters('CustomTheme-intialize-action',  'plugins_loaded');
					$priority = apply_filters('CustomTheme-intialize-priority', 88888888);
					
					// Load our theme setup into the Wordpress after_setup_theme action
					
					add_action($action, array($this, 'initialize'), $priority);
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// ->initialize()
				// @Description
				//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
				// ----------------------------------------------------------------------------------------------------
				
				// @Version
				public $func_initialize = 1.0;
				public function initialize() {
					
					// Load our _validate file
					
					require_once('_validate.php');
					
				}
				
			}
			
			// ----------------------------------------------------------------------------------------------------
			// Instantiate our $this_class
			// ----------------------------------------------------------------------------------------------------
			
			new $this_class();
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
