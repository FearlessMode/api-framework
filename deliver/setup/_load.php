<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// @Note
	//  Instead of mutiple function and class checks you can run...
	// if (has_action('CustomTheme-init')) {}
	// if (has_action('CustomTheme-backend')) {}
	// if (has_action('CustomTheme-frontend')) {}
	// This is a way of identifying that our initial class and function checks have been validated
	//  meaning... if the custom action does exist then we know the function and class checks have already been passed
	// @Usage
	//  add_action('CustomTheme-init',     'My_Function_Name'); // Runs backend and frontend
	//  add_action('CustomTheme-frontend', 'My_Function_Name'); // Runs frontend only
	//  add_action('CustomTheme-backend',  'My_Function_Name'); // Runs backend only
	//  function My_Function_Name() {}
	// ----------------------------------------------------------------------------------------------------
	
	// Set our various path related variables
	
	$classes_path   = 'classes/';
	$functions_path = 'functions/';
	
	// Set our $global variable
	
	$global = $classes_path . 'global/';
	
	// ----------------------------------------------------------------------------------------------------
	// Load our safe function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($functions_path . 'safe.php');
	
	// ----------------------------------------------------------------------------------------------------
	// Load our is class and function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($global         . 'is.php');
	require_once($functions_path . 'is.php');
	
	// ----------------------------------------------------------------------------------------------------
	// Load our is class and function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($global         . 'user.php');
	require_once($functions_path . 'user.php');
	
	// ----------------------------------------------------------------------------------------------------
	// Load our path class and function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($global         . 'api.php');
	require_once($functions_path . 'api.php');
	
	// ----------------------------------------------------------------------------------------------------
	// Load our theme class and function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($global         . 'theme.php');
	require_once($functions_path . 'theme.php');
	
	// ----------------------------------------------------------------------------------------------------
	// Load our other theme function files
	// ----------------------------------------------------------------------------------------------------
	
	require_once($functions_path . 'theme-data.php');
	require_once($functions_path . 'theme-status.php');
	require_once($functions_path . 'version.php');
	require_once($functions_path . 'instantiate.php');
	
	// Load our main class file
	// @Note
	//  This is our main/core class. It only currently servers to initializing loading our theme related files
	//  When extended (the class) it will provide our path related variables to your class.
	//  * See CustomTheme_API class for more details
	
	require_once($classes_path . '_load.php');
	
	// Load our global class files
	// @Note
	//  These class files need to load in the frontend and backend
	
	require_once($global . 'global-helpers.php');
	require_once($global . 'hooks.php');
	require_once($global . 'developer.php');
	require_once($global . 'third-party.php');
	require_once($global . 'data.php');
	
	// Load our developer's custom action
	// @Note
	//  Functions tied to this action will be loaded before any of the theme related hooks are loaded
	//  So loading your custom routines to this action should all execute as expected :D - Enjoy!
	// @Hook->Action->CustomTheme-init
	
	do_action('CustomTheme-plugin');
	
	// Instantiate our classes
	
	instantiate('CustomTheme');
	instantiate('CustomTheme_Global_Helpers_API');
	instantiate('CustomTheme_Global_Hooks_API');
	instantiate('CustomTheme_Global_Developer_API');
	instantiate('CustomTheme_Global_Third_Party_API');
	instantiate('CustomTheme_Global_Data_API');
	
	// ----------------------------------------------------------------------------------------------------
	// If this is the site's frontend
	// ----------------------------------------------------------------------------------------------------
	
	if (is()->frontend) {
		
		// Set our $frontend variable
		
		$frontend = $classes_path . 'frontend/';
		
		// Load our frontend class files
		// @Note
		//  These class files need to load only in the frontend
		//  Let's focus on performance going forward, writing any new code so these classes only need to load in the site's frontend
		
		require_once($frontend . 'frontend-helpers.php');
		require_once($frontend . 'asset.php');
		require_once($frontend . 'output.php');
		require_once($frontend . 'content.php');
		require_once($frontend . 'template.php');
		
		// Load our developer's custom action
		// @Note
		//  Functions tied to this action will be loaded before any of the theme related hooks are loaded
		//  So loading your custom routines to this action should all execute as expected :D - Enjoy!
		// @Hook->Action->CustomTheme-frontend
		
		do_action('CustomTheme-plugin_frontend');
		
		// Instantiate our classes
		
		instantiate('CustomTheme_Frontend_Helpers_API');
		instantiate('CustomTheme_Frontend_Asset_API');
		instantiate('CustomTheme_Frontend_Output_API');
		instantiate('CustomTheme_Frontend_Content_API');
		instantiate('CustomTheme_Frontend_Template_API');
		
	}
	
	// ----------------------------------------------------------------------------------------------------
	// If this is the site's backend
	// ----------------------------------------------------------------------------------------------------
	
	else {
		
		// Set our $backend variable
		
		$backend = $classes_path . 'backend/';
		
		// Load our backend class files
		// @Note
		//  These class files need to load only in the backend
		
		require_once($backend        . 'backend-helpers.php');
		require_once($backend        . 'product.php');
		require_once($functions_path . 'product.php');
		require_once($backend        . 'advanced-array.php');
		require_once($backend        . 'simple-array.php');
		
		// Load our developer's custom action
		// @Note
		//  Functions tied to this action will be loaded before any of the theme related hooks are loaded
		//  So loading your custom routines to this action should all execute as expected :D - Enjoy!
		// @Hook->Action->CustomTheme-backend
		
		do_action('CustomTheme-plugin_backend');
		
		// Instantiate our classes
		
		instantiate('CustomTheme_Backend_Helpers_API'); 
		instantiate('CustomTheme_Backend_Product_API'); 
		instantiate('CustomTheme_Backend_Advanced_Array_API'); 
		instantiate('CustomTheme_Backend_Simple_Array_API'); 
		
	}
	
	// Start initializing/loading our theme
	// @Todo->!IMPORTANT_because_it_is_easy_shorten_wordplay_here_simplify
	// @Example
	//  theme()->initialize;
	
	theme()->start->initialize();
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
