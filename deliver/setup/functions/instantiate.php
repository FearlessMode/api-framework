<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our instantiate() function
	// @Description
	//  This allows us and extending developers to more easily instantiate our class
	//   while auto applying our custom actions and filters relative to that class
	// @Usage
	//  Instead of...
	//   new Class_Name_Here($constructor_args);
	//  Use...
	//   // instantiate($this_class, $constructor_args);
	// @Note
	//  * See function for @AutoHooks created
	// ----------------------------------------------------------------------------------------------------
	
	function instantiate($class = '', $constructor_args = array()) {
		
		// Set our $this_class variable
		
		$this_class = $class;
		
		// If our $this_class variable is not empty
		
		if (!empty($this_class)) {
			
			// If our $this_class exists
			
			if (class_exists($this_class)) {
				
				// Load our developer's custom action
				// @Hook->Action->$this_class-before
				
				do_action("{$this_class}-before");
				
				// If our developer's custom filter exists reset our $constructor_args variable
				// @Hook->Filter->$this_class-constructor_args
				
				if (has_filter("{$this_class}-constructor_args")) { $constructor_args = apply_filters("{$this_class}-constructor_args", $constructor_args); }
				
				// Instantiate $this_class and load any functions necessary via our $constructor_args
				
				new $this_class($constructor_args);
				
				// Load our developer's custom action
				// @Hook->Action->$this_class-after
				
				do_action("{$this_class}-after");
				
			}
		
		}
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
