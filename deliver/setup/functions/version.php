<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our version() function
	// @Description
	//  This function is a simple function wrapper to represent our theme class objects
	//   to more easily refer to class methods
	// @Usage
	//  
	// @Todo->!IMPORTANT_fix!!!
	// ----------------------------------------------------------------------------------------------------
	
	function version($filter_tag = '') {
		
		// Adjust our $filter_tag variable
		
		// $filter_tag = "$filter_tag-asset_version";
		// $filter_tag = theme()->hook->tag($filter_tag, 'asset_version', TRUE);
		
		// Set our $this_asset_version variables
		// @AutoHook->Filter->$filter_tag-asset_version
		// @Note
		//  If the auto filter created for this does not exist then our global default asset verison will be return
		// @Todo->!IMPORTANT_add_support_for_dynamic_class_filterable_asset_versions_:D_:D_;)
		
		// $this_asset_version = theme()->hook->filter($filter_tag, 'asset_version', theme()->api->asset_version);
		$this_asset_version = theme()->api->asset_version;
		
		// $this_asset_version = theme()->api->asset_version;
		
		// if (has_filter($filter_tag)) {
			
		// 	// Reset our $this_asset_version variable
			
		// 	$this_asset_version = apply_filters($filter_tag, $this_asset_version);
			
		// }
		
		// If our $this_asset_version is not empty let's return it otherwise 
		
		return (!empty($this_asset_version)) ? $this_asset_version : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
