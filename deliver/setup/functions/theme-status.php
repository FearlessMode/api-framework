<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// theme_status()
	// @Description
	//  This function allows devs to check against specific attributes of theme updates
	// @Usage
	// * See code for details
	// ----------------------------------------------------------------------------------------------------
	
	function theme_status($status = '') {
		
		// Set our $theme_data variable
		
		$theme_data = theme_data();
		
		// If our $status variable is not empty
		
		if (!empty($status)) {
			
			// If our $status variable is id
			// @Usage
			//  if (theme_status('id')     == 'custom-theme') {}
			//  if (theme_status('active') == 'custom-theme') {}
			
			if ($status == 'id' or $status == 'active') {
				
				// Set our $this_status variable
				//  $theme_data[0]
				
				$this_status = $theme_data[0];
				
			}
			
			// If our $status variable is version
			// @Usage
			//  if (theme_status('version') == 3.0) {}
			//  if (theme_status('version') > 3.0) {}
			//  if (theme_status('version') < 3.0) {}
			
			elseif ($status == 'version') {
				
				// Set our $this_status variable
				//  $theme_data[1]
				
				$this_status = $theme_data[1];
				
			}
			
			// If our $status variable is update
			// @Usage
			//  if (theme_status('update') == 'minor') {}
			//  if (theme_status('update') == 'major') {}
			
			elseif ($status == 'update') {
				
				// Set our $this_status variable
				//  $theme_data[2]
				
				$this_status = $theme_data[2];
				
			}
			
		}
		
		// If our $status variable is empty
		// @Note
		//  This returns all of the theme related data to check against for multiple check methods
		//  * See function theme_data() (/variables/_load.php) for all array data returned
		// @Usage
		//  The below is an example of only loading your code if the theme version is equal to or greater than 1.0, 
		//		but is a minor update.
		//  This is just an idea for now that will help devs avoid potential conflicts with 'major' updates if they choose to
		//   $theme_data = theme_data();
		//   if ($theme_data[1] >= 1.0 and $theme_data[2]  == 'minor') {}
		//   if ($theme_data[1] >= 1.0 and $theme_data[2] !== 'major') {}
		
		else {
			
			// Set our $this_status variable
			
			$this_status = $theme_data;
			
		}
		
		// If our $this_status variable is not empty let's return it otherwise FALSE
		
		return (!empty($this_status)) ? $this_status : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
