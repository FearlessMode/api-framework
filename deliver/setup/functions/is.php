<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our path() function
	// @Description
	//  This function is a simple function wrapper to represent our theme class objects
	//   to more easily refer to class methods
	// @Usage
	// 
	//  Wordpress Shorthands
	// 
	//   if (is()->backend) { Your_Code_Here... }
	//   if (is()->frontend) { Your_Code_Here... }
	// 
	//  Device Context Shorthands
	//  @Todo->!IMPORTANT_revisit_and_confirm_availale_options
	//  @Todo->!IMPORTANT_add_more_variety_combination_support_or_lack_of
	//   
	//   if (is(array('browser', 'version', 'system'))) { Your_Code_Here... }
	//   
	//   if (is(array('chrome'))) { Your_Code_Here... }
	//   if (is(array('chrome', 99, 'mac'))) { Your_Code_Here... runs only to chrome version 99 and above on mac systems }
	//   if (is(array('firefox'))) { Your_Code_Here... }
	//   if (is(array('firefox', 99, 'windows'))) { Your_Code_Here... runs only to firefox version 99 and above on window systems }
	//   if (is(array('safari'))) { Your_Code_Here... }
	//   if (is(array('ios6'))) { Your_Code_Here... }
	//   if (is(array('safari', 99, 'ios6'))) { Your_Code_Here... runs only to safari version 99 and above on iphone 6 systems }
	// ----------------------------------------------------------------------------------------------------
	
	function is($device_args = array()) {
		
		// Set our $theme_object_api variable
		
		$theme_object_api = new CustomTheme_Global_Is_API($device_args);
		
		// If our $theme_object_api is not empty let's return it otherwise FALSE
		
		return (!empty($theme_object_api)) ? $theme_object_api : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
