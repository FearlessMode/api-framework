<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our user() function
	// @Description
	//  This function 
	// @Usage
	//  if (user()->can($capability_value_here)) { Your_Code_Here... }
	//  if (user()->has($property_value_here)) { Your_Code_Here... }
	// ----------------------------------------------------------------------------------------------------
	
	function user() {
		
		// Set our $theme_object_api variable
		
		$theme_object_api = new CustomTheme_Global_User_API();
		
		// If our $theme_object_api is not empty let's return it otherwise FALSE
		
		return (!empty($theme_object_api)) ? $theme_object_api : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
