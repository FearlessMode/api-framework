<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our notice() function
	// @Description
	//  This function is a simple function wrapper to 
	// @Usage
	//  add_action('admin_notices', function() { notice('success', 'Success message here'); });
	//  add_action('admin_notices', function() { notice('error',   'Error message here'); });
	//  add_action('admin_notices', function() { notice('warning', 'Warning message here'); });
	//  add_action('admin_notices', function() { notice('info',    'Info message here'); });
	// ----------------------------------------------------------------------------------------------------
	
	function notice($type = '', $message = '') {
		
		// If our $type and $message variables are not empty
		
		if (!empty($type . $message)) {
			
			// Set our $backend_helper variable
			
			$backend_helper = new CustomTheme_Backend_Helpers_API();
			
			// Set our $this_notice variable
			
			$this_notice = $backend_helper->notice($type, $message);
			
		}
		
		// If our $this_notice variable is not empty let's return it otherwise FALS
		
		return (!empty($this_notice)) ? $this_notice : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
