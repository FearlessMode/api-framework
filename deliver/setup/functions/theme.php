<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our theme() function
	// @Description
	//  This function is a simple function wrapper to represent our theme class objects
	//   to more easily refer to class methods
	// @Usage
	//  theme()->variable_that_represents_class->function_method_here()
	// @Example
	//  theme()->global->constructor()
	//  theme()->hook->do_action()
	//  theme()->asset->load()
	// ----------------------------------------------------------------------------------------------------
	
	function theme() {
		
		// Set our $theme_object_api variable
		
		$theme_object_api = new CustomTheme_Global_Theme_API();
		
		// If our $theme_object_api is not empty let's return it otherwise FALSE
		
		return (!empty($theme_object_api)) ? $theme_object_api : FALSE;
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
