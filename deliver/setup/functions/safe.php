<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Safe Function Call Helpers
	//  A php wrapper for calling functions safely. 
	//  ie: when a function is not avialable we can: 
	//   do nothing, show a message, call another function, etc.
	// ----------------------------------------------------------------------------------------------------
	
	// ----------------------------------------------------------------------------------------------------
	// Contributors: coffee2code
	// Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6ARCFJ9TX3522
	// License: GPLv2 or later
	// License URI: http://www.gnu.org/licenses/gpl-2.0.html
	// Requires at least: 1.5
	// Tested up to: 4.7.1
	// Safely and easily call functions that may not be available (such as those provided by a plugin that gets deactivated)
	// ----------------------------------------------------------------------------------------------------
	
	// ----------------------------------------------------------------------------------------------------
	// Safely invoke the function by the name of $callback. 
	//  Any additional arguments will get passed to $callback(). 
	//  If the callback does not exist, nothing is displayed and no error is generated.
	// @param callback $callback The function to call.
	// @return bool TRUE if callback is valid, FALSE otherwise.
	// ----------------------------------------------------------------------------------------------------
	
	function __sfc_is_valid_callback($callback) {
		
		if (is_string($callback)) {
			
			if (!function_exists($callback)) {
				
				return FALSE;
				
			}
			
		}
		
		elseif (is_array($callback) and 2 === count($callback)) {
			
			if (!method_exists($callback[0], $callback[1])) {
				
				return FALSE;
				
			}
			
		}
		
		else {
			
			return FALSE;
			
		}
		
		return TRUE;
		
	}

	// ----------------------------------------------------------------------------------------------------
	// Safely invoke the function by the name of $callback and return the result. 
	//  Any additional arguments will get passed to $callback(). 
	//  If the callback does not exist, nothing is displayed and no error is generated.
	// @param callback $callback The function to call.
	// @return mixed If the callback exists as a function, returns whatever that function returns. Otherwise, returns nothing.
	// ----------------------------------------------------------------------------------------------------
	
	// @usage
	//  safe_call('Function_Name_Here');
	
	function safe_call($callback) {
		
		if (__sfc_is_valid_callback($callback)) {
			
			$args = array_slice(func_get_args(), 1);
			
			return call_user_func_array($callback, $args);
			
		}
		
	}
	
	// ----------------------------------------------------------------------------------------------------
	// Safely invoke the function by the name of $callback and return the result. 
	//  Any additional arguments will get passed to it. If the callback does not exist, then a message gets echoed.
	// This functions is the same as _sfc() except that if the intended function does not exist, then a string is echoed (if provided).--
	// @param string $callback The function to call.
	// @param string $msg_if_missing (optional) String to be echoed if $callback does not exist.
	// @return mixed If $callback exists as a function, returns whatever that function returns. Otherwise, returns nothing.
	// ----------------------------------------------------------------------------------------------------
	
	// @usage
	//  safe_message('Function_Name_Here', 'Function_Message_Here');
	
	function safe_message($callback, $msg_if_missing = '') {
		
		if (!__sfc_is_valid_callback($callback)) {
			
			if ($msg_if_missing) {
				
				echo $msg_if_missing;
				
			}
			
			return;
			
		}
		
		$args = array_slice(func_get_args(), 2);
		
		return call_user_func_array($callback, $args);
		
	}

	// ----------------------------------------------------------------------------------------------------
	// Safely invoke the function by the name of $callback and return the result. 
	//  Any additional arguments will get passed to it. 
	//  If the callback does not exist, then an alternative function is called.
	// This functions is the same as _sfc() except that if the intended function does not exist, 
	//  then an alternative function is invoked (if it exists itself).
	// @param string $callback The function to call.
	// @param string $fallback_callback (optional) Name of the alternative function to call if the callback does not exist.
	// @return mixed If the callback exists as a function, returns whatever that function returns. Otherwise, returns nothing.
	// ----------------------------------------------------------------------------------------------------
	
	// @usage
	//  safe_fallback('Function_Name_Here', 'Unavailable_Function_Handler');
	//  function Unavailable_Function_Handler($args) {
	//      echo "The function \"$args\" is not available.";
	//      // Or your fallback function here
	//  }
	
	function safe_fallback($callback, $fallback_callback = NULL) {
		
		$args = array_slice(func_get_args(), 2);
		
		if (!__sfc_is_valid_callback($callback)) {
			
			if (__sfc_is_valid_callback($fallback_callback)) {
				
				$args = array_merge(array($callback), $args);
				
				return call_user_func_array($fallback_callback, $args);
				
			}
			
			return;
			
		}
		
		return call_user_func_array($callback, $args);
		
	}
	
	// ----------------------------------------------------------------------------------------------------
	// Safely invoke the function by the name of $callback and echo and return the result. 
	//  Any additional arguments will get passed to it. If the callback does not exist, nothing is displayed and no error is generated.
	//  This function is the same as _sfc() except that it echoes the return value of the callback before returning that value.
	// @param string $callback The function to call.
	// @return mixed If the callback exists as a function, returns whatever that function returns. Otherwise, returns nothing.
	// ----------------------------------------------------------------------------------------------------
	
	// @usage
	//  safe_echo('Function_Name_Here', 'Function_Message_Here');
	
	function safe_echo($callback) {
		
		if (__sfc_is_valid_callback($callback)) {
			
			$args  = func_get_args();
			
			$value = call_user_func_array('safe_call', $args);
			
			if ($value) echo $value;
			
			return $value;
			
		}
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
