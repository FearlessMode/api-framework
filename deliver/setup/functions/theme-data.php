<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Set our $theme_data variable array
	// ----------------------------------------------------------------------------------------------------
	
	function theme_data() {
		
		return $theme_data = 
		array(
			
			// Theme ID
			// $theme_data[0]
			
			'custom-theme',
			
			// Theme Version
			// $theme_data[1]
			
			1.0,
			
			// Update Type
			// $theme_data[2]
			
			'minor',
			
			// Advanced Framework
			
			// Advanced Framework Version
			// $theme_data[3]
			// $theme_data[4]
			
			1.0,
			'minor',
			
			// Advanced Framework Settings Version
			// $theme_data[5]
			// $theme_data[6]
			
			1.0,
			'minor',
			
			// Simple Framework
			
			// Simple Framework Version
			// $theme_data[7]
			// $theme_data[8]
			
			1.0,
			'minor',
			
			// Simple Framework Settings Version
			// $theme_data[9]
			// $theme_data[10]
			
			1.0,
			'minor',
			
			// Product Framework
			
			// Product Framework Version
			// $theme_data[11]
			// $theme_data[12]
			
			1.0,
			'minor',
			
			// Product Framework Settings Version
			// $theme_data[13]
			// $theme_data[14]
			
			1.0,
			'minor',
			
		);
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
