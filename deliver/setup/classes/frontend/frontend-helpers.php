<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Frontend_Helpers_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Frontend_Helpers_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->widget()
		// @Description
		//  Check for active sidebars ("active" has at least one widget in it, which makes a sidebar "active", 
		//   which then that sidebar can be displayed if it should.).
		//   @ http://codex.wordpress.org/Function_Reference/is_active_sidebar
		// @Usage
		//  None
		// @Todo->!IMPORTANT_This_is_old_IMPROVE_funtion_naming_and_location_should_probably_be_frontend_helper_for_checking_active_widgets_to_load
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_widget = 1.0;
		public function widget($sidebar_id) {
			
			// If our context is frontend
			
			if (is()->frontend) {
				
				// Set our global variables
				
				global $wp_registered_sidebars;
				
				// Set our this sidebar variable
				
				$this_sidebar = wp_get_sidebars_widgets();
				
				// If our this sidebar sidebar id variable is set return TRUE otherwise FALSE
				
				if (isset($this_sidebar[$sidebar_id]) and $this_sidebar[$sidebar_id]) { return TRUE; } else { return FALSE; }
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
