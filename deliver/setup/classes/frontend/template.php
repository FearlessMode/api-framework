<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Frontend_Template_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Frontend_Template_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->load()
		// @Description
		//  This function 
		// @Usage
		// @Note
		//  This has been updated to dyanamically extract the template file needed.
		//  When __FILE__ is passed the full path prior and the .php will be removed
		//  That will leave only the file slug which is what Wordpress normally used to determine the template or template part to load
		// @Example
		//  theme()->template->load(__FILE__);
		//  When the above function is written in the header.php the __FILE__ will pass the $type variable 'header'
		//  When the above function is written in the footer.php the __FILE__ will pass the $type variable 'footer'
		// * See header.php, body.php, footer.php and canvas.php for live examples
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public static $func_load = 11.0;
		public function load($type = '') {
			
			// Adjust our $type variable
			
			$type = basename($type, '.php');
			
			// If our type is header
			
			if ($type == 'header') {
				
				// Load our beginning HTML structure
				
				theme()->output->html('structure', 'begin');
				
				// Set our id name variable
				
				$id_name = 'header';
				
				// Set our this filter variable
				
				$this_filter = $id_name;
				
				// Set our row classes variable
				
				$row_classes = theme()->hook->filter($this_filter . '_row_classes', '', 'row center');
				
				// Load our header HTML and output
				
				theme()->output->div('tag', 'open', $id_name);
					
					theme()->output->div('id', $id_name, "{$id_name} {$row_classes}");
						
						// Load our top banner output
						
						theme()->content->get_filtered_output('top_banner', array('top_banner'), TRUE);
						
						// Load our social menu output
						
						theme()->content->menu('social-menu');
						
						// Load our above header output
						
						theme()->content->get_filtered_output('above_' . $id_name, array('above_' . $id_name));
						
						// Load our header output
						
						theme()->content->get_filtered_output($id_name, array($id_name));
						
						// Load our header menu output
						
						theme()->content->menu('header-menu');
						
						// Load our below header output
						
						theme()->content->get_filtered_output('below_' . $id_name, array('below_' . $id_name));
						
					theme()->output->div('close');
					
				theme()->output->div('tag', 'close', $id_name);
				
			}
			
			// If our output is body
			
			elseif ($type == 'body') {
				
				// Load our beginning HTML components
				
				theme()->output->div('class', 'outer-wrapper');
				theme()->output->div('class', 'inner-wrapper');
				
				// Load our main tag
				
				theme()->output->div('tag', 'open', 'main');
					
					// Set our default layout type variable
					// @Todo->!IMPORTANT_PERFORMANCE_update_method_to_check_filter_hook_first_so_data_option_method_is_not_checked_when_not_needed
					
					$default_layout_type = theme()->hook->filter('default_layout_type', '', 'no_sidebar');
					
					// Set our layout type variable
					
					$layout_type = theme()->data->option('layout_type', $default_layout_type);
					
					// Load our developer filter hook
					
					if (theme()->hook->has_filter('force_layout_type')) {
						
						// Reset our layout type variable
						
						$layout_type = theme()->hook->add_filter('force_layout_type');
						
					}
					
					// Set our layout type class variable
					
					$layout_class = str_replace('_', '-', $layout_type);
					
					// Set our layout type variable
					
					$before_layout_classes = theme()->hook->filter('before_layout_classes');
					
					// Set our layout type variable
					
					$after_layout_classes = theme()->hook->filter('after_layout_classes');
					
					// Set our before and after classes variables
					
					$before_layout_classes = (!empty($before_layout_classes)) ? "{$before_layout_classes} " : FALSE;
					$after_layout_classes  = (!empty($after_layout_classes))  ? " {$after_layout_classes}"  : FALSE;
					
					// Load our body within our passed layout type
					
					theme()->content->body_layout($layout_type, "{$before_layout_classes}{$layout_class}-layout{$after_layout_classes}");
					
				// Close our main tag
				
				theme()->output->div('tag', 'close', 'main');
				
				// @Note
				//  Our outer and inner wrappers are closed in our footer
				
			}
			
			// If our output is footer
			
			elseif ($type == 'footer') {
				
				// Set our footer and copyright id variables
				
				$footer_id    = 'footer';
				$copyright_id = 'copyright';
				
				// Set our row classes variable
				
				$row_classes = 'row center';
				
				// Set our row classes variable
				
				$row_classes = theme()->hook->filter($footer_id . '_row_classes', '', 'row center');
				
				// Load our footer open tag
				
				theme()->output->div('tag', 'open', $footer_id);
					
					// Load our footer open id and classes
					
					theme()->output->div('id', $footer_id, "{$footer_id} {$row_classes}");
						
						// Load our above footer output
						
						theme()->content->get_filtered_output('above_' . $footer_id, array('above_' . $footer_id));
						
						// Load our footer output
						
						theme()->content->get_filtered_output($footer_id, array($footer_id));
						
						// Load our below footer output
						
						theme()->content->get_filtered_output('below_' . $footer_id, array('below_' . $footer_id));
						
						// Load our bottom banner output
						
						theme()->content->get_filtered_output('bottom_banner', array('bottom_banner'), TRUE);
						
						// Load our footer menu output
						
						theme()->content->menu('footer-menu');
						
						// If our developer filter hook exists
						
						if (theme()->hook->has_filter('above_' . $copyright_id)) {
							
							// Set our these values variable
							
							$these_values = theme()->hook->add_filter('above_' . $copyright_id);
							
							// Load our above copyright output
							
							theme()->content->get_filtered_output('above_' . $copyright_id, $these_values);
							
						}
						
						// Load our copyright credit menu output
						
						theme()->content->menu('copyright-credit-menu');
						
						// Load our copyright output
						
						theme()->content->get_filtered_output($copyright_id, array($copyright_id));
						
						// If our developer filter hook exists
						
						if (theme()->hook->has_filter('below_' . $copyright_id)) {
							
							// Set our these values variable
							
							$these_values = theme()->hook->add_filter('below_' . $copyright_id);
							
							// Load our below copyright output
							
							theme()->content->get_filtered_output('below_' . $copyright_id, $these_values);
							
						}
						
					// Close our footer open id and classes
					
					theme()->output->div('close');
					
				// Close our footer tag
				
				theme()->output->div('tag', 'close', $footer_id);
				
				// Load our ending HTML components
				
				theme()->output->div('close'); // Close our .inner-wrapper
				theme()->output->div('close'); // Close our .outer-wrapper
				
				// Load our ending HTML structure
				
				theme()->output->html('structure', 'end');
				
			}
			
			// If our output is canvas
			
			elseif ($type == 'canvas') {
				
				// Load our beginning HTML structure
				
				theme()->output->html('structure', 'begin');
				
				// Set our various id name variables
				
				$id_name  = 'canvas';
				$tag_name = 'main';
				
				// Load our beginning HTML components
				
				theme()->output->div('class', 'outer-wrapper');
				theme()->output->div('class', 'inner-wrapper');
					
					// Load our main tag
					
					theme()->output->div('tag', 'open', $tag_name);
						
						// Load our post content
						
						theme()->content->get_post_content($id_name);
						
					// Close our main tag
					
					theme()->output->div('tag', 'close', $tag_name);
					
				// Load our ending HTML components
				
				theme()->output->div('close');
				theme()->output->div('close');
				
				// Load our beginning HTML structure
				
				theme()->output->html('structure', 'end');
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->file()
		// @Description
		//  This function determines which php template to load. It is mostly a simple wrapper with auto hook creation
		// @Note
		//  Our template() runs the routine in this priority
		//  If the developer's hook exists load the hook
		//  If the developer's hook doesn't exist and the developer's custom php template exists load the php template
		//  If the developer's custom php template doesn't exists load our default php template
		//   The 2nd template() $arg is the default to load if the developer's hook and template doesn't exist
		//   See our API->template() for other options
		// @Usage
		//  $api->template('header', get_header());
		//  $api->template('body', get_template_part('body'));
		//  $api->template('footer', get_footer());
		//  * See ../index.php for more details/examples to understand more clearly as a Wordpress routine
		// @AutoHook
		//  add_action('CustomTheme-$template_name-template);
		//  add_action('CustomTheme-header-template);
		//  add_action('CustomTheme-body-template);
		//  add_action('CustomTheme-footer-template);
		// @Todo->consider_more_ways_to_make_hookable
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_file = 2.0;
		public function file($template_name = '', $func = '', $full_template_file_path = '') {
			
			// If our developer's custom template action does exist
			
			if (theme()->hook->has_action($template_name, 'template')) {
				
				// Load our developer's custom template action
				// @AutoHook->Action->CustomTheme-$template_name-template
				
				theme()->hook->do_action($template_name, 'template');
				
			}
			
			// If our $func variable is empty
			
			elseif (empty($func)) {
				
				// Load our template file
				// @AutoHook->Filter->CustomTheme-$template_name-template
				
				theme()->global->load_file("{$template_name}-template", $full_template_file_path);
				
			}
			
			// If our $func variable is not empty load our function
			
			else {
				
				// If our $func variable is a function
				
				if (is_callable($func)) {
					
					// Load our $func variable
					
					$func;
					
				}
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
