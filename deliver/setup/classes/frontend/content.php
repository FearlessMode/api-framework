<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Frontend_Content_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Frontend_Content_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $default_layout;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			$this->default_layout = '__custom_theme_default_layout__';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_layout()
		// @Description
		//  This function creates our default and custom layout data from our custom post types.
		//  When the "Default" Layout option is saved in our custom post type "Post Type Layout"
		//   the $post_id is saved to our get_option('__custom_theme_default_layout__').
		//   Then to check for our default layout settings we get the $post_id from that get_option()
		//    then we run the get_post_meta() for that $post_id retrieved from the get_option().
		//  When the "Custom" Layout option is saved in our custom post type "Post Type Layout"
		//   the $post_id is saved dynamically to a get_option('__custom_theme_' . $current_post_type . '_layout__').
		//   Then to check for our custom post type layout settings we run the is_singular() against the dynamically created get_option()
		//   If we have a match, then we run the get_post_meta() for that $post_id retrieved from that dynaimcally created get_option().
		//   Then run the get_post_meta() for that $post_id. If the get_post_meta() reveals that this custom post type layout is be enabled
		//    then we load it, otherwise return to our default layout method
		//  * See function code for more details
		// @Usage
		//  @Todo->create_revisit
		//  @Todo->create_taxonomy_methods
		//  @Todo->add_hooks
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_layout = 1.0;
		public function get_layout($type = '', $option_name = '', $indexes = '', $output_method = '') {
			
			// If our option name variable is default
			
			if ($option_name == 'default') {
				
				// Set our $default_layout_id variable
				
				$default_layout_id = theme()->data->get_option($this->theme_data, $this->default_layout);
				
				// If our type variable is data
				
				if ($type == 'data') {
					
					// Set our layout data variable
					
					$layout_data = theme()->data->get($this->post_data, theme()->dev->option_name($option_name), $indexes, $default_layout_id);
					
				}
				
				// If our type variable is id
				
				elseif ($type == 'id') {
					
					// Set our layout data variable
					
					$layout_data = $default_layout_id;
					
				}
				
			}
			
			// If our option name variable is current
			
			elseif ($option_name == 'current') {
				
				// If our type variable is data
				
				if ($type == 'data') {
					
					// Set our layout data variable
					
					$layout_data = theme()->data->get($this->post_data, theme()->dev->option_name($option_name), $indexes);
					
				}
				
				// If our type variable is id
				
				elseif ($type == 'id') {
					
					// Set our layout data variable
					
					$layout_data = get_the_ID();
					
				}
				
			}
			
			// If our option name variable is empty or is custom
			
			elseif (empty($option_name) or $option_name == 'custom') {
				
				// Set our $default_layout_id variable
				
				$default_layout_id = theme()->data->get_option($this->theme_data, $this->default_layout);
				
				// Set our current post type variable
				
				$current_post_type = theme()->global->post_type();
				
				// If our current post type variable matches the singular version of the current page, post being displayed
				
				if (is_singular($current_post_type)) {
					
					// Set our custom layout id variable
					
					$custom_layout_id = theme()->data->get_option($this->theme_data, '__custom_theme_' . $current_post_type . '_layout__');
					
					// If our custom layout id variable is TRUE
					
					if ($custom_layout_id) {
						
						// Set our custom layout data variable
						
						$custom_layout_data = theme()->data->get($this->post_data, theme()->dev->option_name($option_name), 'layout', $custom_layout_id);
						
						// If our custom layout data variable is TRUE
						
						if ($custom_layout_data) {
							
							// Set our use as varibale
							
							$use_as = $custom_layout_data['use_as'];
							
							// If our use as variable is custom
							
							if ($use_as == 'custom') {
								
								// Set our apply to variable
								
								$apply_to = $custom_layout_data['apply_to'];
								
								// If our apply to variable is an array (ie: exists) and if one of those array items matches our current post type variable
								
								if (is_array($apply_to) and in_array($current_post_type, $apply_to)) {
									
									// Set our this layout id variable
									
									$this_layout_id = $custom_layout_id;
									
								}
								
							}
							
						}
						
					}
					
				}
				
				// Set our layout id status variable
				
				$layout_id_status = (!empty($this_layout_id)) ? $this_layout_id : $default_layout_id;
				
				// If our type variable is id
				
				if ($type == 'id') {
					
					// Set our layout data variable
					
					$layout_data = $layout_id_status;
					
				}
				
				// If our type variable is data
				
				elseif ($type == 'data') {
					
					// Set our layout data variable
					
					$layout_data = theme()->data->get($this->post_data, theme()->dev->option_name($option_name), $indexes, $layout_id_status);
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($layout_data, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->body_layout()
		// @Description
		//  This function handles getting all of our layout data and displaying those options on the site's frontend
		//  * See function code for more details
		// @Usage
		//  $this->body_layout('no_sidebar', 'no-sidebar-class-here');
		//  $this->body_layout('left_sidebar', 'left-sidebar-class-here', 'show_title', FALSE, FALSE);
		//  $this->body_layout('right_sidebar', 'right-sidebar-class-here', 'show_title', TRUE, FALSE);
		//  $this->body_layout('dual_sidebar', 'dual-sidebar-class-here', FALSE);
		//  * See function code for more details
		//  * See function code $this->output('body') for more details
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_body_layout = 11.0;
		public function body_layout($layout_type = '', $layout_class = '', $show_title = TRUE, $above = TRUE, $below = TRUE) {
			
			// Set our various variables
			
			{
				
				// ----------------------------------------------------------------------------------------------------
				// Set our base class variables
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// @Hook->add_filter('CustomTheme-content_id');
					
					$content_id         = theme()->hook->filter('content_id', FALSE, 'content');
					
					// @Hook->add_filter('CustomTheme-content_class');
					
					$content_class      = theme()->hook->filter('content_class', FALSE, 'content');
					
					// @Hook->add_filter('CustomTheme-outer_and_position');
					
					$outer_and_position = theme()->hook->filter('outer_and_position', FALSE, 'row center');
					
					$content_class      = "$content_class $outer_and_position";
					
					// @Hook->add_filter('CustomTheme-row_classes');
					
					$row_classes        = theme()->hook->filter('row_classes', FALSE, 'row center');
					
					$layout_class       = ($layout_class !== 'no-sidebar-layout') ? $layout_class : '';
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// Set our no sidebar content variables
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// @Hook->add_filter('CustomTheme-no_sidebar_content_base_span');
					
					$no_sidebar_content_base_span    = theme()->hook->filter('no_sidebar_content_base_span', FALSE, 'column span-12');
					
					// @Hook->add_filter('CustomTheme-no_sidebar_content_desktop_span');
					
					$no_sidebar_content_desktop_span = theme()->hook->filter('no_sidebar_content_desktop_span', FALSE, 'desktop-12');
					
					// @Hook->add_filter('CustomTheme-no_sidebar_content_mobile_span');
					
					$no_sidebar_content_mobile_span  = theme()->hook->filter('no_sidebar_content_mobile_span', FALSE, 'mobile-12');
					
					// @Hook->add_filter('CustomTheme-no_sidebar_content_phone_span');
					
					$no_sidebar_content_phone_span   = theme()->hook->filter('no_sidebar_content_phone_span', FALSE, 'phone-12');
					
					$no_sidebar_content_classes      = "$no_sidebar_content_base_span $no_sidebar_content_desktop_span $no_sidebar_content_mobile_span $no_sidebar_content_phone_span";
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// Left Sidebar Layout
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// Set our left sidebar variables
					
					{
						
						// @Hook->add_filter('CustomTheme-left_sidebar_content_base_span');
						
						$left_sidebar_content_base_span    = theme()->hook->filter('left_sidebar_content_base_span', FALSE, 'column span-8');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_content_desktop_span');
						
						$left_sidebar_content_desktop_span = theme()->hook->filter('left_sidebar_content_desktop_span', FALSE, 'desktop-8');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_content_mobile_span');
						
						$left_sidebar_content_mobile_span  = theme()->hook->filter('left_sidebar_content_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_content_phone_span');
						
						$left_sidebar_content_phone_span   = theme()->hook->filter('left_sidebar_content_phone_span', FALSE, 'phone-12');
						
						$left_sidebar_content_classes      = "$left_sidebar_content_base_span $left_sidebar_content_desktop_span $left_sidebar_content_mobile_span $left_sidebar_content_phone_span";
						
					}
					
					// Set our left sidebar variables
					
					{
						
						// @Hook->add_filter('CustomTheme-left_sidebar_base_span');
						
						$left_sidebar_base_span    = theme()->hook->filter('left_sidebar_base_span', FALSE, 'column span-4');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_desktop_span');
						
						$left_sidebar_desktop_span = theme()->hook->filter('left_sidebar_desktop_span', FALSE, 'desktop-4');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_mobile_span');
						
						$left_sidebar_mobile_span  = theme()->hook->filter('left_sidebar_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-left_sidebar_phone_span');
						
						$left_sidebar_phone_span   = theme()->hook->filter('left_sidebar_phone_span', FALSE, 'phone-12');
						
						$left_sidebar_classes      = "$left_sidebar_base_span $left_sidebar_desktop_span $left_sidebar_mobile_span $left_sidebar_phone_span";
						
					}
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// Right Sidebar Layout
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// Set our right sidebar content variables
					
					{
						
						// @Hook->add_filter('CustomTheme-right_sidebar_content_base_span');
						
						$right_sidebar_content_base_span    = theme()->hook->filter('right_sidebar_content_base_span', FALSE, 'column span-8');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_content_desktop_span');
						
						$right_sidebar_content_desktop_span = theme()->hook->filter('right_sidebar_content_desktop_span', FALSE, 'desktop-8');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_content_mobile_span');
						
						$right_sidebar_content_mobile_span  = theme()->hook->filter('right_sidebar_content_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_content_phone_span');
						
						$right_sidebar_content_phone_span   = theme()->hook->filter('right_sidebar_content_phone_span', FALSE, 'phone-12');
						
						$right_sidebar_content_classes      = "$right_sidebar_content_base_span $right_sidebar_content_desktop_span $right_sidebar_content_mobile_span $right_sidebar_content_phone_span";
						
					}
					
					// Set our right sidebar variables
					
					{
						
						// @Hook->add_filter('CustomTheme-right_sidebar_base_span');
						
						$right_sidebar_base_span    = theme()->hook->filter('right_sidebar_base_span', FALSE, 'column span-4');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_desktop_span');
						
						$right_sidebar_desktop_span = theme()->hook->filter('right_sidebar_desktop_span', FALSE, 'desktop-4');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_mobile_span');
						
						$right_sidebar_mobile_span  = theme()->hook->filter('right_sidebar_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-right_sidebar_phone_span');
						
						$right_sidebar_phone_span   = theme()->hook->filter('right_sidebar_phone_span', FALSE, 'phone-12');
						
						$right_sidebar_classes      = "$right_sidebar_base_span $right_sidebar_desktop_span $right_sidebar_mobile_span $right_sidebar_phone_span";
						
					}
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// Dual Sidebar Layout
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// Set our dual sidebar content variables
					
					{
						
						// @Hook->add_filter('CustomTheme-dual_sidebar_content_base_span');
						
						$dual_sidebar_content_base_span    = theme()->hook->filter('dual_sidebar_content_base_span', FALSE, 'column span-6');
						
						// @Hook->add_filter('CustomTheme-dual_sidebar_content_desktop_span');
						
						$dual_sidebar_content_desktop_span = theme()->hook->filter('dual_sidebar_content_desktop_span', FALSE, 'desktop-6');
						
						// @Hook->add_filter('CustomTheme-dual_sidebar_content_mobile_span');
						
						$dual_sidebar_content_mobile_span  = theme()->hook->filter('dual_sidebar_content_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-dual_sidebar_content_phone_span');
						
						$dual_sidebar_content_phone_span   = theme()->hook->filter('dual_sidebar_content_phone_span', FALSE, 'phone-12');
						
						$dual_sidebar_content_classes      = "$dual_sidebar_content_base_span $dual_sidebar_content_desktop_span $dual_sidebar_content_mobile_span $dual_sidebar_content_phone_span";
						
					}
					
					// Set our dual left sidebar variables
					
					{
						
						// @Hook->add_filter('CustomTheme-dual_left_sidebar_base_span');
						
						$dual_left_sidebar_base_span    = theme()->hook->filter('dual_left_sidebar_base_span', FALSE, 'column span-3');
						
						// @Hook->add_filter('CustomTheme-dual_left_sidebar_desktop_span');
						
						$dual_left_sidebar_desktop_span = theme()->hook->filter('dual_left_sidebar_desktop_span', FALSE, 'desktop-3');
						
						// @Hook->add_filter('CustomTheme-dual_left_sidebar_mobile_span');
						
						$dual_left_sidebar_mobile_span  = theme()->hook->filter('dual_left_sidebar_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-dual_left_sidebar_phone_span');
						
						$dual_left_sidebar_phone_span   = theme()->hook->filter('dual_left_sidebar_phone_span', FALSE, 'phone-12');
						
						$dual_left_sidebar_classes      = "$dual_left_sidebar_base_span $dual_left_sidebar_desktop_span $dual_left_sidebar_mobile_span $dual_left_sidebar_phone_span";
						
					}
					
					// Set our dual right sidebar variables
					
					{
						
						// @Hook->add_filter('CustomTheme-dual_right_sidebar_base_span');
						
						$dual_right_sidebar_base_span    = theme()->hook->filter('dual_right_sidebar_base_span', FALSE, 'column span-3');
						
						// @Hook->add_filter('CustomTheme-dual_right_sidebar_desktop_span');
						
						$dual_right_sidebar_desktop_span = theme()->hook->filter('dual_right_sidebar_desktop_span', FALSE, 'desktop-3');
						
						// @Hook->add_filter('CustomTheme-dual_right_sidebar_mobile_span');
						
						$dual_right_sidebar_mobile_span  = theme()->hook->filter('dual_right_sidebar_mobile_span', FALSE, 'mobile-12');
						
						// @Hook->add_filter('CustomTheme-dual_right_sidebar_phone_span');
						
						$dual_right_sidebar_phone_span   = theme()->hook->filter('dual_right_sidebar_phone_span', FALSE, 'phone-12');
						
						$dual_right_sidebar_classes      = "$dual_right_sidebar_base_span $dual_right_sidebar_desktop_span $dual_right_sidebar_mobile_span $dual_right_sidebar_phone_span";
						
					}
					
				}
				
				// ----------------------------------------------------------------------------------------------------
				// Set our content first variables
				// ----------------------------------------------------------------------------------------------------
				
				{
					
					// @Hook->add_filter('CustomTheme-content_first_desktop_span');
					// @Note
					// @Todo->reconsider_currently_not_used
					
					$content_first_desktop_span = theme()->hook->filter('content_first_desktop_span', FALSE, 'first-desktop');
					
					// @Hook->add_filter('CustomTheme-content_first_mobile_span');
					
					$content_first_mobile_span  = theme()->hook->filter('content_first_mobile_span', FALSE, 'first-mobile');
					
					// @Hook->add_filter('CustomTheme-content_first_phone_span');
					
					$content_first_phone_span   = theme()->hook->filter('content_first_phone_span', FALSE, 'first-phone');
					
				}
				
			}
			
			// Set and combine our content related variables
			
			{
				
				$no_sidebar_content_classes    .= " $content_first_mobile_span $content_first_phone_span";
				$left_sidebar_content_classes  .= " $content_first_mobile_span $content_first_phone_span";
				$right_sidebar_content_classes .= " $content_first_mobile_span $content_first_phone_span";
				$dual_sidebar_content_classes  .= " $content_first_mobile_span $content_first_phone_span";
				
				// Remove any spaces from the begininng and ending of our classes for ideal output
				//  @Note
				//   All our classes have a space at the end of them so this function in result
				//    only removes the trailing white space at the end of our classes
				//  @Example
				//   class="1 2 3 " ... becomes ... class="1 2 3"
				// @Reference
				//  trim()->http://php.net/manual/en/function.trim.php
				
				$body_classes                  = trim("$outer_and_position $layout_class");
				
				$content_class                 = trim($content_class);
				
				$no_sidebar_content_classes    = trim($no_sidebar_content_classes);
				
				$left_sidebar_content_classes  = trim($left_sidebar_content_classes);
				$left_sidebar_classes          = trim($left_sidebar_classes);
				
				$right_sidebar_content_classes = trim($right_sidebar_content_classes);
				$right_sidebar_classes         = trim($right_sidebar_classes);
				
				$dual_sidebar_content_classes  = trim($dual_sidebar_content_classes);
				$dual_left_sidebar_classes     = trim($dual_left_sidebar_classes);
				$dual_right_sidebar_classes    = trim($dual_right_sidebar_classes);
				
			}
			
			// ----------------------------------------------------------------------------------------------------
			// Start our layout
			// ----------------------------------------------------------------------------------------------------
			
			// Set our various $layout_type condition variables
			
			$no_sidebar_layout    = ($layout_type == 'no_sidebar')    ? TRUE : FALSE;
			$left_sidebar_layout  = ($layout_type == 'left_sidebar')  ? TRUE : FALSE;
			$right_sidebar_layout = ($layout_type == 'right_sidebar') ? TRUE : FALSE;
			$dual_sidebar_layout  = ($layout_type == 'dual_sidebar')  ? TRUE : FALSE;
			
			// ----------------------------------------------------------------------------------------------------
			// Start our before body
			// ----------------------------------------------------------------------------------------------------
			
			// Set our this action hook variable
			
			theme()->hook->do_action('before_main');
			
			// If our show title layout is TRUE
			
			if ($show_title) { self::get_filtered_output('title', array('title'), 'use_container'); }
			
			// Set our this action hook variable
			
			theme()->hook->do_action('before_outer_body');
			
			// ----------------------------------------------------------------------------------------------------
			// Start our body
			// ----------------------------------------------------------------------------------------------------
			
			// Set our this action hook variable
			
			if (theme()->hook->has_action('body')) { theme()->hook->do_action('body'); }
			
			// If our developer action does not exist load our body
			
			else {
				
				// Load our beginning HTML components
				
				theme()->output->div('id', 'body', "body {$body_classes}");
					
					// Set our this action hook variable
					
					theme()->hook->do_action('before_inner_body');
					
					// If our above variable is TRUE
					
					if ($above) { self::get_filtered_output('above_body', array('above_body')); }
					
					// Set our this action hook variable
					
					theme()->hook->do_action('before_layout');
					
					// Set our this action hook variable
					
					if (theme()->hook->has_action('layout')) { theme()->hook->do_action('layout'); }
					
					// If our developer action does not exist load our layout
					
					else {
						
						theme()->hook->do_action('before_' . $layout_type . '_layout');
						
						// If our no sidebar layout variable is TRUE
						
						if ($no_sidebar_layout) {
							
							self::layout_section('content', 'content', $no_sidebar_content_classes, $content_class);
							
						}
						
						// If our left sidebar layout variable is TRUE
						
						elseif ($left_sidebar_layout) {
							
							self::layout_section('sidebar', $layout_type, $left_sidebar_classes, $row_classes);
							
							self::layout_section('content', 'content', $left_sidebar_content_classes, $content_class);
							
						}
						
						// If our left sidebar layout variable is TRUE
						
						elseif ($right_sidebar_layout) {
							
							self::layout_section('content', 'content', $right_sidebar_content_classes, $content_class);
							
							self::layout_section('sidebar', $layout_type, $right_sidebar_classes, $row_classes);
							
						}
						
						// If our dual sidebar layout variable is TRUE
						
						elseif ($dual_sidebar_layout) {
							
							self::layout_section('sidebar', $layout_type, $dual_left_sidebar_classes, $row_classes);
							
							self::layout_section('content', 'content', $dual_sidebar_content_classes, $content_class);
							
							self::layout_section('sidebar', $layout_type, $dual_right_sidebar_classes, $row_classes);
							
						}
						
						theme()->hook->do_action('after_' . $layout_type . '_layout');
						
					}
					
					// Set our this action hook variable
					
					theme()->hook->do_action('after_layout');
					
					// If our below variable is TRUE
					
					if ($below) { self::get_filtered_output('below_body', array('below_body')); }
					
					// Set our this action hook variable
					
					theme()->hook->do_action('after_inner_body');
					
				// Load our ending HTML components
				
				theme()->output->div('close');
				
			}
			
			// ----------------------------------------------------------------------------------------------------
			// Start our after body
			// ----------------------------------------------------------------------------------------------------
			
			// Set our this action hook variable
			
			theme()->hook->do_action('after_outer_body');
			
			// Set our this action hook variable
			
			theme()->hook->do_action('after_main');
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->layout_section()
		// @Description
		//  This function 
		// @Usage
		//  self::layout_section($context, $index_id, $class_classes, $id_classes);
		//  self::layout_section($context, $index_id, $class_classes, $id_classes);
		//  self::layout_section($context, $index_id, $class_classes, $id_classes);
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_sidebar_layout = 2.0;
		public function layout_section($context, $index_id = '', $class_classes = '', $id_classes = '') {
			
			// If our $context variable is sidebar
			
			if ($context == 'sidebar') {
				
				theme()->output->div('class', $class_classes);
					
					// @AutoHook->add_filter('CustomTheme-$index_id_row_classes');
					// @Hook->add_filter('CustomTheme-left_sidebar_row_classes');
					// @Hook->add_filter('CustomTheme-right_sidebar_row_classes');
					// @Output
					//  <div class="$class_classes">
					//   @Example <div class="column span-4 desktop-4 mobile-4 phone-12">
					//  <div id="$type_sidebar" class="$id_classes"> @Example
					//   @Example <div id="left_sidebar" class="row center">
					//  </div>
					//  </div>
					
					$this_filter = $index_id . '_row_classes';
					
					$id_classes = theme()->hook->filter($this_filter, FALSE, $id_classes);
					
					theme()->output->div('id', $index_id, trim("$index_id $id_classes"));
						
						self::get_filtered_output('above_' . $index_id, array('above_' . $index_id));
						self::get_filtered_output($index_id, array($index_id));
						self::get_filtered_output('below_' . $index_id, array('below_' . $index_id));
						
					theme()->output->div('close');
					
				theme()->output->div('close');
				
			}
			
			// If our $context variable is content
			
			elseif ($context == 'content') {
				
				theme()->output->div('class', $class_classes);
					
					theme()->output->div('id', $index_id, $id_classes);
						
						// @Todo->reconsider_this_whole_thing_but_also_$args_for_get_post_content_function
						
						self::get_post_content('post_content');
						
					theme()->output->div('close');
					
				theme()->output->div('close');
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_filtered_output()
		// @Description
		//  This function handles just passing some $args to our self::get_output() function
		//   and applies some dyanamic filters along the way
		// @Usage
		//  self::get_filtered_output('top_banner', array('top_banner'), TRUE);
		//  This will output $post_id post_content from the option data top_banner wrapping it in a container div
		//  self::get_filtered_output('header', array('above_header', header', 'below_header'), FALSE, 'header');
		//  This will output $post_id post_content from the option data above_header, header, above_header and in that order on the site's frontend
		//   wrapping it in the <header></header> tags (ie: last $arg triggers this tag wrapping)
		//  self::get_filtered_output('footer', array('above_footer', footer', 'below_footer'), FALSE, 'footer');
		//  This will output $post_id post_content from the option data above_footer, footer, below_footer and in that order on the site's frontend
		//   wrapping it in the <footer></footer> tags (ie: last $arg triggers this tag wrapping)
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_filtered_output = 1.0;
		public function get_filtered_output($id_name = '', $output_values = array(), $container = FALSE, $tag = '') {
			
			// Set our $filter_id variable to our $id_name variable for ideal language usage
			
			$filter_id = $id_name;
			
			// Set our $output_values variable
			
			$output_values = theme()->hook->array_filter($filter_id, FALSE, $output_values);
			
			// Load our content output
			
			self::get_output(theme()->dev->option_name($option_name), $indexes, $id_name, $output_values, $tag, $container);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_output()
		// @Description
		//  This function just handles to determine wether the option data value should be retrieved
		//   the current post, custom layout or default layout settings
		// @Usage
		//  self::get_output('__settings__', 'Index_1, Index_2', 'my-css-id', array('above_header', header', 'below_header'), FALSE, FALSE);
		//  * See function code for more details
		// @Todo->add_support_for_output_method
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_output = 8.0;
		public function get_output($option_name = '', $indexes = '', $css_identifier = '', $output_keys = array(), $tag_name = '', $container = TRUE, $layout_id = '') {
			
			// Set our various layout data variables
			
			$default = self::get_layout('data', 'default');
			$custom  = self::get_layout('data');
			$current = self::get_layout('data', 'current');
			
			// Set our various $output_data variables
			
			$output_data = (!empty($default)) ? $default : '';
			$output_data = (!empty($custom))  ? $custom  : $output_data;
			$output_data = (!empty($current)) ? $current : $output_data;
			
			// Pass our args recieved into our get content function
			
			self::get_content($output_data, $css_identifier, $output_keys, $tag_name, $container);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_content()
		// @Description
		//  This function gets our final content output. It runs a foreach based on the $post_id's saved/chosen by the user.
		//  This function handles all of the final output of all our custom post type content, ie: Theme Header, Footer, Sidebars, etc.
		//  * See function code for more details
		// @Usage
		//  $this->($setting_status, 'my-css-class', array('post_ids'), 'optional-div-tag-name', 'use-optional-div-container');
		//  $this->($setting_status, 'my-css-class', array('post_ids'), TRUE, FALSE);
		//  $this->($setting_status, 'my-css-class', array('post_ids'), FALSE, FALSE);
		//  $this->($setting_status, 'my-css-class', array('post_ids'), TRUE, TRUE);
		// @Todo->think_of_removing_theme()->hook->has_action_calls_for_just_theme()->hook->do_action_calls
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_content = 10.0;
		public function get_content($setting_status = '', $css_identifier = '', $output_keys = array(), $tag_name = '', $container = TRUE) {
			
			// Turn on output buffering
			
			ob_start();
			
			// Set our our valid output variable to FALSE (as a default)
			// @Note
			//  In our foreach below we will determine if we have a valid output
			//   If we do we will set our valid output variable to TRUE
			
			$valid_output = FALSE;
			
			// If our use tag name variable is TRUE
			
			if (!empty($tag_name)) { theme()->output->div('tag', 'open', $tag_name); }
			
			// Set and sanitize our css id identifier variable
			
			$css_id = str_replace('_', '', $css_identifier);
			$css_id = str_replace(' ', '', $css_id);
			
			// Set and sanitize our css class identifier variable
			
			$css_class = str_replace('_', '-', $css_identifier);
			
			// Set our row classes variable
			
			$row_classes = theme()->output->flexbox($setting_status, 'row');
			
			// Set our css classes variable
			
			$css_classes = "$css_class $row_classes";
			
			// If our container variable is TRUE load our HTML beginning components
			
			if ($container) { theme()->output->div('id', $css_id, $css_classes); }
			
			// For each output key passed let's get the value of that key and determine if we need to output its post content
			
			foreach($output_keys as $output_value) {
				
				// Set our this action to our output value for ideal language usage
				
				$this_action = $output_value;
				
				// If our developer's custom action exist
				
				if (theme()->hook->has_action('before_outer_' . $this_action)) {
					
					// @AutoHook->add_filter('CustomTheme-before_outer_$this_action');
					// @Note
					//  When loading a custom action it will be wrapped in a div using the defaults of theme()->output->class_filter()
					// @Example
					//  add_filter('CustomTheme-before_outer_above_header', array(6, 8, 12, 12, 'my-custom-class');
					// @Output
					//  <div class="column span-6 desktop-8 mobile-12 phone-12 my-custom-class">
					
					// Load our developer action hook
					
					theme()->hook->do_action('before_outer_' . $this_action, FALSE, FALSE, array());
					
				}
				
				// If our developer's custom action does exist
				// @Note
				//  This means proceed with outputting the developer's custom action
				// @Example
				//  If add_action('CustomTheme-above_header'); exist and is to be loaded by choice of the user
				//   then instead of their content from the page builder loading your methods from your action will load instead
				//  So essentially when the user chooses "Above Header" you can load a custom header
				//   This means you don't have to add a new theme option for the user to choose just to run a custom output
				//   So Above Header essentially becomes your option instead of the default theme's option :D
				//  In theory this means for simple operations developer's won't have to worry about theme options
				//   and getting meta values from our options just to override our basic output options
				
				if (theme()->hook->has_action($this_action)) {
					
					// @AutoHook->add_filter('CustomTheme-$this_action');
					// @Example
					//  add_filter('CustomTheme-above_header', array(6, 8, 12, 12, 'my-custom-class');
					
					// Load our developer action hook
					
					theme()->hook->do_action($this_action, FALSE, FALSE, array());
					
				}
				
				// If our developer's custom action does not exist
				// @Note
				//  This means proceed with outputting our user's selected content
				
				else {
					
					// If our setting status output variable is not empty (ie: a valid option)
					
					if (!empty($setting_status[$output_value])) {
						
						// Set our this content variable
						
						$this_content = $setting_status[$output_value];
						
						// If our this content variable is numeric
						// @Note
						//  Our is_numeric() check is just to distinguish the content from other saved values
						//   as the value saved should be the $post_id number saved
						// @Maybe->consider_another_method_so_our_content_process_can_run_for_other_saved_values_by_developers_not_just_numerical_values
						
						if (is_numeric($this_content)) {
							
							// Load our developer action hook
							
							theme()->hook->do_action($this_action . '_is_live');
							theme()->hook->do_action($this_action . '_is_active');
							
							// Set our various class variables
							
							$column_classes = theme()->output->flexbox($setting_status, 'column');
							$general_class  = $output_value;
							$unique_class   = theme()->output->div('unique_class', $this_content);
							
							// Load our beginning HTML components
							
							theme()->output->div('class', trim("$column_classes $general_class $unique_class"));
								
								// If our developer's custom action does exists
								
								if (theme()->hook->has_action('before_inner_' . $this_action)) {
									
									// @AutoHook->add_filter('CustomTheme-before_inner_$this_action');
									// @Example
									//  add_filter('CustomTheme-before_inner_above_header', array(6, 8, 12, 12, 'my-custom-class');
									
									// Load our developer action hook
									
									theme()->hook->do_action('before_inner_' . $this_action, FALSE, FALSE, array());
									
								}
								
								// Process and output the content from our user's supported Page Builders
								// @Todo->!IMPORTANT_add_beaver_builder_support
								
								self::process('KC', $this_content);
								self::process('VC', $this_content);
								self::process('CS', $this_content);
								self::process('DB', $this_content);
								self::process('FB', $this_content);
								
								// If our developer's custom action does exists
								
								if (theme()->hook->has_action('after_inner_' . $this_action)) {
									
									// @AutoHook->add_filter('CustomTheme-after_inner_$this_action');
									// @Example
									//  add_filter('CustomTheme-after_inner_above_header', array(6, 8, 12, 12, 'my-custom-class');
									
									// Load our developer action hook
									
									theme()->hook->do_action('after_inner_' . $this_action, FALSE, FALSE, array());
									
								}
								
							// Load our ending HTML components
							
							theme()->output->div('close');
							
							// Set our our valid output variable to TRUE
							
							$valid_output = TRUE;
							
						}
						
						// If our this content variable is disable do nothing
						
						elseif ($this_content == 'disabled') {
							
							// Load our developer action hook
							// @AutoHook->add_action('CustomTheme-$index_key_is_disabled');
							
							theme()->hook->do_action($this_action . '_is_disabled');
							
						}
						
						// If our this content variable is default
						// @Todo->revisit_rethink_reconsider
						// @Note
						//  With our new $this->option() method the "default" value never actually gets returned
						
						elseif ($this_content == 'default') {}
						
					}
					
				}
				
				// If our developer's custom action does exists
				
				if (theme()->hook->has_action('after_outer_' . $this_action)) {
					
					// @AutoHook->add_filter('CustomTheme-after_outer_$this_action');
					// @Example
					//  add_filter('CustomTheme-after_outer_above_header', array(6, 8, 12, 12, 'my-custom-class');
					
					// Load our developer action hook
					
					theme()->hook->do_action('after_outer_' . $this_action, FALSE, FALSE, array());
					
				}
				
			}
			
			// If our container variable is TRUE load our HTML ending components
			
			if ($container) { theme()->output->div('close'); }
			
			// If our use tag name variable is TRUE
			
			if (!empty($tag_name)) { theme()->output->div('tag', 'close', $tag_name); }
			
			// Set our output variable to store our function's content
			
			$output = ob_get_contents();
			
			// Erase and turn off output buffering
			
			ob_end_clean();
			
			// If our valid output variable is TRUE let's echo our output variable
			
			if ($valid_output) { echo $output; }
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_post_content()
		// @Description
		//  This function is an extensive hook filled wrapper with optional $args for Wordpress's 
		//   while (have_posts()): the_post(); the_content(); endwhile;
		// @Usage
		//  self::get_post_content('optional-hook-id');
		//  * See function code for more details
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_post_content = 3.0;
		public function get_post_content($special_hook = FALSE) {
			
			// If our developer's custom action does exists
			
			if (theme()->hook->has_action($special_hook)) {
				
				// @AutoHook->add_filter('CustomTheme-$special_hook');
				// @Example
				//  add_filter('CustomTheme-above_header', array(6, 8, 12, 12, 'my-custom-class');
				
				// Load our developer action hook
				
				theme()->hook->do_action($special_hook, FALSE, FALSE, array());
				
			}
			
			// If our developer's custom action does not exist
			
			else {
				
				// Set our special hook status variable
				
				$special_hook_status = ($special_hook) ? theme()->hook->do_action($special_hook) : FALSE;
				
				// ----------------------------------------------------------------------------------------------------
				// If our $special_hook variable is TRUE of some value
				// ----------------------------------------------------------------------------------------------------
				
				if ($special_hook) {
					
					// Load our developer action hooks
					
					theme()->hook->do_action($special_hook . '_is_live');
					theme()->hook->do_action('special_post_content_hook_is_live');
					
					// Set our this filter and this action variables
					
					$this_filter = $special_hook;
					$this_action = $special_hook;
					
					// If our developer's custom action does exists
					// @AutoHook->add_filter('CustomTheme-before_outer_$special_hook');
					
					theme()->hook->do_action('before_outer_' . $this_action);
					
					// Set our $row_classes variable
					// @AutoHook->add_filter('CustomTheme-$special_hook_row_classes');
					// @Example
					//  add_filter('CustomTheme-$special_hook_row_classes');
					
					$row_classes = theme()->hook->filter($this_filter . '_row_classes', FALSE, 'row center');
					
					// Open our special hook div
					
					theme()->output->div('class', $row_classes);
					
					// If our developer's custom action does exists
					// @AutoHook->add_filter('CustomTheme-before_inner_$special_hook');
					
					theme()->hook->do_action('before_inner_' . $this_action);
					
				}
				
				// If our $special_hook variable is FALSE
				
				else { theme()->hook->do_action('special_post_content_hook_is_disabled'); }
				
				// ----------------------------------------------------------------------------------------------------
				// Load our post content and post content actions
				// ----------------------------------------------------------------------------------------------------
				
				// Set our $this_content variable
				
				$this_content = 'post_content';
				
				// Set our $this_action variable
				
				$this_action = $this_content;
				
				// If our developer's custom action does exists
				// @AutoHook->add_filter('CustomTheme-before_$special_hook');
				
				theme()->hook->do_action('before_' . $this_action);
				
				// If our developer's custom action does exists
				// @Todo->test_against_!theme()->hook->do_action()_returning_FALSE_to_simplify_code
				
				if (theme()->hook->has_action($this_action)) {
					
					// @Example
					//  add_filter('CustomTheme-post_content', array(6, 8, 12, 12, 'my-custom-class');
					
					// Load our developer action hook
					
					theme()->hook->do_action($this_action, FALSE, FALSE, array());
					
				}
				
				// If our developer hook does not exist
				
				else {
					
					// Load our Wordpress post content
					
					while (have_posts()): the_post(); the_content(); endwhile;
					
				}
				
				// If our developer's custom action does exists
				// @AutoHook->add_filter('CustomTheme-after_$special_hook');
				
				theme()->hook->do_action('after_' . $this_action);
				
				// ----------------------------------------------------------------------------------------------------
				// If our $special_hook variable is TRUE
				// ----------------------------------------------------------------------------------------------------
				
				if ($special_hook) {
					
					// Set our this action variable
					
					$this_action = $special_hook;
					
					// If our developer's custom action does exists
					// @AutoHook->add_filter('CustomTheme-after_inner_$special_hook');
					
					theme()->hook->do_action('after_inner_' . $this_action);
					
					// Close our special hook div
					
					theme()->output->div('close');
					
					// If our developer's custom action does exists
					// @AutoHook->add_filter('CustomTheme-after_outer_$special_hook');
					
					theme()->hook->do_action('after_outer_' . $this_action);
					
				}
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->process()
		// @Description
		//  This function simply process post type content for the page builder $arg passed. 
		//   For now this is good because Page builders have different output methods.
		// @Usage
		//  $this->process('KC', $post_id); outputs King Composer content
		//  $this->process('VC', $post_id); outputs Visual Composer content
		//  $this->process('CS', $post_id); outputs Cornerstone content
		// @Todo-> see about one process for all Page Builders. I have an idea :D But will have to rethink 
		//   page builder detection hooks created when specifc page builder used
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_process = 1.0;
		public function process($page_builder = '', $post_id = '', $output_method = '') {
			
			// Get the post id of the content we need to process and set that as our selected content variable
			
			$selected_content = get_post($post_id);
			
			// Set our this page builder variable for ideal lanugage usage in our developer action hooks
			
			$this_page_builder = $page_builder;
			
			// King Composer
			//  If our page builder variable is KC
			
			if ($this_page_builder == 'KC') {
				
				// If King Composer is active
				
				if (theme()->third_party->is_active('KC')) {
					
					// Load our developer action hook
					
					theme()->hook->do_action('content_is_' . $this_page_builder, '', '', FALSE);
					
					// Process our content as needed
					
					$processed_content = kc_raw_content($selected_content);
					$finished_content  = kc_do_shortcode($processed_content);
					
				}
				
			}
			
			// Visual Composer
			//  If our page builder variable is VC
			
			elseif ($this_page_builder == 'VC') {
				
				// If Visual Composer is active
				
				if (theme()->third_party->is_active('VC')) {
					
					// Load our developer action hook
					
					theme()->hook->do_action('content_is_' . $this_page_builder, '', '', FALSE);
					
					// Process our content as needed
					
					
					
				}
				
			}
			
			// Cornerstone
			//  If our page builder variable is CS
			
			elseif ($this_page_builder == 'CS') {
				
				// If Cornerstone is active
				
				if (theme()->third_party->is_active('CS')) {
					
					// Load our developer action hook
					
					theme()->hook->do_action('content_is_' . $this_page_builder, '', '', FALSE);
					
					// Process our content as needed
					
					
					
				}
				
			}
			
			// Divi Builder
			//  If our page builder variable is DB
			
			elseif ($this_page_builder == 'DB') {
				
				// If Divi Builder is active
				
				if (theme()->third_party->is_active('DB')) {
					
					// Load our developer action hook
					
					theme()->hook->do_action('content_is_' . $this_page_builder, '', '', FALSE);
					
					// Process our content as needed
					
					
					
				}
				
			}
			
			// Fusion Builder
			//  If our page builder variable is FB
			
			elseif ($this_page_builder == 'FB') {
				
				// If Fusion Builder is active
				
				if (theme()->third_party->is_active('FB')) {
					
					// Load our developer action hook
					
					theme()->hook->do_action('content_is_' . $this_page_builder, '', '', FALSE);
					
					// Process our content as needed
					
					
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($finished_content, $output_method, 'echo');
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->menu()
		// @Description
		//  This function is a simpler wrapper for checking if a menu is selected and to output it
		//   @ https://developer.wordpress.org/reference/functions/wp_nav_menu/
		//   @ https://developer.wordpress.org/reference/functions/has_nav_menu/
		// @Usage
		//  $this->menu('header-menu'); outputs the header menu only if enabled/selected by the user
		//  $this->menu('footer-menu'); outputs the footer menu only if enabled/selected by the user
		//  $this->menu('primary-menu'); outputs the primary menu only if enabled/selected by the user
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_menu = 1.0;
		public function menu($menu_location_id = '', $custom_menu_class = '', $output_method = '') {
			
			// If our menu location id is selected by our user
			
			if (has_nav_menu($menu_location_id)) {
				
				// Set our default menu variables
				
				$default_menu_class = str_replace('_', '-', $menu_location_id);
				
				// Set our various menu status variables
				
				$menu_class_status = (!empty($custom_menu_class)) ? $custom_menu_class : $default_menu_class;
				
				// Set our menu status variable
				
				$menu_status = wp_nav_menu(array('theme_location' => $menu_location_id, 'menu_class' => "menu {$menu_class_status}"));
				
			}
			
			// Load our data method
			
			theme()->dev->data($menu_status, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
