<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Frontend_Output_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Frontend_Output_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->html()
		// @Description
		//  This function is a simple helper for outputting HMTL (comments, css and js tags)
		//  This allows us to write in PHP instead of PHP and HTML causing ugly code writing and inconsistencies
		// @Usage
		//  $this->html('comment', 'start', 'My_Custom_HTML_Comment_Here');
		//  $this->html('comment', 'end', 'My_Custom_HTML_Comment_Here');
		//  $this->html('css', 'open', 'My_Custom_Css_ID_Name_Here');
		//  $this->html('css', 'close', 'My_Custom_Css_ID_Name_Here');
		//  $this->html('js', 'open', 'My_Custom_Js_ID_Name_Here');
		//  $this->html('js', 'close', 'My_Custom_Js_ID_Name_Here');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_html = 3.0;
		public function html($type = '', $operation_type = '', $name = '', $var_1 = '') {
			
			// If our type variable is doc
			// @Todo->reconsider_moving_to_output
			
			if ($type == 'doc') {
				
				// If our operation type variable is begin
				
				if ($operation_type == 'begin') {
					
					// Load our beginning HTML document
					
					echo '<!DOCTYPE html>' . PHP_EOL;
					echo '<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->' . PHP_EOL;
					echo '<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->' . PHP_EOL;
					echo '<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->' . PHP_EOL;
					echo '<!--[if gt IE 8]>' . PHP_EOL;
					echo '<!-->' . PHP_EOL;
					
					echo "<html class=\"no-js" . language_attributes() . "\">" . PHP_EOL;
					
					echo '<!--<![endif]-->' . PHP_EOL;
					
					// Load our head
					
					echo '<head>' . PHP_EOL;
					
					// If our developer's custom action hook doesn't exist
					
					if (!theme()->hook->has_action('site_meta')) {
						
						// Load our meta
						
						echo "<meta charset=\"" . bloginfo('charset') . "\"/>" . PHP_EOL;
						echo '<meta http-equiv="X-UA-Compatible" content="IE=edge"/>' . PHP_EOL;
						echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">' . PHP_EOL;
						
						// @Todo->add_custom_options
						//  Through our theme()->data->option() for per page, post, context results ;) <3 <3 <3
						
					}
					
					// If our developer's custom action hook does exist
					
					else {
						
						// Load developer's custom action hook
						
						theme()->hook->do_action('site_meta') . PHP_EOL;
						
					}
					
					// Load our other normal Wordpress stuff
					// @Todo_consider_theme_options_to_remove
					
					echo '<link rel="profile" href="http://gmpg.org/xfn/11"/>' . PHP_EOL;
					echo "<link rel=\"pingback\" href=\"" . bloginfo('pingback_url') . "\"/>" . PHP_EOL;
					
					// Load our Wordpress header function/hook
					
					wp_head() . PHP_EOL;
					
					// Close out head
					
					echo '</head>' . PHP_EOL;
					
					// Load our body tag using the Wordpress body_class() function so that it is hookable
					
					echo "<body " . body_class(self::header_layout() . self::body_classes()) . ">" . PHP_EOL;
					
					// If our developer's custom action hook exists load it
					
					if (theme()->hook->has_action('before_outer_site')) { theme()->hook->do_action('before_outer_site'); }
					
					// Start loading our Theme's HTML Markup
					
					// Set our $this_class variable
					
					$this_class = 'hfeed';
					
					// If our developer filter hook exists
					
					if (theme()->hook->has_filter($this_class)) {
						
						// Reset our $this_class variable
						
						$this_class = theme()->hook->add_filter($this_class);
						
					}
					
					echo "<div id=\"site\" class=\"$this_class\">" . PHP_EOL;
					
					// Set our $this_class variable
					
					$this_class = 'site';
					
					// If our developer filter hook exists
					
					if (theme()->hook->has_filter($this_class)) {
						
						// Reset our $this_class variable
						
						$this_class = theme()->hook->add_filter($this_class);
						
					}
					
					echo "<div class=\"$this_class\">" . PHP_EOL;
					
					// Load our developer action hook
					
					if (theme()->hook->has_action('before_inner_site')) { theme()->hook->do_action('before_inner_site'); }
					
				}
				
				// If our operation type variable is end
				
				elseif ($operation_type == 'end') {
					
					// If our developer's custom action hook exists load it
					
					if (theme()->hook->has_action('after_inner_site')) { theme()->hook->do_action('after_inner_site'); }
					
					// Load our ending HTML components
					
					echo '</div>' . PHP_EOL;
					
					echo '</div>' . PHP_EOL;
					
					// If our developer's custom action hook exists load it
					
					if (theme()->hook->has_action('after_inner_site')) { theme()->hook->do_action('after_inner_site'); }
					
					// Load our Wordpress footer function/action
					
					wp_footer();
					
					// Load our ending HTML document
					
					echo '</body>' . PHP_EOL;
					
					echo '</html>';
					
				}
				
			}
			
			// If our type variable is comment
			// @Note
			//  @Discontinued for performance sake
			
			elseif ($type == 'comment') {
				
				// Rename our name variable to comment for more appropiate launguage use
				
				$comment = $name;
				
				// If our operation variable is start
				
				if ($operation_type == 'start') {
					
					// Output our comment div
					
					echo PHP_EOL . "<!-- - - - - - - - - - - - {$comment} - - - - - - - - - - - -->" . PHP_EOL;
					
				}
				
				// If our operation variable is end
				
				elseif ($operation_type == 'end') {
					
					// Output our comment div
					
					echo PHP_EOL . "<!-- {$comment} -->" . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is css
			
			elseif ($type == 'css') {
				
				// If our name variable is not empty
				
				if (!empty($name)) {
					
					// Set our variable public name variables
					
					$public_name = strtoupper($name);
					$public_name = str_replace('-', ' ', $public_name);
					$public_name = str_replace('_', ' ', $public_name);
					
					// Set our variable style id name variables
					
					$style_id_name = strtolower($name);
					$style_id_name = str_replace(' ', '-', $style_id_name);
					$style_id_name = str_replace('_', '-', $style_id_name);
					
					// If our operation type variable is open
					
					if ($operation_type == 'open') {
						
						// Load our open css container
						
						echo PHP_EOL;
						
						echo "<!-- - - - - - - - - - - - START \"CUSTOM THEME ". $public_name ."\" STYLES - - - - - - - - - - - -->";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<style type=\"text/css\" id=\"custom-theme-". $style_id_name ."-css\">";
						
						echo PHP_EOL;
						
					}
					
					// If our operation type variable is close
					
					elseif ($operation_type == 'close') {
						
						// Close our open css container
						
						echo PHP_EOL;
						
						echo "</style>";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<!-- END \"CUSTOM THEME ". $public_name ."\" STYLES -->";
						
						echo PHP_EOL.PHP_EOL;
						
					}
					
				}
				
			}
			
			// If our type variable is js
			
			elseif ($type == 'js') {
				
				// If our name variable is not empty
				
				if (!empty($name)) {
					
					// Set our variable public name variables
					
					$public_name = strtoupper($name);
					$public_name = str_replace('-', ' ', $public_name);
					$public_name = str_replace('_', ' ', $public_name);
					
					// Set our variable style id name variables
					
					$style_id_name = strtolower($name);
					$style_id_name = str_replace(' ', '-', $style_id_name);
					$style_id_name = str_replace('_', '-', $style_id_name);
					
					// If our operation type variable is open
					
					if ($operation_type == 'open') {
						
						// Load our open js container
						
						echo PHP_EOL;
						
						echo "<!-- - - - - - - - - - - - START \"CUSTOM THEME ". $public_name ."\" SCRIPTS - - - - - - - - - - - -->";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<script type=\"text/javascript\" id=\"custom-theme-". $style_id_name ."-js\">";
						
						echo PHP_EOL;
						
					}
					
					// If our operation type variable is close
					
					elseif ($operation_type == 'close') {
						
						// Close our open js container
						
						echo PHP_EOL;
						
						echo "</script>";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<!-- END \"CUSTOM THEME ". $public_name ."\" SCRIPTS -->";
						
						echo PHP_EOL.PHP_EOL;
						
					}
					
				}
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->div()
		// @Description
		//  This function is a simple helper for outputting HMTL (comments, css and js tags)
		//  This allows us to write in PHP instead of PHP and HTML causing ugly code writing and inconsistencies
		// @Usage
		//  $this->html('tag', 'open', 'header'); outputs <header>
		//  $this->html('tag', 'close', 'header'); outputs </header>
		//  $this->html('tag', 'open', 'header', 'my-class', 'my-role'); outputs <header class="my-class" role="my-role">
		//  $this->html('id', 'my-div-id'); outputs <div id="my-div-id">
		//  $this->html('id', 'my-div-id', 'my-class'); outputs <div id="my-div-id" class="my-class">
		//  $this->html('class', 'my-class'); outputs <div class="my-class">
		//  $this->div('span', '6-12'); outputs 6 column width of 12 column grid
		//  $this->div('span', '6-12', '3-12'); outputs 6 column width of 12 column grid with 3 column offset
		//  $this->html('unique_class', $post_id); outputs <div class="post-title-post-id">
		//  $this->html('close'); outputs </div>
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_div = 2.0;
		public function div($type = '', $var_1 = '', $var_2 = '', $var_3 = '', $var_4 = '', $output_method = '') {
			
			// If our type variable is id
			
			if ($type == 'tag') {
				
				// Rename our needed variable for ideal language usage
				
				$type     = $var_1;
				$tag_name = $var_2;
				$classes  = $var_3;
				$role     = $var_4;
				
				// Convert our tag name underscores to none, if any
				
				$tag_name = str_replace('_', '', $tag_name);
				
				// Convert our tag name spaces to none, if any
				
				$tag_name = str_replace(' ', '', $tag_name);
				
				// If our type variable is open
				
				if ($type == 'open') {
					
					// Set our role variable
					
					$role = (!empty($role)) ? " role=\"$role\"" : '';
					
					// Set our classes variable
					
					$classes = (!empty($classes)) ? " class=\"$classes\"" : '';
					
					// Output our tag
					
					echo PHP_EOL . "<$tag_name$classes$role>" . PHP_EOL;
					
				}
				
				// If our type variable is close
				
				elseif ($type == 'close') {
					
					// Output our closing tag div
					
					echo PHP_EOL . "</$tag_name>" . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is id
			
			elseif ($type == 'id') {
				
				// Rename our needed variable for ideal language usage
				
				$div_id    = $var_1;
				$div_class = $var_2;
				
				// Convert our div id and div class underscores to hyphens, if any
				
				$div_id    = str_replace('_', '-', $div_id);
				$div_class = str_replace('_', '-', $div_class);
				
				$div_class = (!empty($div_class)) ? " class=\"$div_class\"" : '';
				
				// Output our div
				
				echo PHP_EOL . "<div id=\"$div_id\"$div_class>" . PHP_EOL;
				
			}
			
			// If our type variable is class
			
			elseif ($type == 'class') {
				
				// Rename our needed variable for ideal language usage
				
				$location = $var_1;
				$schema  = $var_2;
				
				// Convert our variable underscores to hyphens, if any
				
				$location = str_replace('_', '-', $location);
				
				// If our classes variable is empty
				
				if (empty($schema)) {
					
					// Output our div
					
					echo PHP_EOL . "<div class=\"$location\">" . PHP_EOL;
					
				}
				
				// If our classes variable is not empty
				
				else {
					
					// Output our div
					
					echo PHP_EOL . "<div class=\"$location $schema\">" . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is span
			// @Todo->reevaluate
			
			elseif ($type == 'span') {
				
				// Rename our needed variable for ideal language usage
				
				$span = $var_1;
				$push = $var_2;
				
				// Convert our span variable underscores to hyphens, if any
				
				$span = str_replace('_', '-', $span);
				
				// Convert our span variable spaces to hyphens, if any
				
				$span = str_replace(' ', '', $span);
				
				// If our push variable is empty
				
				if (empty($push)) {
					
					// Output our div span
					
					echo PHP_EOL . "<div class=\"content center\">" . PHP_EOL . PHP_EOL;
					echo PHP_EOL . "<div class=\"span-$span\">"     . PHP_EOL . PHP_EOL;
					
				}
				
				// If our push variable is not empty
				
				else {
					
					// Convert our push variable underscores to hyphens, if any
					
					$push = str_replace('_', '-', $push);
					
					// Convert our push variable spaces to hyphens, if any
					
					$push = str_replace(' ', '-', $push);
					
					// Output our div span
					
					echo PHP_EOL . "<div class=\"content center\">"        . PHP_EOL . PHP_EOL;
					echo PHP_EOL . "<div class=\"span-$span push-$push\">" . PHP_EOL . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is wrappers
			// @Todo->!IMPORTANT_reconsider_currently_I_think_not_being_used
			
			elseif ($type == 'wrappers') {
				
				// Rename our needed variable for ideal language usage
				
				$outer_attributes = $var_1;
				$inner_attributes = $var_2;
				
				echo PHP_EOL;
				
				// If our outer attributes variable is not empty
				
				if (!empty($outer_attributes)) {
					
					// Load our div
					
					echo "<div class=\"outer $outer_attributes\">" . PHP_EOL . PHP_EOL;
					
				}
				
				// If our outer attributes variable is empty
				
				else {
					
					// Load our div
					
					echo "<div class=\"outer\">" . PHP_EOL . PHP_EOL;
					
				}
				
				// If our inner attributes variable is not empty
				
				if (!empty($inner_attributes)) {
					
					// Load our div
					
					echo "<div class=\"inner $inner_attributes\">" . PHP_EOL . PHP_EOL;
					
				}
				
				// If our outer attributes variable is empty
				
				else {
					
					// Load our div
					
					echo "<div class=\"inner\">" . PHP_EOL . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is id
			
			elseif ($type == 'close') {
				
				// Output our div
				
				echo PHP_EOL . PHP_EOL . '</div>';
				
			}
			
			// If our type variable is unique_class
			
			elseif ($type == 'unique_class') {
				
				// Rename our needed variable for ideal language usage
				
				$post_id = $var_1;
				
				// Set our various variables to get our post title and pass it as part of the unique class
				
				$post       = get_post($post_id);
				$post_id    = $post->ID;
				$post_title = $post->post_title;
				$post_title = strtolower($post_title);
				$post_title = str_replace(' ', '-', $post_title);
				
				// Set our unique class variable
				
				$unique_class = "{$post_title}-{$post_id}";
				
				// Load our data method
				
				theme()->dev->data($unique_class, $output_method);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->schema()
		// @Description
		//  This function it a wrapper for returning the full markup for schema.org data
		//   When a type is entered it will return itemtype='http://schema.org/$type' itemscope='itemscope'
		// @Usage
		//  $this->div('class', 'my-class-here', $this->schema('WebPage'));
		//  outputs... <div class="my-class-here" itemtype="http://schema.org/WebPage" itemscope="itemscope">
		//  $this->schema('WebPage', TRUE);
		//  outputs... <div class="my-class-here" itemtype="http://schema.org/WebPage" itemscope="itemscope">
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_schema = 1.0;
		public function schema($type = '', $use_div = FALSE, $output_method = '')  {
			
			// If our developer's custom filter hook exists
			// @Note
			//  This automatically creates the filter hook add_filter('CustomTheme-$type-schema', 'Your_Function_Name');
			
			if (theme()->hook->has_filter("{$type}-schema")) {
				
				// Reset our $type variable to our developer's custom filter hook
				
				$type = theme()->hook->add_filter("{$type}-schema");
				
			}
			
			// If our $use_div variable is TRUE
			
			if ($use_div) {
				
				// Set our $schema_type variable
				
				$schema_type = "<div itemtype=\"" . theme()->global->http_method() . "schema.org/{$type}\" itemscope=\"itemscope\">" . PHP_EOL;
				
				// Load our data method
				
				theme()->dev->data($schema_type, $output_method, 'echo');
				
			}
			
			// If our $use_div variable is FALSE
			
			else {
				
				// Set our $schema_type variable
				
				$schema_type = "itemtype=\"" . theme()->global->http_method() . "schema.org/{$type}\" itemscope=\"itemscope\"";
				
				// Load our data method
				
				theme()->dev->data($schema_type, $output_method);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->flexbox()
		// @Description
		//  This function is a simple API to get flexbox grid properties from the option name index
		// @Usage
		//  $this->flexbox($setting_location, 'row');
		//  $this->flexbox($setting_location, 'column');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_flexbox = 1.0;
		public function flexbox($setting_location = '', $type = '', $output_method = '') {
			
			// If our type variable is row
			
			if ($type == 'row') {
				
				// Set our row variable
				
				$type     = 'row';
				$default  = 'row';
				$class    = (isset($setting_location[$type . '_class']));
				$class    = (!empty($class)) ? "$class $default " : "$default ";
				$classes .= $class;
				
				// Set our row position class
				
				$type     = 'row';
				$default  = 'center';
				$class    = (isset($setting_location[$type . '_position']));
				$class    = (!empty($class)) ? "$class " : "$default ";
				$classes .= $class;
				
				// Set our row class
				
				$type     = 'row';
				$default  = '';
				$class    = (isset($setting_location[$type . '_order']));
				$class    = (!empty($class)) ? "$class " : "$default ";
				$classes .= $class;
				
			}
			
			// If our type variable is column
			
			elseif ($type == 'column') {
				
				// Set our column class
				
				$type     = 'column';
				$default  = 'column';
				$class    = (isset($setting_location[$type . '_class']));
				$class    = (!empty($class)) ? "$class $default " : "$default ";
				$classes .= $class;
				
				// Set our default span class
				
				$type     = 'span';
				$default  = '12';
				$class    = (isset($setting_location[$type]));
				$class    = (!empty($class)) ? "$type-$class " : "$type-$default ";
				$classes .= $class;
				
				// Set our desktop span class
				
				$type     = 'desktop';
				$default  = '12';
				$class    = (isset($setting_location[$type . '_span']));
				$class    = (!empty($class)) ? "$type-$class " : "$type-$default ";
				$classes .= $class;
				
				// Set our mobile span class
				
				$type     = 'mobile';
				$default  = '12';
				$class    = (isset($setting_location[$type . '_span']));
				$class    = (!empty($class)) ? "$type-$class " : "$type-$default ";
				$classes .= $class;
				
				// Set our phone span class
				
				$type     = 'phone';
				$default  = '12';
				$class    = (isset($setting_location[$type . '_span']));
				$class    = (!empty($class)) ? "$type-$class " : "$type-$default ";
				$classes .= $class;
				
			}
			
			// If our $classes variable is not empty let's return it trim() otherwise FALSE
			
			$classes = (!empty($classes)) ? trim($classes) : FALSE;
			
			// Load our data method
			
			theme()->dev->data($classes, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->class_filter()
		// @Description
		//  This function is a simple wrapper for outputting our flexbox column class values
		// @Usage
		//  $this->div('class', $this->class_filter(array(1, 2, 3, 4, 'my-custom-class'));
		//   outputs <div class="column span-1 desktop-2 mobile-3 phone-4 my-custom-class">
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_class_filter = 1.0;
		public function class_filter($classes = array(), $output_method = '') {
			
			// Set our various class context variables
			
			$span    = (isset($classes[0])) ? $classes[0] : 12;
			$desktop = (isset($classes[1])) ? $classes[1] : 12;
			$mobile  = (isset($classes[2])) ? $classes[2] : 12;
			$phone   = (isset($classes[3])) ? $classes[3] : 12;
			$custom  = (isset($classes[4])) ? ' ' . $classes[4] : FALSE;
			
			// Set our classes variable
			
			$classes = "column span-{$span} desktop-{$desktop} mobile-{$mobile} phone-{$phone}{$custom}";
			
			// Load our data method
			
			theme()->dev->data($classes, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->header_layout()
		// @Description
		//  This function applies our body class "custom-theme" but also applies the header layout as a body class
		//   ie: <body class="center-header"> <body class="left-header"> <body class="right-header">
		// @Note
		//  body_class(self::header_layout()); within the Wordpress body_class() function
		//  Use the methods below to detect the output of this function. No need to use this function elsewhere.
		// @Usage (use theme()->data->option() for detection methods)
		//  if (theme()->data->option('header_layout', 'center')) { Your_Code_Here... }
		//  if (theme()->data->option('header_layout', 'left')) { Your_Code_Here... }
		//  if (theme()->data->option('header_layout', 'right')) { Your_Code_Here... }
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_header_layout = 7.0;
		public function header_layout($output_method = '') {
			
			// Set our mandatory body class
			
			$body_class = 'custom-theme';
			
			// Set our site layout variables
			
			$site_layout = theme()->data->option('site_layout', 'fullwidth');
			
			// Set our this class variable string
			
			$this_class = str_replace('_', '-', $site_layout);
			
			// Set our body class variable to the passed site layout type class value
			
			$body_class .= " {$this_class}";
			
			// Set our header layout variables
			
			$header_layout = theme()->data->option('header_layout', 'center');
			
			// If our header layout variable is not center
			
			if ($header_layout !== 'center') {
				
				// Set our this class variable string
				
				$this_class = str_replace('_', '-', $header_layout);
				
				// Set our body class variable to the passed header layout class value
				
				$body_class .= " {$this_class}-header";
				
			}
			
			// Load our data method
			
			theme()->dev->data($body_class, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->header_type()
		// @Description
		//  This function is a simple wrapper for outputting an HTTP header status
		//   @ http://php.net/manual/en/function.header.php
		// @Usage
		//  $this->header_type($content_type, $UTF);
		// @Todo->consider_removal_not_used
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_header_type = 1.0;
		public function header_type($content_type = '', $UTF = '', $output_method = '') {
			
			// Set our content type status variable
			
			$content_type_status = (!empty($content_type)) ? $content_type : 'text/html';
			
			// Set our UTF status variable
			
			$UTF_status = (!empty($UTF)) ? $UTF : 'UTF-8';
			
			// Set our header type variable
			
			$header_type = header("Content-type: {$content_type_status}; charset: {$UTF_status}");
			
			// Load our data method
			
			theme()->dev->data($header_type, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->body_classes()
		// @Description
		//  This function 
		//  body_class(self::body_classes()); within the Wordpress body_class() function
		// @Usage
		//  @Todo->create_when_theme_option_created
		// @Todo->make_theme_option
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_body_classes = 2.0;
		public function body_classes($output_method = '') {
			
			// Set our body class helpers variable
			
			$body_class_helpers  = 'margin-top margin-right margin-bottom margin-left';
			$body_class_helpers .= ' padding-top padding-right padding-bottom padding-left';
			$body_class_helpers .= ' border-top border-right border-bottom border-left';
			
			// Load our data method
			
			theme()->dev->data($body_class_helpers, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
