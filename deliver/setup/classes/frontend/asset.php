<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Frontend_Asset_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Frontend_Asset_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_css()
		// @Description
		//  This function runs a simple API to get various css styles from saved theme/post data
		//   outputted to the css selector $arg passed
		// @Usage
		//  $my_css_selector = 'my-css-selector-id';
		//  $css  = $this->get_css($setting_location, 'width', $my_css_selector);
		//  $css .= $this->get_css($setting_location, 'background', $my_css_selector);
		//  $css .= $this->get_css($setting_location, 'padding', $my_css_selector);
		//  $css .= $this->get_css($setting_location, 'margin', $my_css_selector);
		//  $css .= $this->get_css($setting_location, 'border', $my_css_selector);
		//  echo $css;
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_css_styles = 1.0;
		public function get_css($setting_location = '', $type = '', $css_selectors = '', $hover = FALSE) {
			
			// Rename our setting location variable to css location for ideal language use
			
			$css_location = $setting_location;
			
			// If our type variable is width
			
			if ($type == 'width') {
				
				// Set our various value related variables
				
				$width = $css_location[$type];
				
				// Set our various css related variables
				
				$these_styles = (!empty($width)) ? "   $type: $width;" . PHP_EOL : FALSE;
				
				// Set our this css variable
				
				$this_css .= "$css_selectors $these_styles}" . PHP_EOL . "";
				
			}
			
			// If our type variable is background
			
			elseif ($type == 'background') {
				
				// Set our various value related variables
				
				$color      = $css_location[$type.'_color'];
				$position   = $css_location[$type.'_position'];
				$repeat     = $css_location[$type.'_repeat'];
				$image      = $css_location[$type.'_image'];
				$attachment = $css_location[$type.'_attachment'];
				
				// Set our various css related variables
				
				$this_color      = (!empty($color)) ?      "	$type-color: $color;"           . PHP_EOL : FALSE;
				$this_image      = (!empty($image)) ?      "	$type-image: $image;"           . PHP_EOL : FALSE;
				$this_position   = (!empty($position)) ?   "	$type-position: $position;"     . PHP_EOL : FALSE;
				$this_repeat     = (!empty($repeat)) ?     "	$type-repeat: $repeat;"         . PHP_EOL : FALSE;
				$this_attachment = (!empty($attachment)) ? "	$type-attachment: $attachment;" . PHP_EOL : FALSE;
				
				// Set our these styles variable
				
				$these_styles = $this_color . $this_image . $this_position . $this_repeat . $this_attachment;
				
				// Set our this css variable
				
				$this_css .= "$css_selectors " . PHP_EOL . " {$these_styles}" . PHP_EOL . "";
				
			}
			
			// If our type variable is padding
			
			elseif ($type == 'padding') {
				
				// Set our various value related variables
				
				$top    = $css_location[$type.'_top'];
				$right  = $css_location[$type.'_right'];
				$bottom = $css_location[$type.'_bottom'];
				$left   = $css_location[$type.'_left'];
				
				// Set our various css related variables
				
				$this_top    = (!empty($top)) ?    "   $type-top: $top;"       . PHP_EOL : FALSE;
				$this_right  = (!empty($right)) ?  "   $type-right: $right;"   . PHP_EOL : FALSE;
				$this_bottom = (!empty($bottom)) ? "   $type-bottom: $bottom;" . PHP_EOL : FALSE;
				$this_left   = (!empty($left)) ?   "   $type-left: $left;"     . PHP_EOL : FALSE;
				
				// Set our these styles variable
				
				$these_styles = $this_top . $this_right . $this_bottom . $this_left;
				
				// Set our this css variable
				
				$this_css .= "$css_selectors " . PHP_EOL . " {$these_styles}" . PHP_EOL . "";
				
			}
			
			// If our type variable is margin
			
			elseif ($type == 'margin') {
				
				// Set our various value related variables
				
				$top    = $css_location[$type.'_top'];
				$right  = $css_location[$type.'_right'];
				$bottom = $css_location[$type.'_bottom'];
				$left   = $css_location[$type.'_left'];
				
				// Set our various css related variables
				
				$this_top    = (!empty($top)) ?    "   $type-top: $top;"       . PHP_EOL : FALSE;
				$this_right  = (!empty($right)) ?  "   $type-right: $right;"   . PHP_EOL : FALSE;
				$this_bottom = (!empty($bottom)) ? "   $type-bottom: $bottom;" . PHP_EOL : FALSE;
				$this_left   = (!empty($left)) ?   "   $type-left: $left;"     . PHP_EOL : FALSE;
				
				// Set our these styles variable
				
				$these_styles = $this_top . $this_right . $this_bottom . $this_left;
				
				// Set our this css variable
				
				$this_css .= "$css_selectors " . PHP_EOL . " {$these_styles}" . PHP_EOL . "";
				
			}
			
			// If our type variable is border
			
			elseif ($type == 'border') {
				
				// Set our various value related variables
				
				$top    = $css_location[$type.'_top'];
				$right  = $css_location[$type.'_right'];
				$bottom = $css_location[$type.'_bottom'];
				$left   = $css_location[$type.'_left'];
				
				// Set our various css related variables
				
				$this_top    = (!empty($top)) ?    "   $type-top: $top;"       . PHP_EOL : FALSE;
				$this_right  = (!empty($right)) ?  "   $type-right: $right;"   . PHP_EOL : FALSE;
				$this_bottom = (!empty($bottom)) ? "   $type-bottom: $bottom;" . PHP_EOL : FALSE;
				$this_left   = (!empty($left)) ?   "   $type-left: $left;"     . PHP_EOL : FALSE;
				
				// Set our these styles variable
				
				$these_styles = $this_top . $this_right . $this_bottom . $this_left;
				
				// Set our this css variable
				
				$this_css .= "$css_selectors " . PHP_EOL . " {$these_styles}" . PHP_EOL . "";
				
			}
			
			// If our type variable is shadow
			// @Todo->create
			
			elseif ($type == 'shadow') {
				
			}
			
			// If our this css variable is not empty let's return it otherwise FALSE
			
			return (!empty($this_css)) ? $this_css : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_layout_styles()
		// @Description
		//  This function runs a simple routine of our get_css() function
		//  This makes it simpler to output all our layout css needed from one function
		// @Todo->reconsider
		// @Usage
		//  $css = $this->get_layout_styles('default');
		//  $css = $this->get_layout_styles('custom');
		//  echo $css;
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_layout_styles = 1.0;
		public function get_layout_styles($context = '') {
			
			// If our context variable is empty or is default
			
			// Set our styles activated variable
			
			// $styles_activated = $css_location['activate_styles'];
			
			// Set our option name variable
			
			$option_name = $this->option_name;
			
			// If our context variable is empty or is default
			
			if (empty($context) or $context == 'default') {
				
				// ----------------------------------------------------------------------------------------------------
				// Headers
				// ----------------------------------------------------------------------------------------------------
				
				// Above Header
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.above-header';
				$this_index_option    = $this->get('all_data', $option_name, 'header, above_header');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Center Header
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.center-header';
				$this_index_option    = $this->get('all_data', $option_name, 'header, center_header');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Below Header
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.below-header';
				$this_index_option    = $this->get('all_data', $option_name, 'header, below_header');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// ----------------------------------------------------------------------------------------------------
				// Page, Post, Post Type Title Section
				// ----------------------------------------------------------------------------------------------------
				
				// Page, Post, Post Type Title Section
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.title';
				$this_index_option    = $this->get('all_data', $option_name, 'body, title');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// ----------------------------------------------------------------------------------------------------
				// Body
				// ----------------------------------------------------------------------------------------------------
				
				// Above Body
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.above-body';
				$this_index_option    = $this->get('all_data', $option_name, 'body, above_body');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Center Body
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.center-body';
				$this_index_option    = $this->get('all_data', $option_name, 'body, center_body');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Below Body
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.below-body';
				$this_index_option    = $this->get('all_data', $option_name, 'body, below_body');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// ----------------------------------------------------------------------------------------------------
				// Footer
				// ----------------------------------------------------------------------------------------------------
				
				// Above Footer
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.above-footer';
				$this_index_option    = $this->get('all_data', $option_name, 'footer, above_footer');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Center Footer
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.center-footer';
				$this_index_option    = $this->get('all_data', $option_name, 'footer, center_footer');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
				// Below Footer
				// Set our various variables and output any styles if we have them to be loaded
				
				$this_class           = '.below-footer';
				$this_index_option    = $this->get('all_data', $option_name, 'footer, below_footer');
				$these_section_styles = $this->get_css($this_index_option, 'width', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'background', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'padding', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'margin', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'border', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				$these_section_styles = $this->get_css($this_index_option, 'shadow', $this_class, FALSE);
				$user_generated_css  .= $these_section_styles;
				
			}
			
			// If our user generated css variable is not empty let's return it otherwise FALSE
			
			return (!empty($user_generated_css)) ? $user_generated_css : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_font_styles()
		// @Description
		//  This function is a simple API that makes it easier to output all font css needed from one function
		// @Usage
		//  $font_styles  = $this->get_font_styles($setting_location, 'h1', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'h2', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'h3', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'h4', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'h5', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'h6', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'p', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'a', TRUE);
		//  $font_styles .= $this->get_font_styles($setting_location, 'a:hover', TRUE);
		//  echo $font_styles;
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_font_styles = 1.0;
		public function get_font_styles($setting_location = '', $css_selectors = '', $hover = FALSE) {
			
			// Set our font activated font variable
			// @Todo->create
			// @Todo->maybe
			
			$font_activated = $setting_location['activate_font'];
			
			// If our font activated variable is TRUE
			
			if ($font_activated or $font_activated == 'TRUE') {
				
				// Set our this font variable to our setting location variable for ideal language usage
				
				$this_font = $setting_location;
				
				// ----------------------------------------------------------------------------------------------------
				// Before Hover
				// ----------------------------------------------------------------------------------------------------
				
				// Set our various font property variables
				
				$this_family     = $this_font['family'];
				$family          = (!empty($this_family))    ? "	font-family: $this_family;" . PHP_EOL : FALSE;
				
				$this_size       = $this_font['size'];
				$size            = (!empty($this_size))      ? "	font-size: $this_size;" . PHP_EOL : FALSE;
				
				$this_color      = $this_font['color'];
				$color           = (!empty($this_color))     ? "	color: $this_color;" . PHP_EOL : FALSE;
				
				$this_style      = $this_font['style'];
				$style           = (!empty($this_style))     ? "	font-style: $this_style;" . PHP_EOL : FALSE;
				
				$this_weight     = $this_font['weight'];
				$weight          = (!empty($this_weight))    ? "	font-weight: $this_weight;" . PHP_EOL : FALSE;
				
				$this_height     = $this_font['height'];
				$height          = (!empty($this_height))    ? "	line-height: $this_height;" . PHP_EOL : FALSE;
				
				$this_spacing    = $this_font['spacing'];
				$spacing         = (!empty($this_spacing))   ? "	letter-spacing: $this_spacing;" . PHP_EOL : FALSE;
				
				$this_stretch    = $this_font['stretch'];
				$stretch         = (!empty($this_stretch))   ? "	font-stretch: $this_stretch;" . PHP_EOL : FALSE;
				
				$this_variant    = $this_font['variant'];
				$variant         = (!empty($this_variant))   ? "	font-variant: $this_variant;" . PHP_EOL : FALSE;
				
				$this_transform  = $this_font['transform'];
				$transform       = (!empty($this_transform)) ? "	text-transform: $this_transform;" . PHP_EOL : FALSE;
				
				$this_overflow   = $this_font['overflow'];
				$overflow        = (!empty($this_overflow))  ? "	text-overflow: $this_overflow;" . PHP_EOL : FALSE;
				
				$this_align      = $this_font['align'];
				$align           = (!empty($this_align))     ? "	text-align: $this_align;" . PHP_EOL : FALSE;
				
				// @Todo
				//  Use this for custom css selectors
				//   Our css areas do not need custom indent property options
				// $this_indent     = $this_font['indent'];
				// $indent          = (!empty($this_indent))     ? "	text-indent: $this_indent;" . PHP_EOL : FALSE;
				
				$this_decoration = $this_font['decoration'];
				$decoration      = (!empty($this_decoration)) ? "	text-decoration: $this_decoration;" . PHP_EOL : FALSE;
				
				$this_shadow     = $this_font['shadow'];
				$shadow          = (!empty($this_shadow))     ? "	text-shadow: $this_shadow;" . PHP_EOL : FALSE;
				
				$this_align_last = $this_font['align_last'];
				$align_last      = (!empty($this_align_last)) ? "	text-align-last: $this_align_last;" . PHP_EOL . "	-moz-text-align-last: $this_align_last;" . PHP_EOL : FALSE;
				
				// Set our these font styles variable for ease of use
				
				$these_font_styles = "$family$size$color$style$weight$height$spacing$stretch$variant$transform$overflow$align$decoration$shadow$align_last";
				
				// Set our this font css variable
				
				$this_font_css .= "$css_selectors " . PHP_EOL . " {$these_font_styles}" . PHP_EOL . "";
				
			}
			
			// If our hover variable is TRUE
			
			if ($hover) {
				
				// ----------------------------------------------------------------------------------------------------
				// After Hover
				// ----------------------------------------------------------------------------------------------------
				
				// Set our various font property variables
				
				$this_family     = $this_font['family_hover'];
				$family          = (!empty($this_family))     ? "	font-family: $this_family;" . PHP_EOL : FALSE;
				
				$this_size       = $this_font['size_hover'];
				$size            = (!empty($this_size))       ? "	font-size: $this_size;" . PHP_EOL : FALSE;
				
				$this_color      = $this_font['color_hover'];
				$color           = (!empty($this_color))      ? "	color: $this_color;" . PHP_EOL : FALSE;
				
				$this_style      = $this_font['style_hover'];
				$style           = (!empty($this_style))      ? "	font-style: $this_style;" . PHP_EOL : FALSE;
				
				$this_weight     = $this_font['weight_hover'];
				$weight          = (!empty($this_weight))     ? "	font-weight: $this_weight;" . PHP_EOL : FALSE;
				
				$this_height     = $this_font['height_hover'];
				$height          = (!empty($this_height))     ? "	line-height: $this_height;" . PHP_EOL : FALSE;
				
				$this_spacing    = $this_font['spacing_hover'];
				$spacing         = (!empty($this_spacing))    ? "	letter-spacing: $this_spacing;" . PHP_EOL : FALSE;
				
				$this_stretch    = $this_font['stretch_hover'];
				$stretch         = (!empty($this_stretch))    ? "	font-stretch: $this_stretch;" . PHP_EOL : FALSE;
				
				$this_variant    = $this_font['variant_hover'];
				$variant         = (!empty($this_variant))    ? "	font-variant: $this_variant;" . PHP_EOL : FALSE;
				
				$this_transform  = $this_font['transform_hover'];
				$transform       = (!empty($this_transform))  ? "	text-transform: $this_transform;" . PHP_EOL : FALSE;
				
				$this_overflow   = $this_font['overflow_hover'];
				$overflow        = (!empty($this_overflow))   ? "	text-overflow: $this_overflow;" . PHP_EOL : FALSE;
				
				$this_align      = $this_font['align_hover'];
				$align           = (!empty($this_align))      ? "	text-align: $this_align;" . PHP_EOL : FALSE;
				
				// @Todo
				//  Use this for custom css selectors
				//   Our css areas do not need custom indent property options
				// $this_indent     = $this_font['indent_hover'];
				// $indent          = (!empty($this_indent))     ? "	text-indent: $this_indent;" . PHP_EOL : FALSE;
				
				$this_decoration = $this_font['decoration_hover'];
				$decoration      = (!empty($this_decoration)) ? "	text-decoration: $this_decoration;" . PHP_EOL : FALSE;
				
				$this_shadow     = $this_font['shadow_hover'];
				$shadow          = (!empty($this_shadow))     ? "	text-shadow: $this_shadow;" . PHP_EOL : FALSE;
				
				$this_align_last = $this_font['align_last_hover'];
				$align_last      = (!empty($this_align_last)) ? "	text-align-last: $this_align_last;" . PHP_EOL . "	-moz-text-align-last: $this_align_last;" . PHP_EOL : FALSE;
				
				// Set our these font styles variable for ease of use
				
				$these_font_styles = "$family$size$color$style$weight$height$spacing$stretch$variant$transform$overflow$align$decoration$shadow$align_last";
				
				// Set our this font css variable
				
				$this_font_css .= "$css_selectors:hover " . PHP_EOL . " {$these_font_styles}" . PHP_EOL . "";
				
			}
			
			// If our this font css variable is not empty let's return it otherwise FALSE
			
			return (!empty($this_font_css)) ? $this_font_css : FALSE;
			
		}
		
		// @Todo->consildate_and_remove
		
		public function wp_enqueue_style($cpt, $handle, $src = FALSE, $deps = array(), $ver = FALSE, $media = 'all') {
		// Check the admin page we are on.
		global $pagenow;
		// Default to NULL to prevent enqueuing.
		$enqueue = NULL;
		// Enqueue style only if we are on the correct CPT editor page.
		if (isset($_GET['post_type']) and $_GET['post_type'] == $cpt and $pagenow == "post-new.php") {
		$enqueue = true;
		}
		// Enqueue style only if we are on the correct CPT editor page.
		if (isset($_GET['post']) and $pagenow == "post.php") {
		$post_id = $_GET['post'];
		$post_obj = get_post($post_id);
		if ($post_obj->post_type == $cpt) $enqueue = true;
		}
		// Only enqueue if editor page is the correct CPT.
		if ($enqueue) wp_enqueue_style($handle, $src, $deps, $ver, $media);
		}
		public function wp_enqueue_script($cpt, $handle, $src = FALSE, $deps = array(), $ver = FALSE, $in_footer = FALSE) {
		// Check the admin page we are on.
		global $pagenow;
		// Default to NULL to prevent enqueuing.
		$enqueue = NULL;
		// Enqueue script only if we are on the correct CPT editor page.
		if (isset($_GET['post_type']) and $_GET['post_type'] == $cpt and $pagenow == "post-new.php") {
		$enqueue = true;
		}
		// Enqueue script only if we are on the correct CPT editor page.
		if (isset($_GET['post']) and $pagenow == "post.php") {
		$post_id = $_GET['post'];
		$post_obj = get_post($post_id);
		if ($post_obj->post_type == $cpt) $enqueue = true;
		}
		// Only enqueue if editor page is the correct CPT.
		if ($enqueue) wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
