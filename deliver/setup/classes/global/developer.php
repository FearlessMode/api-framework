<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Developer_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Developer_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $text_domain;
		public $brand_name;
		public $brand_version;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			$this->text_domain   = 'custom-theme-translation';
			$this->brand_name    = 'CustomTheme';
			$this->brand_version = theme_status('version');
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->data()
		// @Description
		//  This function is appended to many (maybe to be all) of our functions. This simply allows devs to more
		//   easily output the $var_data into echo, print_r() or dump() so devs don't have to write it
		// @Usage
		//  self::data($var_data, $output_method);
		//  self::data($var_data, $output_method, 'echo');
		//  self::data($var_data, $output_method, 'print');
		//  self::data($var_data, $output_method, 'dump');
		//  self::data($var_data, $output_method, FALSE, 'isset');
		// @Todo->!IMPORTANT_VERY!!!_For_some_reason_array_data_is_not_returning_specifically_from_theme()->backend->advanced_array
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_var = 4.0;
		public function data($data = '', $output_method = '', $default_output_method = '', $var_check_method = '') {
			
			// If our $var_check_method variable is not empty
			// @Note
			//  This is for now for only using the isset() method to check against the $data variable
			//   instead of using the default empty() method
			// @Usage
			//  Set $var_check_method to 'isset'
			
			if (!empty($var_check_method)) {
				
				// If our $var_check_method variable against our $data variable is not FALSE
				// @Runs
				// if (!isset($data)) { return FALSE; }
				
				if (!$var_check_method($data)) {
					
					// Return FALSE
					
					return FALSE;
					
				}
				
			}
			
			// If our $var_check_method variable is empty
			
			else {
				
				// Reset our $output_method variable
				
				$output_method = (!empty($default_output_method)) ? $default_output_method : $output_method;
				
				// If our output method variable is empty or is default
				
				if (empty($output_method) or $output_method == 'default') {
					
					// Let's return our var data variable
					
					return $data;
					
				}
				
				// If our output method variable is echo
				
				elseif ($output_method == 'echo') {
					
					// Let's echo our var data variable
					
					echo $data;
					
				}
				
				// If our output method variable is print
				
				elseif ($output_method == 'print') {
					
					// Let's print our var data variable
					
					print_r($data);
					
				}
				
				// If our output method variable is dump
				
				elseif ($output_method == 'dump') {
					
					// Let's dump our var data variable
					
					var_dump($data);
					
				}
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->dev_error()
		// @Description
		//  This function is a simple wrapper for PHP's trigger_error()
		//   @ http://php.net/manual/en/function.trigger-error.php
		// @Usage
		//  return self::dev_error('', 'Your_Custom_Error_Message');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_dev_error = 1.1;
		public function dev_error($error_type = '', $message = '') {
			
			// If our error type variable is empty
			
			if (empty($error_type)) {
				
				// Set our error message
				
				trigger_error($message . 'REFERENCE:');
				
			}
			
			// If our error type variable is not empty
			
			elseif (!empty($error_type)) {
				
				// Set our error message
				
				trigger_error($message . 'REFERENCE:', $error_type);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->dev_message()
		// @Description
		//  This function is a text and space output to help make more readable sense of our dev_report()
		// @Usage
		//  echo self::dev_message('Your_Custom_Error_Message');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_dev_message = 1.1;
		public function dev_message($dev_message = '') {
			
			// Set our space variable
			
			$space = ' ';
			
			// Return our dev message variable
			
			return $dev_message . $space;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->dev_data()
		// @Description
		//  This function 
		// @Usage
		//  When writing function args
		//   $this->function_created_with_args_here($arg_1, $arg_2, $arg_3, $dev_data = array());
		// @Usage
		//  When calling the function that has the $dev_data array() arg
		//   $this->function_called_with_args_here($arg_1, $arg_2, $arg_3, $this->dev_data());
		// @Usage
		//  When writing inside other functions
		// else {
		// 	if (!empty($dev_data)) {
		// 		$this->dev_report(
		// 			'var',
		// 			(isset($dev_data[0])), (isset($dev_data[1])), (isset($dev_data[2])), (isset($dev_data[3])),
		// 			__FUNCTION__, __CLASS__, __LINE__, __FILE__,
		// 			'Message_Here',
		// 			$arg_name
		// 		);
		// 	}
		// }
		// @Todo->add_to_other_functions
		// @Todo->consider_making_hook_filter
		// @Todo->!IMPORTANT_Remove_Not_Workable_LOL_OR_Look_at_debug_backtrace_function @ http://php.net/debug_backtrace
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_dev_data = 1.0;
		public function dev_data($file = '', $class = '', $func = '', $line = '') {
			
			// Set our $dev_data variable
			
			$dev_data = array($file, $class, $func, $line);
			
			// If our $dev_data variable is not empty let's return it otherwise FALSE
			
			return (!empty($dev_data)) ? $dev_data : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->dev_report()
		// @Description
		//  This function runs our dev report errors to devs when functions fail or have missing needed data, etc.
		//  This function outputs the function name, class name, line number, file location, error message and type as needed 
		// @Usage
		//  @Todo->usages
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_dev_report = 1.5;
		public function dev_report($type = '', $function_name = '', $class_name = '', $line_number = '', $file_path = '', $dev_message = '', $arg_name = '') {
			
			// If our type variable is function or func
			
			if ($type == 'function' or $type == 'func') {
				
				// Let's set our dev message content variable
				
				$dev_message_content = 
				self::dev_message('FUNCTION NAME:') . '"' . $function_name . self::dev_message('()"') . 
				PHP_EOL . PHP_EOL . 
				self::dev_message('IN CLASS:') . '"' . $class_name . '".' . 
				PHP_EOL . PHP_EOL . 
				self::dev_message($dev_message);
				
			}
			
			// If our type variable is variable or var
			// @Since
			//  1.3
			
			// If our type variable is variable or var
			
			elseif ($type == 'variable' or $type == 'var') {
				
				// Let's set our dev message content variable
				
				$dev_message_content = 
				self::dev_message('At least one of the $args passed was not set correctly.') . 
				PHP_EOL . PHP_EOL . 
				self::dev_message('The arg or args needed are: ' . $dev_message);
				
			}
			
			// If our type variable is invalid or wrong value
			// @Since
			//  1.4
			
			// If our type variable is inalid or wrong value
			
			elseif ($type == 'invalid' or $type == 'wrong_value') {
				
				// Let's set our dev message content variable
				
				$dev_message_content = 
				self::dev_message('Attention!!! You passed an invalid value for your ' . $arg_name . ' arg.') . 
				PHP_EOL . PHP_EOL . 
				self::dev_message('The values allowed are: ' . $dev_message);
				
			}
			
			// If our type variable is message or custom
			
			elseif ($type == 'message' or $type == 'custom') {
				
				// Let's set our dev message content variable
				
				$dev_message_content = $dev_message;
				
			}
			
			// @Since
			//  1.2
			// @Note
			//  New method to use variables dev report prefix, then dev message content, then dev report suffix
			
			// Set our dev report prefix variable
			
			$dev_report_prefix = 
			
			self::dev_message('(DEV MESSAGE ERROR - Function Failed To Execute Correctly.)') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('FUNCTION ORIGIN:') . 
			PHP_EOL . 
			self::dev_message('----------------') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('IN FILE:') . $file_path . 
			PHP_EOL . 
			self::dev_message('ON LINE:') . $line_number . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('FAILED DETAILS:') . 
			PHP_EOL . 
			self::dev_message('---------------') . 
			PHP_EOL . PHP_EOL;
			
			// Set our our dev report suffix variable
			
			$dev_report_suffix = 
			
			// @Todo
			//  Find a way to pass the parent function to output a more precise error message
			
			// PHP_EOL . PHP_EOL . 
			// self::dev_message('PARENT FUNCTION LOCATION:') . 
			// PHP_EOL . 
			// self::dev_message('------------------------') . 
			// PHP_EOL . PHP_EOL . 
			// self::dev_message('IN FILE:') . __FILE__ . 
			// PHP_EOL . 
			// self::dev_message('ON LINE:') . __LINE__ . 
			
			PHP_EOL . PHP_EOL . 
			self::dev_message('---------------------------------------------------------------------------------') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('REGARD THE "REFERENCE" AND "CALL STACK" BELOW FOR MORE DETAILS:') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('IN THE "CALL STACK" YOU SHOULD SEE LISTED THE FUNCTION NAME YOU ARE TESTING.') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('PLEASE CHECK THAT FUNCTION AGAINST "FAILED FUNCTION" AND "FUNCTION ORIGIN" ABOVE.') . 
			PHP_EOL . PHP_EOL . 
			self::dev_message('---------------------------------------------------------------------------------') . 
			PHP_EOL . PHP_EOL;
			
			// Set our dev report variable
			
			$dev_report = self::dev_error('', $dev_report_prefix . $dev_message_content . $dev_report_suffix);
			
			// If our dev report variable is not empty let's return it otherwise FALSE
			
			return (!empty($dev_report)) ? $dev_report : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->text_domain()
		// @Description
		//  This function allows us to more easily pass our text domain or a custom one for devs
		// @Usage
		//  theme()->dev->text_domain('Your_Custom_Text_Domain');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_text_domain = 2.0;
		public function text_domain($text_domain = '', $output_method = '') {
			
			// Set our $text_domain_status variable
			
			$text_domain_status = (!empty($text_domain)) and $this->is_developer ? $text_domain : $this->text_domain;
			
			// Load our data method
			
			self::data($text_domain_status, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->option_name()
		// @Description
		//  This function allows us to more easily pass our option name or a custom one for devs
		// @Usage
		//  $this->option_name('Your_Custom_Option_Name');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_option_name = 2.0;
		public function option_name($option_name = '', $output_method = '') {
			
			// Set our $option_name_status variable
			
			$option_name_status = (!empty($option_name)) and $this->is_developer ? $option_name : $this->option_name;
			
			// Load our data method
			
			self::data($option_name_status, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->brand_name()
		// @Description
		//  This function allows us to more easily pass our brand name or a custom one for devs
		// @Usage
		//  $this->brand_name('Your_Custom_Theme_Brand_Name');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_brand_name = 2.0;
		public function brand_name($brand_name = '', $output_method = '') {
			
			// Set our $brand_name_status variable
			
			$brand_name_status = (!empty($brand_name)) and $this->is_white_label ? $brand_name : $this->brand_name;
			
			// Load our data method
			
			self::data($brand_name_status, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->brand_version()
		// @Description
		//  This function allows us to more easily pass our brand version or a custom one for devs
		// @Usage
		//  $this->brand_version('Your_Custom_Theme_Brand_Version');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_brand_version = 2.0;
		public function brand_version($brand_version = '', $output_method = '') {
			
			// Set our $brand_version_status variable
			
			$brand_version_status = (!empty($brand_version)) and $this->is_white_label ? $brand_version : $this->brand_version;
			
			// Load our data method
			
			self::data($brand_version_status, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
