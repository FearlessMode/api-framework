<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// @Todo->figure_out_how_to_run_$this->output_div_for_only_frontend_use_CONFLICT!!!
	// @Todo->figure_out_how_to_run_$this->output_div_for_only_frontend_use_CONFLICT!!!
	// @Todo->figure_out_how_to_run_$this->output_div_for_only_frontend_use_CONFLICT!!!
	// For theme()->hook->do_action();
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Hooks_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Hooks_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $tag;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			$this->tag = 'CustomTheme';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->tag()
		// @Description
		//  This function 
		// @Usage
		//  $this_action_hook = self::tag($tag_1);
		//  returns "CustomTheme-$tag_1"
		//  $this_action_hook = self::tag($tag_1, $tag_2);
		//  returns "CustomTheme-$tag_1-$tag_2"
		//  $this_filter_hook = self::tag($tag_1);
		//  returns "CustomTheme-$tag_1"
		//  $this_filter_hook = self::tag($tag_1, $tag_2);
		//  returns "CustomTheme-$tag_1-$tag_2"
		// @Note
		//  By default each hook tag is prepended with CustomTheme-
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_tag = 2.0;
		public function tag($tag_1 = '', $tag_2 = '', $custom_prefix = '', $output_method = '') {
			
			// If our $tag_1 is not empty
			
			if (!empty($tag_1)) {
				
				// Reset our $tag_1 variable
				
				$tag_1 = '-' . theme()->global->class_type($tag_1);
				
				// Reset our $tag_2 variable
				
				$tag_2 = (!empty($tag_2)) ? '-' . theme()->global->class_type($tag_2) : FALSE;
				
				// If our $custom_prefix variable is not empty
				
				if (!empty($custom_prefix)) {
					
					// Set our $this_hook variable to use our developer's custom hook prefix
					// @Output
					//  $custom_prefix-$tag_1 (when $tag_2 is empty)
					//  $custom_prefix-$tag_1-$tag_2 (when $tag_2 is not empty)
					
					// $this_hook = "{$custom_prefix}" . "{$tag_1}" . "{$tag_2}";
					
					$this_hook = $tag_1 . $tag_2;
					
				}
				
				// If our $custom_prefix variable is empty
				
				else {
					
					// Set our $this_hook variable
					// @Output
					//  CustomTheme-$tag_1 (when $tag_2 is empty)
					//  CustomTheme-$tag_1-$tag_2 (when $tag_2 is not empty)
					
					$this_hook = $this->tag . "{$tag_1}" . "{$tag_2}";
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($this_hook, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->array_filter()
		// @Description
		//  This function is a simple wrapper for passing an array with a customizable filter hook to add to that array
		// @Usage
		//  $my_array = array_filter('Value_1', 'Value_2', 'Value_3');
		//  $this->array_filter('my-custom-filter-hook', $my_array);
		//   The above will add the custom filter result after the default array
		//  $this->array_filter('my-custom-filter-hook', $my_array, TRUE);
		//  $this->array_filter('my-custom-filter-hook', $my_array, 'prepend_custom_filter');
		//   The above will add the custom filter result before the default array
		// @Note
		//  Each filter hook is prepended with CustomTheme-
		//  * See self::add_filter() function code for more details
		// @Reference
		//  http://php.net/manual/en/function.array-merge.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_array_filter = 3.0;
		public function array_filter($tag_1 = '', $tag_2 = '', $array_data = array(), $merge_method = 'both', $output_method = '') {
			
			// Set our $empty_array array placeholder variable
			
			$empty_array = array();
			
			// @Note
			//  We are using an empty array  ie: $empty_array as our default
			//  This allows us to avoid is_array() checks or has_filter() add_filter() checks to avoid array_merge() errors
			//   because array_merge() has to have an array passed, so if the developer's filter doesn't exists
			//   we are just passing an empty array. I think this will be quicker in theory.
			
			// If our $merge_method is before or both
			
			if ($merge_method == 'before' or $merge_method == 'both') {
				
				// Set our $custom_array_data variable
				// @Note
				//  This gathers our appending array data from our developer's filter hook
				// @AutoHook->Filter->CustomTheme-before_$tag_1-$tag_2
				
				$custom_array_data = self::filter("before_{$tag_1}", $tag_2, $empty_array);
				
				// Set our $array_data variable
				
				$array_data = array_merge($custom_array_data, $array_data);
				
			}
			
			// If our $merge_method is after or both
			
			if ($merge_method == 'after' or $merge_method == 'both') {
				
				// Set our $custom_array_data variable
				// @Note
				//  This gathers our appending array data from our developer's filter hook
				// @AutoHook->Filter->CustomTheme-after_$tag_1-$tag_2
				
				$custom_array_data = self::filter("after_$tag_1", $tag_2, $empty_array);
				
				// Set our $array_data variable
				
				$array_data = array_merge($array_data, $custom_array_data);
				
			}
			
			// Load our data method
			// @Note
			//  Developers can make empty arrays hookable for other developers
			//  isset will be used to check that the $array_data isset, instead of using empty
			
			theme()->dev->data($array_data, $output_method, FALSE, 'isset');
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->has_filter()
		// @Description
		//  This function checks for our developer's custom filter
		// @Usage
		//  if (self::has_filter(__CLASS__, 'my_filter_hook_tag_here') { Your_Code_Here... } else { Your_Code_Here... }
		//   This will run has_filter('CustomTheme-Class_Name_Here-my_filter_hook_tag_here');
		// @Note
		//  Each filter is prepended with CustomTheme-
		// @Note
		// Currently we are running the conditions off of our $this->$type_filter()
		//   because if we add some hooks they will be autogenerated from 1 or 2 filter() functions
		//   but let's take revisit this later because self::filter() can run a little lighter
		//   if we call the Wordpress filter functions directly, removing the need to run our self::tag() twice
		// @Reference
		//  https://codex.wordpress.org/Function_Reference/has_filter
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_has_filter = 3.0;
		public function has_filter($tag_1 = '', $tag_2 = '') {
			
			// Set our $this_filter variable
			
			$this_filter = self::tag($tag_1, $tag_2);
			
			// If our developer's custom filter does exist
			
			if (has_filter($this_filter)) {
				
				// Return TRUE
				
				return TRUE;
				
			}
			
			// If our developer's custom filter does not exist
			
			else {
				
				// Return FALSE
				
				return FALSE;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->add_filter()
		// @Description
		//  This function runs our developer's custom filter
		// @Usage
		//  $filter = self::add_filter(__CLASS__, 'my_filter_hook_tag_here');
		//  This will run add_filter('CustomTheme-Class_Name_Here-my_filter_hook_tag_here', 'My_Function_Name_Here');
		// @Note
		//  Each filter is prepended with CustomTheme-
		// @Todo->consider_adding_more_$args_for_apply_filter_return_data_currently_do_not_need_interally
		// @Reference
		//  https://developer.wordpress.org/reference/functions/add_filter/
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_add_filter = 3.0;
		public function add_filter($tag_1 = '', $tag_2 = '') {
			
			// Set our $this_filter variable
			
			$this_filter = self::tag($tag_1, $tag_2);
			
			// Set our $filtered_data placeholder
			
			$filtered_data = '';
			
			// Return our developer's custom filter
			
			return apply_filters($this_filter, $filtered_data);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->filter()
		// @Description
		//  This function runs our developer's custom filter
		// @Usage
		//  $filter = self::filter(__CLASS__, 'my_filter_hook_tag_here');
		//  This will run add_filter('CustomTheme-Class_Name_Here-my_filter_hook_tag_here', 'My_Function_Name_Here');
		// @Note
		//  Each filter is prepended with CustomTheme-
		// @Todo->consider_adding_more_$args_for_apply_filter_return_data_currently_do_not_need_interally
		// @Note
		//  Currently we are running the conditions off of our $this->$type_filter()
		//   because if we add some hooks they will be autogenerated from 1 or 2 filter() functions
		//   but let's take revisit this later because self::filter() can run a little lighter
		//   if we call the Wordpress filter functions directly, removing the need to run our self::tag() twice
		// @Reference
		//  https://developer.wordpress.org/reference/functions/add_filter/
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_filter = 3.0;
		public function filter($tag_1 = '', $tag_2 = '', $default_data = '') {
			
			// If our developer's custom filter dov es exist
			
			if (self::has_filter($tag_1, $tag_2)) {
				
				// Return our developer's custom filter
				
				return self::add_filter($tag_1, $tag_2);
				
			}
			
			// If our developer's custom filter does not exist
			
			else {
				
				// Return our $default_data variable
				
				return $default_data;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->has_action()
		// @Description
		//  This function checks for our developer's custom action
		// @Usage
		//  if (self::has_action(__CLASS__, 'my_filter_hook_tag_here') { Your_Code_Here... } else { Your_Code_Here... }
		//   This will run has_action('CustomTheme-Class_Name_Here-my_filter_hook_tag_here');
		// @Hook
		//  Each action is prepended with CustomTheme-
		// @Reference
		//  https://developer.wordpress.org/reference/functions/has_action/
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_has_action = 3.0;
		public function has_action($tag_1 = '', $tag_2 = '', $use_custom_tag = FALSE) {
			
			// If our $use_custom_tag variable is TRUE
			
			if ($use_custom_tag) {
				
				// Set our $class variable
				
				$class = theme()->global->class_type($tag_1);
				
				// Set our $this_action variables
				
				$this_action = "{$class}-{$tag_2}";
				
			}
			
			// If our $use_custom_tag variable is FALSE
			
			else {
				
				// Set our $this_action variable
				
				$this_action = self::tag($tag_1, $tag_2);
				
			}
			
			// If our developer's custom action does exist
			
			if (has_action($this_action)) {
				
				// Return TRUE
				
				return TRUE;
				
			}
			
			// If our developer's custom action doesn't exist
			
			else {
				
				// Return FALSE
				
				return FALSE;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->do_action()
		// @Description
		//  This function runs our developer's custom action
		// @Usage
		//  self::do_action(__CLASS__, 'my-action-hook-tag-here', FALSE);
		//   This will check for do_do_action('CustomTheme-Class_Name_Here-my-action-hook-tag-here')
		//   This will simply output/run whatever the function returns applied to this action/location
		//  self::do_action(__CLASS__, 'my-action-hook-tag-here', TRUE);
		//   This will check for do_do_action('CustomTheme-Class_Name_Here-my-action-hook-tag-here')
		//   This will output whatever the function returns applied in div
		//    <div class="flexbox_classes_here action-hook-my-action-hook-tag-here">Content</div>
		//  if (!self::do_action(__CLASS__, 'my-action-hook-tag-here')) { Your_Code_Here... }
		//  if (!self::do_action(__CLASS__, 'my-action-hook-tag-here')) { Your_Code_Here... } else { Your_Code_Here... }
		// @Note
		//  The above method will run...
		//   do_action('Class_Name_Here-Unique_Tag_Here');
		// @Hook
		//  Each action is action with CustomTheme-
		//  * See function code for more details
		// @Reference
		//  https://developer.wordpress.org/reference/functions/do_action/
		// @Todo->double_check_function_explanation
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_do_action = 3.0;
		public function do_action($tag_1 = '', $tag_2 = '', $use_custom_tag = FALSE, $use_div = FALSE) {
			
			// If our $type variable is php
			
			// if ($type == 'php') {
				
				// If our $use_custom_tag variable is TRUE (ie: has a value, does not have to be "TRUE")
				//  This means our $use_custom_tag variable can be "TRUE" OR when used with a string value it will become a 2nd tag
				// @Example
				//  self::do_action(__CLASS__, __FUNCTION__, TRUE);
				//  self::do_action($this, __FUNCTION__, TRUE);
				//   -> add_action('Class_Name_Here-Function_Name_Here');
				//  self::do_action($this, __FUNCTION__, 'my_custom_tag');
				//  self::do_action(__CLASS__, __FUNCTION__, 'my_custom_tag');
				//   -> add_action('Class_Name_Here-Function_Name_Here-my_custom_tag');
				
				if ($use_custom_tag) {
					
					// Set our $class variable
					
					$class = theme()->global->class_type($tag_1);
					
					// Set our $custom_tag variable
					
					$custom_tag = (is_string($use_custom_tag)) ? "-{$use_custom_tag}" : FALSE;
					
					// Set our $this_action variables
					
					$this_action = "{$class}-{$tag_2}{$custom_tag}";
					
					// Run our developer's custom action
					// @Auto->Action->Class_Name_Here-$tag_2
					// @Auto->Action->Class_Name_Here-$tag_2-$custom_tag
					
					do_action($this_action);
					
				}
				
				// If our $use_class_tag variable is FALSE
				
				else {
					
					// Set our $this_action variables
					
					$this_action = self::tag($tag_1, $tag_2);
					
					// If our $use_div variable is an array
					// @Note
					//  An empty array means use default grid classes
					//  A non empty array means use the custom grid classes passed from the array
					//  * See theme()->output->class_filter() for more details
					
					if (is_array($use_div)) {
						
						// Set our $use_div variable
						// @Auto->Filter->CustomTheme-Class_Name_Here-$tag_2-div_classes
						// @Note
						//  This filters allows all of our div containers to be change via the filter
						//  * See documenation for further application
						// @Todo->revisit_explanation_explain_better
						
						$div_container = self::filter($tag_1, "$tag_2-div_classes", $use_div);
						
						// Set our $classes variable
						
						$classes = theme()->output->class_filter($div_container);
						
						// Set our $this_class variable
						
						$this_class = strtolower("{$tag_1}-{$tag_2}");
						$this_class = str_replace('_' , '-', $this_class);
						$this_class = str_replace(' ' , '-', $this_class);
						
						// Load our beginning HTML components
						
						theme()->output->div('class', "{$classes} custom-action-{$this_class}");
						
						// Run our developer's custom action
						// @Auto->Action->CustomTheme-$tag_1-$tag_2
						
						do_action($this_action);
						
						// Load our ending HTML components
						
						theme()->output->div('close');
						
						// Add an HTML space after this div for a cleaner HTML look
						
						echo PHP_EOL;
						
					}
					
					// If our use div variable is FALSE
					
					else {
						
						// Run our developer's custom action
						
						do_action($this_action);
						
					}
					
				}
				
			// }
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->method_is_valid()
		// @Description
		//  This function validates wether our proceeding method has a custom action to replace our method instead
		//   At the same time, if so auto before and after hooks are created for the developer's custom action as well
		// @Note
		//  When a function's routine is ran within the theme()->hook->method_is_valid() method it will allow
		//   the developer's custom action to replace the entire execution of that function with their own functions and method
		// This ultimately allowing a developer to replace any function with their own function
		//  at the same time our function would run automatically
		// @Usage
		//  @Todo->add_usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_method_is_valid = 2.0;
		public function method_is_valid($class, $function) {
			
			// Set our $this_class variable
			
			$this_class = $class;
			
			// If our developers custom action does not exist return TRUE
			
			if (!self::has_action($this_class, "replace-{$function}", TRUE)) { return TRUE; }
			
			// If our developers custom action does exist
			
			else {
				
				// @Note
				//  The normal function tags without before or after are fired in case devs are hooked into the normal routine
				//  We will leave it up to developers for now to remove_action() if needed
				
				// @AutoHook->Action->Class_Name_Here-before-$function
				// @AutoHook->Action->Class_Name_Here-before-replace-$function
				
				self::do_action($this_class, "before-{$function}", TRUE);
				self::do_action($this_class, "before-replace-{$function}", TRUE);
				
				// @AutoHook->Action->Class_Name_Here-replace-$function
				// @Note
				//  When a function is ran using the $this->validates() method it will allow
				//   your action to replace the entire execution of that function with your own function
				
				self::do_action($this_class, "replace-{$function}", TRUE);
				
				// @AutoHook->Action->Class_Name_Here-after-replace-$function
				// @AutoHook->Action->Class_Name_Here-after-$function
				
				self::do_action($this_class, "after-replace-{$function}", TRUE);
				self::do_action($this_class, "after-{$function}", TRUE);
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
