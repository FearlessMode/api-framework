<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_API {
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Note
		//  $load_auto_path is __CLASS__
		//   When passing this $arg $this->tag, $this->hook, $this->class can be used as "unique" tag in hook related functions
		//   This will be the class string name of the class where the parnet constructor is called
		//  $load_auto_path is __FILE__
		//   When passing this $arg $this->path will be relative to the path of the file where the parent constructor is called
		//  $load_auto_url  is __DIR__
		//   When passing this $arg $this->url will be relative to the url of the file where the parent constructor is called
		// @Usage
		//  parent::__construct(__CLASS__, __FILE__, __DIR__);
		//  theme()->hook->do_action($this->tag);
		//  theme()->hook->has_filter($this->hook);
		//  theme()->global->load('action', $this->class, ...);
		//  theme()->global->load('filter', $this->class, ...);
		//  $this->path = "$this->path/styles";
		//  $this->url  = "$this->url/styles";
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct($auto_tag = '', $auto_path = '', $auto_url = '') {
			
			// Dynamic paths
			
			$this->path = (!empty($auto_path)) ? this_path($auto_path) : FALSE;
			$this->url  = (!empty($auto_url))  ? this_url($auto_url)   : FALSE;
			
			// Set our various variables
			
			// @copypaste
			// $this->path_name  = 'path_name';
			// $this->_path_name = '/path_name';
			
			// Theme related variables
			
			{
				
				// CustomTheme
				
				$this->theme_path = get_stylesheet_directory();
				$this->theme_url  = get_stylesheet_directory_uri();
				
				// Theme Slug
				// @Note
				//  Returns "custom-theme" ie: the active theme's folder slug
				
				$this->theme_slug = get_template();
				
				// CustomOption
				
				$this->custom_option = 'custom-option';
				
				$this->option_path   = get_theme_root()     . '/' . $this->custom_option . '/';
				$this->option_url    = get_theme_root_uri() . '/' . $this->custom_option . '/';
				
				// Translation
				
				$this->translation = 'translation';
				
			}
			
			// This path related variables
			
			$this->dir_path = get_stylesheet_directory() . '/';
			$this->dir_url  = get_stylesheet_directory_uri() . '/';
			// $this->dir_path = this_path();
			// $this->dir_url  = this_url();
			
			// Misc. global usage
			
			{
				
				// Default Theme Tag
				// Get our theme's slug and adjust it to be used as our unique default hook tag
				//  1. Adjust "custom-theme" to "custom theme"
				//  2. Adjust "custom theme" to "Custom Theme"
				//  3. Adjust "Custom Theme" to "CustomTheme"
				// * See $this->tag, $this->hook, $this->class ie $auto_tag variables for more application details
				// @Note
				//  This enables devs to rename the custom theme folder to their own theme branding name
				
				$this->theme_tag = str_replace('-', ' ', $this->theme_slug);
				$this->theme_tag = ucwords($this->theme_tag);
				$this->theme_tag = str_replace(' ', '',  $this->theme_tag);
				
				// Tagging based variables
				// @Note
				//  These variables are used to create unique hoog tags based from the classes they are created in
				//  This is varible is also to help us easily identify where these unique hook tags are being rendered in various function $args
				//  Lastly this is to help speed up performance of creating our unique tags
				//   Previously we were allowing the $this to create a unique tag
				//   While writing $this is obviously easier we have to run an extra routine to to get the object name of $this to use it as a string
				//   so with this method it will be quicker to just get the class string directly still while 
				// @Note
				//  Currently 3 variables are available to make sense in various contexts needed, but this might change
				
				$this->tag   = (!empty($auto_tag)) ? $auto_tag : $this->theme_tag;
				$this->hook  = (!empty($auto_tag)) ? $auto_tag : $this->theme_tag;
				$this->class = (!empty($auto_tag)) ? $auto_tag : $this->theme_tag;
				
				// Common file names
				
				$this->validate = '_validate.php';
				$this->load     = '_load.php';
				$this->style    = 'style.css';
				$this->script   = 'script.js';
				$this->min      = '.min';
				
				// Asset based folder structure
				
				$this->styles     = 'styles';
				$this->scripts    = 'scripts';
				
				$this->css        = 'css';
				$this->css_path   = 'css/';
				$this->js         = 'js';
				$this->js_path    = 'js/';
				$this->images     = 'images';
				$this->image_path = 'images/';
				
				// Misc string values used
				
				$this->action   = 'action';
				$this->filter   = 'filter';
				$this->enqueue  = 'enqueue';
				$this->inline   = 'inline';
				$this->dequeue  = 'dequeue';
				$this->priority = 'priority';
				$this->dev      = 'dev';
				$this->live     = 'live';
				
				// Various Php shorthands
				// @Reference
				//  http://php.net/manual/en/reserved.constants.php#constant.php-eol
				//  http://php.net/manual/en/dir.constants.php
				
				$this->_  = PHP_EOL;
				$this->__ = DIRECTORY_SEPARATOR;
				
			}
			
			// Third Parties
			
			{
				
				// Third Party Integrations and/or Support
				
				$this->woocommerce = 'woocommerce';
				$this->ubermenu    = 'ubermenu';
				
			}
			
			// Deliver
			
			$this->deliver      = 'deliver';
			$this->deliver_path = "$this->dir_path/$this->deliver";
			
			// Asset
			// @Note
			//  This is calling $this->theme_path not $this->dir_path
			
			$this->asset      = 'asset';
			$this->asset_path = "$this->theme_path/$this->deliver/$this->asset";
			
			{
				
				$this->browser     = 'browser';
				$this->core        = 'core';
				$this->menu        = 'menu';
				$this->third_party = 'third-party';
				
				$this->load_asset_browser     = "$this->asset_path/$this->browser/$this->load";
				$this->load_asset_core        = "$this->asset_path/$this->core/$this->load";
				$this->load_asset_menu        = "$this->asset_path/$this->menu/$this->load";
				$this->load_asset_third_party = "$this->asset_path/$this->third_party/$this->load";
				
			}
			
			// Content
			
			$this->content      = 'content';
			$this->content_path = "$this->dir_path/$this->deliver/$this->content";
			
			{
				
				// @Todo->Reconsider_Using_or_Usage
				
				$this->template       = 'template';
					$this->content_only = 'content-only';
						$this->parts      = 'parts';
					$this->sidebar_only = 'sidebar-only';
					
			}
			
			// Setting
			
			$this->setting      = 'setting';
			$this->setting_path = "$this->dir_path/$this->deliver/$this->setting";
			
			{
				
				$this->framework = 'framework';
					
					$this->advanced           = 'advanced' ;
					$this->advanced_settings  = 'advanced-settings'; 
					$this->advanced_framework = 'advanced-framework';
					$this->simple             = 'simple';
					$this->simple_settings    = 'simple-settings';
					$this->simple_framework   = 'simple-framework';
				
				$this->install = 'install';
				
				$this->language = 'language';
				
				$this->options    = 'options';
					$this->global   = 'global';
					$this->backend  = 'backend';
					$this->frontend = 'frontend';
					
				$this->page_builder      = 'page-builder';
					$this->beaver_builder  = 'beaver-builder';
					$this->cornerstone     = 'cornerstone';
					$this->divi_builder    = 'divi-builder';
					$this->fusion_builder  = 'fusion-builder';
					$this->king_composer   = 'king-composer';
					$this->visual_composer = 'visual-composer';
						$this->arrays        = 'arrays';
						$this->shortcodes    = 'shortcodes';
						$this->templates     = 'templates';
						
				$this->product = 'product';
				
			}
			
			// ----------------------------------------------------------------------------------------------------
			// Shorthands
			// ----------------------------------------------------------------------------------------------------
			
			$this->framework_asset_path  = "$this->setting_path/$this->asset";
			$this->load_framework_assets = "$this->framework_asset_path/$this->load";
			
			// Install path
			
			$this->install_path = "$this->setting_path/$this->install";
			$this->load_install = "$this->install_path/$this->load";
			
			{
				
				// Plugins
				
				$this->php_info      = 'php-info';
				$this->php_info_path = "$this->install_path/$this->php_info";
				$this->load_php_info = "$this->php_info_path/$this->load";
				
				// Plugins
				
				$this->plugins      = 'plugins';
				$this->plugins_path = "$this->install_path/$this->plugins";
				$this->load_plugins = "$this->plugins_path/$this->load";
				
				// Requires
				
				$this->requires      = 'requires';
				$this->requires_path = "$this->install_path/$this->requires";
				$this->load_requires = "$this->requires_path/$this->load";
				
				// Settings Menu
				
				$this->theme_settings_menu      = 'theme-settings-menu';
				$this->theme_settings_menu_path = "$this->install_path/$this->theme_settings_menu";
				$this->load_theme_settings_menu = "$this->theme_settings_menu_path/$this->load";
				
				$this->theme_settings_menu_slug = "admin.php?page=$this->theme_settings_menu";
				
				// Post Types
				
				$this->theme_post_types      = 'theme-post-types';
				$this->theme_post_types_path = "$this->install_path/$this->theme_post_types";
				$this->load_theme_post_types = "$this->theme_post_types_path/$this->load";
				
				// Theme Support
				
				$this->theme_support      = 'theme-support';
				$this->theme_support_path = "$this->install_path/$this->theme_support";
				$this->load_theme_support = "$this->theme_support_path/$this->load";
				
			}
			
			// Framework path
			
			$this->framework_path = "$this->setting_path/$this->framework";
			$this->load_framework = "$this->framework_path/$this->load";
			
			{
				
				// Advanced framework
				
				$this->advanced_framework_path = "$this->framework_path/$this->advanced";
				$this->load_advanced_framework = "$this->advanced_framework_path/$this->load";
				
				$this->advanced_settings_path = "$this->framework_path/$this->advanced_settings";
				$this->load_advanced_settings = "$this->advanced_settings_path/$this->load";
				
				// Simple framework
				
				$this->simple_framework_path = "$this->framework_path/$this->simple";
				$this->load_simple_framework = "$this->simple_framework_path/$this->load";
				
				$this->simple_settings_path = "$this->framework_path/$this->simple_settings";
				$this->load_simple_settings = "$this->simple_settings_path/$this->load";
				
			}
			
			// Options path
			
			$this->options_path = "$this->setting_path/$this->options";
			$this->load_options = "$this->options_path/$this->load";
			
			{
				
				// Backend
				
				$this->backend_option       = "$this->options_path/$this->backend";
				$this->load_backend_options = "$this->backend_option/$this->load";
				
				// Frontend
				
				$this->frontend_option       = "$this->options_path/$this->frontend";
				$this->load_frontend_options = "$this->frontend_option/$this->load";
				
				// Global
				
				$this->global_option       = "$this->options_path/$this->global";
				$this->load_global_options = "$this->global_option/$this->load";
				
			}
			
			// Product path
			
			$this->product_path = "$this->setting_path/$this->product";
			$this->load_product = "$this->product_path/$this->load";
			
			// 
			
			{
				
				
				
			}
			
			// Filterable variables
			
			{
				
				// Set our global default asset version
				// @Description
				//  This sets a global default wether to load the .css/.js version or the .min.css/.min.js file
				// @Note
				//  Set or $this->live for production releases
				// @Hook->Filter->CustomTheme-global_default-asset_version
				// @Todo->!IMPORTANT_fix!!!
				
				// $this->global_default_asset_version = 'CustomTheme-global_default-asset_version';
				
				// @ProductionChange
				
				// $this->asset_version = $this->dev;
				// $this->asset_version = $this->live;
				
				// if (has_filter($this->global_default_asset_version)) {
					
					// Reset our $this->asset_version variables
					// echo 'Acessed!!!';
					// $this->asset_version = apply_filters($this->global_default_asset_version, $this->asset_version);
					
				// }
				
			}
			
			// Internal API related variables
			// @Note
			//  These are mostly variables prefixed with api the can be easily used to call other class methods
			// @Example
			//  theme()->api_registered_post_types
			
			{
				
				// Product path
				
				$this->product_path = "$this->setting_path/$this->product";
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
