<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Third_Party_API extending our CustomTheme class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Third_Party_API extends CustomTheme {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $license;
		public $kc_license;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			$this->license     = 'license';
			$this->kc_license  = 'KC_LICENSE';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->status()
		// @Description
		//  This function set our various default value for our support page builders
		//   ie: defines, post type editable support, etc.
		// @Usage
		//  $this->status($this->_king_composer);
		//  $this->status($this->_visual_composer);
		// @Todo->set_liense_data_to_new_third_party_class_object
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_status = 1.0;
		public function status($page_builder = '') {
			
			// If our $page_builder variable is king-composer
			
			if ($page_builder == $this->_king_composer) {
				
				// Set our $default_license variable
				
				$default_license = 'ultkwpq3-urvn-rqrr-umdb-f3fl-qiszyckksoh4';
				
				// Set our $license_status variable
				// @Hook->Filter->CustomTheme-license-king-composer
				
				$license_status = theme()->hook->filter($this->license, $this->_king_composer, $default_license);
				
				// If our KC License is not defined let's define it giving our developer's filter hook priority
				
				if (!defined($this->kc_license)) { define($this->kc_license, $license_status); }
				
				// Set KC as the default loaded page builder when loading an KC editable post type
				// @Todo->create_post_selected_condition_loading
				
				// if (!defined('KC_FORCE_DEFAULT'))  { define('KC_FORCE_DEFAULT', TRUE); }
				
				// if (!defined('KC_EXTENSION_BETA')) { define('KC_EXTENSION_BETA', TRUE); }
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->is_active()
		// @Description
		//  This function is a simple wrapper for checking if various plugins, classes, screens, etc. are active
		// @Usage
		//  if (theme()->third_party->is_active('KC')) { Your_Code... } else { Your_Code... } Checks for KingComposer
		//  if (theme()->third_party->is_active('VC')) { Your_Code... } else { Your_Code... } Checks for Visual Composer
		//  if (theme()->third_party->is_active('Woocommerce')) { Your_Code... } else { Your_Code... } checks for Woocommerce
		// @Todo->maybe_admin_screen_support_here_maybe_move_to_other_function
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_is_active = 1.0;
		public function is_active($context = '', $screen = '', $base = '', $output_method = '') {
			
			// If our context variable is BB ie: Beaver Builder Page Builder
			
			if ($context == 'BB' or $context == 'bb') {
				
				// Set our active status variable
				
				$active_status = (class_exists('FLBuilder')) ? TRUE : FALSE;
				
			}
			
			// If our context variable is KC ie: King Composer Page Builder
			
			if ($context == 'KC' or $context == 'kc') {
				
				// Set our active status variable
				
				$active_status = (defined('KC_VERSION') and (class_exists('KingComposer'))) ? TRUE : FALSE;
				
			}
			
			// If our context variable is VC ie: Visual Composer Page Builder
			
			elseif ($context == 'VC' or $context == 'vc') {
				
				// Set our active status variable
				
				$active_status = (defined('WPB_VC_VERSION') and (class_exists('WPBakeryShortCode'))) ? TRUE : FALSE;
				
			}
			
			// If our context variable is Woocommerce or woocommerce
			
			elseif ($context == 'Woocommerce' or $context == 'woocommerce') {
				
				// Set our active status variable
				
				$active_status = (class_exists('woocommerce')) ? TRUE : FALSE;
				
			}
			
			// If our context variable is Importer ie: Our theme demo importer class
			
			elseif ($context == 'Importer' or $context == 'importer') {
				
				// Set our active status variable
				
				$active_status = (class_exists('DT_Demo_Importer')) ? TRUE : FALSE;
				
			}
			
			// Load our data method
			
			theme()->dev->data($active_status, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
