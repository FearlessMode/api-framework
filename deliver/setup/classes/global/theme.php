<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Theme_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Theme_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_global = 1.0;
		public function __construct() {
			
			// ----------------------------------------------------------------------------------------------------
			// Set our various variables to represent and object instance of our various classes
			// ----------------------------------------------------------------------------------------------------
			
			// Load our developer's custom action
			
			// do_action('CustomTheme-before-global-object_api');
			
			// Our global classes
			
			$this->api         = new CustomTheme();
			$this->start       = new CustomTheme();
			
			$this->data        = new CustomTheme_Global_Data_API();
			
			$this->dev         = new CustomTheme_Global_Developer_API();
			
			$this->global      = new CustomTheme_Global_Helpers_API();
			
			$this->hook        = new CustomTheme_Global_Hooks_API();
			
			$this->third_party = new CustomTheme_Global_Third_Party_API();
			
			// Load our developer's custom action
			
			// do_action('CustomTheme-after-global-object_api');
			
			// If this is the Wordpress frontend
			
			if (is()->frontend) {
				
				// Load our developer's custom action
				
				// do_action('CustomTheme-before-frontend-object_api');
				
				// Our frontend classes
				
				$this->asset    = new CustomTheme_Frontend_Asset_API();
				
				$this->content  = new CustomTheme_Frontend_Content_API();
				
				$this->frontend = new CustomTheme_Frontend_Helpers_API();
				
				$this->output   = new CustomTheme_Frontend_Output_API();
				
				$this->template = new CustomTheme_Frontend_Template_API();
				
				// Load our developer's custom action
				
				// do_action('CustomTheme-after-frontend-object_api');
				
			}
			
			// If this is the Wordpress backend
			
			else {
				
				// Load our developer's custom action
				
				// do_action('CustomTheme-before-backend-object_api');
				
				// Our backend classes
				
				$this->simple_array   = new CustomTheme_Backend_Simple_Array_API();
				
				$this->advanced_array = new CustomTheme_Backend_Advanced_Array_API();
				
				$this->backend        = new CustomTheme_Backend_Helpers_API();
				
				$this->product        = new CustomTheme_Backend_Product_API();
				
				// Load our developer's custom action
				
				// do_action('CustomTheme-after-backend-object_api');
				
			}
			
			// add_action('wp', array($this, 'api'));
			
		}
		
		// if (in_array('theme-header', theme()->post_types)) {}
		
		public function api() {
			
			// $post_types = new CustomTheme_Theme_Post_Types();
			
			// return $post_types;
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
