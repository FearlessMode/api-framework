<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Data_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Data_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $theme_data;
		public $post_data;
		public $all_data;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			$this->theme_data = 'theme_data';
			$this->post_data  = 'post_data';
			$this->all_data   = 'all_data';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->valid_args()
		// @Description
		//  This function is a simple holder for our valid args to be passed into functions that allow multiple args
		//  These multiple args are for developers who prefer different wording of args for various coding contexts
		// @Usage
		//  @Todo->create
		//  @Todo->add_dev_report_maybe_for_when_dev_passes_invalid_arg_one_place_for_error_output
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_valid_args = 2.0;
		public function valid_args($type = '', $output_method = '') {
			
			// If our $type variable is theme_data
			
			if ($type == $this->theme_data) {
				
				// Set our $valid_args variable
				
				$valid_args = array($this->theme_data, 'data', 'page_option', 'setting');
				
			}
			
			// If our $type variable is post_data
			
			elseif ($type == $this->post_data) {
				
				// Set our $valid_args variable
				
				$valid_args = array($this->post_data, 'meta', 'post_meta', 'custom');
				
			}
			
			// Set our hookable $valid_args variable for developers to add custom valid args for various contexts
			// @Hook->Filter->CustomTheme-valid_args-theme_data
			// @Hook->Filter->CustomTheme-valid_args-post_data
			
			$valid_args = theme()->hook->array_filter(__FUNCTION__, $type, $valid_args, 'after');
			
			// Load our data method
			
			theme()->dev->data($valid_args, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get()
		// @Description
		//  This function is a simpler wrapper for self::get_option. 
		//  This function runs a routine to check if we should get our option data from the current post setting,
		//   custom layout setting or the default setting. This function allows passing an $arg
		//   to only get the option data from 1 of the 3 or all methods.
		//  * See function code for more details
		// @Usage
		//  $settings_exist = $this->get('all_data', '__settings__');
		//  if ($settings_exist) { Your_Code... } else { Your_Code... }
		//  $this_option = $this->get('all_data', '__settings__', 'Index_1, Index_2');
		//  if ($this_option) { Your_Code... } else { Your_Code... }
		//  $activated = $this->get('all_data', '__settings__', '', '', 'activate_custom_settings');
		//  if ($activated) { Your_Code... } else { Your_Code... }
		//  $this_activated_option = $this->get('all_data', '__settings__', 'Index_1, Index_2', '', 'activate_custom_settings');
		//  if ($this_activated_option) { Your_Code... } else { Your_Code... }
		//  $custom_header = $this->get('all_data', '__settings__', 'Index_1, Index_2', '', 'activate_custom', 'enabled_header');
		//  if ($custom_header) { Your_Code... } else { Your_Code... }
		// @Todo->revisit
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get = 6.0;
		public function get($context = '', $option_name = '', $indexes = '', $post_id = '', $activation_index = '', $enabled_index = '', $output_method = '') {
			
			// @Note
			//  These methods and functions determine wether to load our default or custom content output functions
			//   ie: Theme Settings option vs. Post Settings option
			
			// Set our various internal method variables
			
			$normal_method  = ($context == 'normal')  ? TRUE : FALSE;
			$default_method = ($context == 'default') ? TRUE : FALSE;
			
			// If our normal method or default method variable is TRUE
			// @Note
			//  This will set our variables to our methods, option names, indexes, etc.
			//  The best reason for this is if we decide to change any of these values like our 
			//   our option name or post meta name we can change it here and it will automatically be updated
			//   by our calling functions when the ping off this one get status function
			
			if ($normal_method or $default_method) {
			
			// Set our various option variables
				
				$option_name = (!empty($option_name)) ? $option_name : $this->option_name;
				$indexes     = (!empty($indexes))     ? $indexes  : 'layout, options';
				
			}
			
			// Set our data exists variables
			// @Note
			//  If our theme data exists variable is FALSE do nothing
			//   This can be pulled from our Down Page Theme Options to be developed later, migrated from Tekanewa
			// @Todo->create_a_one_clickable_down_page_activation
			
			$theme_data = self::get_option($this->theme_data, theme()->dev->option_name(), $indexes, $post_id);
			$post_data  = self::get_option($this->post_data,  theme()->dev->option_name(), $indexes, $post_id);
			
			// Set our context related variables
			// @Todo->revisit_context_methods_simplify_consildate
			
			$both_related = ($context == $this->all_data)   ? TRUE : FALSE;
			$page_related = ($context == $this->theme_data) ? TRUE : FALSE;
			$post_related = ($context == $this->post_data)  ? TRUE : FALSE;
			
			// If our both related or normal method variable is TRUE
			// @Note
			//  Add method comments in if/else condition
			
			if ($both_related or $normal_method) {
				
				// If our theme data variable is FALSE
				
				if (!$theme_data) {}
				
				// If our post data variable is FALSE
				
				elseif (!$post_data) {
					
					// Set our data location variable
					
					$data_location = self::get_option($this->theme_data, $option_name, $indexes);
					
				}
				
				// If our theme data variable is TRUE
				
				elseif ($post_data) {
					
					// Set our activation related variable
					
					$activation_related = (!empty($activation_index)) ? TRUE : FALSE;
					
					// If our activation related variable is FALSE
					
					if (!$activation_related) {
						
						// Set our data location variable
						
						$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
						
					}
					
					// If our activation related variable is TRUE
					
					elseif ($activation_related) {
						
						// Set our data location variable
						
						$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
						
						// Set our activated variable
						
						$activated = $setting[$activation_index];
						
						// If our activated variable is FALSE
						
						if (!theme()->global->is($activated)) {
							
							// Set our data location variable
							
							$data_location = self::get_option($this->theme_data, $option_name, $indexes);
							
						}
						
						// If our activated variable is TRUE
						
						elseif (theme()->global->is($activated)) {
							
							// Set our enabled related variable
							
							$enabled_related = (!empty($enabled_index)) ? TRUE : FALSE;
							
							// If our enabled related variable is FALSE
							
							if (!$enabled_related) {
								
								// Set our data location variable
								
								$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
								
							}
							
							// If our enabled related variable is TRUE
							
							elseif ($enabled_related) {
								
								// Set our enabled variable
								
								$enabled = $setting[$enabled_index];
								
								// If our enabled variable is FALSE
								
								if (!theme()->global->is($enabled)) {}
								
								// If our enabled variable is TRUE
								
								elseif (theme()->global->is($enabled)) {
									
									// Set our data location variable
									
									$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
									
								}
								
							}
							
						}
						
					}
					
				}
				
			}
			
			// If our page related or default method variable is TRUE
			// @Note
			//  Add method comments in if/else condition
			
			elseif ($page_related or $default_method) {
				
				// If our theme data variable is TRUE
				
				if ($theme_data) {
					
					// Set our data location variable
					
					$data_location = self::get_option($this->theme_data, $option_name, $indexes);
					
				}
				
			}
			
			// If our post related variable is TRUE
			// @Note
			//  Add method comments in if/else condition
			
			elseif ($post_related) {
				
				// If our post data variable is TRUE
				
				if ($post_data) {
					
					// Set our activation related variable
					
					$activation_related = (!empty($activation_index)) ? TRUE : FALSE;
					
					// If our activation related variable is FALSE
					
					if (!$activation_related) {
						
						// Set our data location variable
						
						$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
						
					}
					
					// If our activation related variable is TRUE
					
					elseif ($activation_related) {
						
						// Set our data location variable
						
						$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
						
						// Set our activated variable
						
						$activated = $setting[$activation_index];
						
						// If our activated variable is FALSE
						
						if (!theme()->global->is($activated)) {}
						
						// If our activated variable is TRUE
						
						elseif (theme()->global->is($activated)) {
							
							// Set our enabled related variable
							
							$enabled_related = (!empty($enabled_index)) ? TRUE : FALSE;
							
							// If our enabled related variable is FALSE
							
							if (!$enabled_related) {
								
								// Set our data location variable
								
								$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
								
							}
							
							// If our enabled related variable is TRUE
							
							elseif ($enabled_related) {
								
								// Set our enabled variable
								
								$enabled = $setting[$enabled_index];
								
								// If our enabled variable is FALSE
								
								if (!theme()->global->is($enabled)) {}
								
								// If our enabled variable is TRUE
								
								elseif (theme()->global->is($enabled)) {
									
									// Set our data location variable
									
									$data_location = self::get_option($this->post_data, $option_name, $indexes, $post_id);
									
								}
								
							}
							
						}
						
					}
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($data_location, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_option()
		// @Description
		//  This function is a wrapper for Wordpress's get_option()
		//   @ https://developer.wordpress.org/reference/functions/get_option/
		// @Usage
		//  self::get_option($this->theme_data, '__settings__', 'index_1, index_2'); Returns theme option data with indexes
		//  self::get_option($this->post_data, '__settings__', 'index_1, index_2', 8); Returns post data with indexes for post id 8
		//  * See function code for more details
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_option = 5.0;
		public function get_option($type = '', $option_name = '', $indexes = '', $post_id = '', $output_method = '') {
			
			// If our type variable is matched against our valid theme args variable
			// @Usage
			//  $this->get($this->theme_data, '__settings__'); Returns theme option data
			//  $this->get($this->theme_data, '__settings__', 'exist'); Returns TRUE or FALSE for theme option data
			//  $this->get($this->theme_data, '__settings__', 'index_1, index_2'); Returns theme option data with indexes
			
			if (in_array($type, self::valid_args($this->theme_data))) {
				
				// If our indexes variable is exist, exists
				
				if ($indexes == 'exist' or $indexes == 'exists') {
					
					// Set our this option variable
					
					$this_option = get_option($option_name) ? TRUE : FALSE;
					
				}
				
				// If our indexes variable is anything else or empty 
				//  let's search for the passed option name and indexes as needed
				
				else {
					
					// Set our settings page option variable
					
					$this_option = self::get_method_type_and_index_count($this->theme_data, $option_name, '', $indexes);
					
				}
				
			}
			
			// If our type variable is matched against our valid post args variable
			// @Usage
			//  $this->get($this->post_data, '__settings__'); Returns post data for for current post id
			//  $this->get($this->post_data, '__settings__', '', 8); Returns post data for custom post id
			//  $this->get($this->post_data, '__settings__', 'exist'); Returns TRUE or FALSE for current post id
			//  $this->get($this->post_data, '__settings__', 'exists', 8); Returns TRUE or FALSE for custom post id
			//  $this->get($this->post_data, '__settings__', 'index_1, index_2'); Returns post data with indexes for current post id
			//  $this->get($this->post_data, '__settings__', 'index_1, index_2', 8); Returns post data with indexes for custom post id
			
			elseif (in_array($type, self::valid_args($this->post_data))) {
				
				// Set our post id variable
				// @Note
				//  If our post id is not empty use get_the_ID() otherwise use the post id variable value passed
				
				$post_id = (!empty($post_id)) ? $post_id : get_the_ID();
				
				// If our indexes variable is exist, exists
				
				if ($indexes == 'exist' or $indexes == 'exists') {
					
					// Set our this option variable
					
					$this_option = get_post_meta($post_id, $option_name) ? TRUE : FALSE;
					
				}
				
				// If our indexes variable is anything else or empty 
				//  let's search for the passed option name and indexes as needed
				
				else {
					
					// Set our this option variable
					
					$this_option = self::get_method_type_and_index_count($this->post_data, $option_name, $post_id, $indexes);
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($this_option, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_method_type_and_index_count()
		// @Description
		//  This function runs a routine to determine how many indexes to append to our get_option()
		//   and runs error free to avoid any undefined index errors
		// @Usage
		//  self::get_method_type_and_index_count($this->theme_data, $option_name, '', $indexes);
		//  self::get_method_type_and_index_count($this->post_data, $option_name, $post_id, $indexes);
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_method_type_and_index_count = 2.0;
		public function get_method_type_and_index_count($type = '', $option_name = '', $post_id = '', $index = '', $output_method = '') {
			
			// If our type variable is matched against our valid theme args variable
			
			if (in_array($type, self::valid_args($this->theme_data))) {
				
				// Set our get option data variable
				
				$get_option_data = get_option($option_name);
				
				// If our get option variable is not empty
				
				if (!empty($get_option_data)) {
					
					// Set our this method variable
					
					$this_method = get_option($option_name);
					
				}
				
				// If our get option data variable is empty let's do nothing and return now to avoid error outputs
				
				else { return; }
				
			}
			
			// If our type variable is matched against our valid post args variable
			
			elseif (in_array($type, self::valid_args($this->post_data))) {
				
				// Set our post meta data variable
				
				$post_meta_data = get_post_meta($post_id, $option_name);
				
				// If our post meta data variable is not empty
				
				if (!empty($post_meta_data)) {
					
					// Set our this method variable
					
					$this_method = get_post_meta($post_id, $option_name, TRUE);
					
				}
				
				// If our post meta data variable is empty let's do nothing and return now to avoid error outputs
				
				else { return; }
				
			}
			
			// Set our $index variables
			
			$index = theme()->global->indexes($index);
			
			// @Note
			//  Let's discover how many indexes we need to apply to our this method variable
			
			// If our index variable $key => $index[0] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			if (!isset($index[0])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method;
				
			}
			
			// If our index variable $key => $index[1] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[1])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]];
				
			}
			
			// If our index variable $key => $index[2] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[2])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]];
				
			}
			
			// If our index variable $key => $index[3] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[3])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]];
				
			}
			
			// If our index variable $key => $index[4] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[4])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]];
				
			}
			
			// If our index variable $key => $index[5] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[5])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]];
				
			}
			
			// If our index variable $key => $index[6] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[6])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]] [$index[5]];
				
			}
			
			// If our index variable $key => $index[7] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[7])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]] [$index[5]] [$index[6]];
				
			}
			
			// If our index variable $key => $index[8] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[8])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]] [$index[5]] [$index[6]] [$index[7]];
				
			}
			
			// If our index variable $key => $index[9] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[9])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]] [$index[5]] [$index[6]] [$index[7]] [$index[8]];
				
			}
			
			// If our index variable $key => $index[10] is not set
			// @Note
			//  This means we have no further indexes to look for so let's stop now and set our this option variable
			
			elseif (!isset($index[10])) {
				
				// Set our $this_option_and_index_count variable
				
				$this_option_and_index_count = $this_method [$index[0]] [$index[1]] [$index[2]] [$index[3]] [$index[4]] [$index[5]] [$index[6]] [$index[7]] [$index[8]] [$index[9]];
				
			}
			
			// Load our data method
			
			theme()->dev->data($this_option_and_index_count, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->option()
		// @Description
		//  This function gets the options from our database and from there determines wether to load
		//   the default option, custom layout option, or from the post itself.
		// @Note
		//  Priority is as follows... Highest (Current Post), Middle (Custom Layout), Lowest (Default Layout)
		//   If the Lowest (Default Layout) is empty or not set or sett to default the default value in the PHP will be used
		// @Usage
		//  echo $this->option('header_layout', 'fullwidth');
		//   will echo header_layout, if not found fullwidth with be used
		// @Todo->add_support_for_custom_option_name_index
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_option = 4.0;
		public function option($option_key = '', $default_value = '', $output_method = '') {
			
			// Set our option name and indexes variables
			
			// $option_name = (!empty($option_name)) ? $option_name : '__settings__';
			// $indexes     = (!empty($indexes))     ? $indexes     : 'layout, options';
			
			// Set our various status related variables
			
			$default = $this->get->get_layout('data', 'default');
			$custom  = $this->get->get_layout('data');
			$current = $this->get->get_layout('data', 'current');
			
			// Set our various this option variables
			
			$this_option = (!empty($default[$option_key]) and $default[$option_key] !== 'default') ? $default[$option_key] : $default_value;
			$this_option = (!empty($custom[$option_key])  and $custom[$option_key]  !== 'default') ? $custom[$option_key]  : $this_option;
			$this_option = (!empty($current[$option_key]) and $current[$option_key] !== 'default') ? $current[$option_key] : $this_option;
			
			// Load our data method
			
			theme()->dev->data($this_option, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
