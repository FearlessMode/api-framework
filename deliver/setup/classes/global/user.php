<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_User_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_User_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		// @Note
		//  There is no need for setting visibility on our variables because they will be public by default
		//   and all variable in here or shorthands for Wordpress conditions so they all should be publicly avialable 
		//   so devs can use them where ever, however they choose.
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// ----------------------------------------------------------------------------------------------------
			// Set our various variables to represent and object instance of our various classes
			// ----------------------------------------------------------------------------------------------------
			
			// Set our $this->current_user variable
			
			$this->current_user = wp_get_current_user();
			
			// Set our user_data_is_valid variable
			// If our $this->current_user is an instance of our WP_User object
			// @Reference
			//  http://php.net/manual/en/internals2.opcodes.instanceof.php
			//  https://codex.wordpress.org/Function_Reference/wp_get_current_user
			//  https://developer.wordpress.org/reference/classes/wp_user/
			
			$this->user_data_is_valid = ($this->current_user instanceof WP_User) ? TRUE : FALSE;
			
			// Set our $this->user_data_is_not_valid variable
			// @Todo->reconsider_this_message_or_return_value_a_better_message_error_perhaps??
			
			$this->user_data_is_not_valid = 'Something was wrong. The WP_User data could not be returned. Possibly it was called to early to retrieve. Please double check.';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->get_context()
		// @Description
		//  This function is not intended for direct use. It is a simple internal function
		//   that our other user related functions pass $args to
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_get_context = 1.0;
		public function get_context($operation_type, $context = '') {
			
			// If our $this->user_data_is_valid variable is TRUE
			
			if ($this->user_data_is_valid) {
				
				// If our $context variable is not empty let's return our $this->current_user method otherwise FALSE
				
				return (is_callable($func) and (!empty($context))) ? $operation_type($context) : FALSE;
				
			}
			
			// If our $this->user_data_is_valid variable is FALSE
			
			else {
				
				// If our $this->user_data_is_not_valid variable is not empty let's return it otherwise NULL
				
				return (!empty($this->user_data_is_not_valid)) ? $this->user_data_is_not_valid : NULL;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->can()
		// @Description
		//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
		// @Usage
		//  if (user()->can('edit_theme_options')) { Your_Code_Here... }
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_can = 1.0;
		public function can($context = '') {
			
			// Load our self::get_context function passing our get method type and $context variable
			
			self::get_context($this->current_user->has_cap, $context);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->has()
		// @Description
		//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
		// @Usage
		//  if (user()->has('facebook')) { Your_Code_Here... }
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_has = 1.0;
		public function has($context = '') {
			
			// Load our self::get_context function passing our get method type and $context variable
			
			self::get_context($this->current_user->has_prop, $context);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
