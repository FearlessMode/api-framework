<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Is_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Is_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		// @Note
		//  There is no need for setting visibility on our variables because they will be public by default
		//   and all variable in here or shorthands for Wordpress conditions so they all should be publicly avialable 
		//   so devs can use them where ever, however they choose.
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_global = 1.0;
		public function __construct($device_args = array()) {
			
			// If our $device_args is not empty
			
			if (!empty($device_args)) {
				
				// Return our CustomTheme_Context_API is context value
				
				return CustomTheme_Context_API::is($args);
				
			}
			
			// If our $device_args is empty
			
			else {
				
				// Set our variaous shorthand variables for our various Wordpress functions
				
				$this->backend  = is_admin();
				$this->frontend = !is_admin();
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
