<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Global_Helpers_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Global_Helpers_API extends CustomTheme {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $wp_actions;
		public $before_init;
		public $after_init;
		public $before_constructor;
		public $after_constructor;
		public $load_constructor;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Load our parent constructor
			
			parent::__construct(__CLASS__, __FILE__);
			
			// Set our various variables
			
			$this->before_init = 'before_init';
			$this->after_init  = 'after_init';
			$this->wp_actions  = 'wp_actions';
			
			// Load any methods on instantiation that we need
			
			// self::intialize();
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// @DEPRECIATED->double_check_not_used_and_remove_better_performance_to_call_functions_directly_instead_of_4_bounces
		// ->constructor()
		// @Description
		//  This function is our constructor function that handles auto loading any
		//   functions, variables and constructors upon an extending class instantiation
		// @Note
		//  We call this in our extending class constructors
		// @Important
		//  * This only supports passing the object $class right now not the string of the class ie: static, __CLASS__
		// @Usage
		//  theme_class()->constructor($constructor_args, $this);
		// @Reference
		//   http://php.net/manual/en/language.oop5.decon.php
		// @Todo->add_support_for_static_method_calls
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_constructor = 4.0;
		public function constructor($class, $constructor_args = array()) {
			
			// Set our $this_class variable
			
			$this_class = $class;
			
			// Set our various $constructor_args
			
			$load_before_init = (isset($constructor_args[$this->before_init])) ? $constructor_args[$this->before_init] : FALSE;
			$load_after_init  = (isset($constructor_args[$this->after_init]))  ? $constructor_args[$this->after_init]  : FALSE;
			$load_wp_actions  = (isset($constructor_args[$this->wp_actions]))  ? $constructor_args[$this->wp_actions]  : FALSE;
			
			// If our $load_before_init is TRUE load our before_init() it and its custom hooks
			// @AutoHook->Class_Name_Here-before-before_init
			// @AutoHook->Class_Name_Here-after-before_init
			
			self::function_constructor($this_class, $load_before_init, $this->before_init);
			
			// If our $load_constructor is TRUE load our custom CustomTheme::__construct() function
			// @Note
			//  This is currently just an idea so extending devs can remove our constructor when instantiating our CustomTheme class
			//  If no constructor is loaded in a child class it automatically inherits the parent class's constructor if has one
			// @Todo->possible_consideration_of_one_more_constructor_args_filter_Here
			//  This will ultimately give the highest priority to our theme's API __construct() globally overriding any previosu set filters
			//  Will leave for now. Developer's can provide feedback about this when the time comes
			// @Todo->!IMPORTANT_RETHINK_DO_WE_NEED??
			// @Note
			//  Currently there is nothing we really need from our core constructor and each constructor is unique
			//   I like that and that all things are still accessible via our theme()->object->$var
			
			// if (get_class() !== 'CustomTheme') { CustomTheme::__construct(); }
			
			// If our $load_after_init is TRUE load our after_init() it and its custom hooks
			// @AutoHook->Class_Name_Here-before-after_init
			// @AutoHook->Class_Name_Here-after-after_init
			
			self::function_constructor($this_class, $load_after_init, $this->after_init);
			
			// If our $load_wp_actions is TRUE load it and its custom hooks
			// @AutoHook->Class_Name_Here-before-wp_actions
			// @AutoHook->Class_Name_Here-after-wp_actions
			
			self::function_constructor($this_class, $load_wp_actions, $this->wp_actions);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->custom_constructor()
		// @Description
		//  This function allows custom __construct() methods to be passed before or after our parent __construct()
		// @Usage
		//  self::custom_constructor('before', $before_constructor);
		//  self::custom_constructor('after', $after_constructor);
		// @Note
		//  When a custom class constructor is passed auto action hooks before and after will be created
		//  * See function for @AutoHooks
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_custom_constructor = 1.0;
		public function custom_constructor($type = '', $custom_constructor_class = FALSE) {
			
			// If our $custom_constructor_class variable is TRUE
			
			if ($custom_constructor_class) {
				
				// Set our $this_class variable
				
				$this_class = $custom_constructor_class;
				
				// If our $this_class variable exists
				
				if (class_exists($this_class)) {
					
					// Load our developer's custom action
					// @AutoHook->Class_Name_Here-before-{$type}_constructor
					//  ie: $type = "before" or "after"
					
					theme()->hook->do_action($this_class, "before-{$type}_constructor", TRUE);
					
					// Load this custom $class constructor
					
					$class::__construct();
					
					// Load our developer's custom action
					// @AutoHook->Class_Name_Here-after-{$type}_constructor
					//  ie: $type = "before" or "after"
					
					theme()->hook->do_action($this_class, "after-{$type}_constructor", TRUE);
					
				}
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->function_constructor()
		// @Description
		//  This function dynamically creates custom before and after actions and calls our function dynamically when the $status is not empty
		//  * See function for @AutoHooks
		// @Todo->updated_class_detection_to_use_is_object()_to_auto_detect_and_remove_$method_arg
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_function_constructor = 3.0;
		public function function_constructor($class = '', $load_function = FALSE, $function = '', $method = FALSE) {
			
			// If our $load_function variable is TRUE
			
			if ($load_function) {
				
				// Set our $this_class variable
				
				$this_class = $class;
				
				// Load our developer's custom action
				// @AutoHook->Class_Name_Here-before-$function
				
				theme()->hook->do_action($this_class, "before-{$function}", TRUE);
				
				// If our $method variable is empty
				// @Note->object_method
				//  $obj->$function is our default method
				
				if (empty($method)) {
					
					// If our $function_name method exists within our $this_class variable
					
					if (method_exists($this_class, $function)) {
						
						// Load our $function function
						
						$this_class->initialize($function);
						
					}
					
				}
				
				// If our $method variable is static
				// @Note->static_method
				//  $this_class::$function() is available if you need to use
				
				elseif ($method == 'static') {
					
					// If our $function method exists within our $this_class variable
					
					if (method_exists($this_class, $function)) {
						
						// Load our $function function
						
						$this_class::initialize($function);
						
					}
					
				}
				
				// If our $method variable is normal
				// @Note->normal_method
				//  $function() is available if you need to use
				
				elseif ($method == 'normal') {
					
					// If our $function exists
					
					if (function_exists($function)) {
						
						// Load our $function
						
						$function;
						
					}
					
				}
				
				// Load our developer's custom action
				// @AutoHook->Class_Name_Here-after-$function
				
				theme()->hook->do_action($this_class, "after-{$function}", TRUE);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->path()
		// @Description
		//  This function 
		// @Usage
		//  
		// @Todo->add_support_for_all_shorthand_paths
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_path = 2.0;
		public function path($file_location) {
			
			// Set our $this_location variable
			
			$this_location = $file_location;
			
			// Set our $this_location developer's filter
			// @AutoHook->Filter->CustomTheme-function-path
			
			$this_location = theme()->hook->filter('function-' . __FUNCTION__, TRUE, $this_location);
			
			// Asset Paths
			
			// $this->asset_path   = __DIR__ . '/' . $this->_asset   . '/';
			// $this->core_path    = $this->asset_path   . $this->_core    . '/';
			// $this->browser_path = $this->core_path    . $this->_browser . '/';
			
			// Content Paths
			
			// $this->output_path  = __DIR__ . '/' . $this->_output  . '/';
			// $this->template_path = $this->content_path . $this->_template . '/';
			
			// Setting Paths
			
			// $this->setting_path      = __DIR__ . '/'  . $this->_setting      . '/';
			// $this->framework_path    = $this->setting_path  . $this->_framework    . '/';
			// $this->install_path      = $this->setting_path  . $this->_install      . '/';
			// $this->options_path      = $this->setting_path  . $this->_options      . '/';
			// $this->global_path       = $this->options_path  . $this->_global       . '/';
			// $this->backend_path      = $this->options_path  . $this->_backend      . '/';
			// $this->frontend_path     = $this->options_path  . $this->_frontend     . '/';
			// $this->page_builder_path = $this->setting_path  . $this->_page_builder . '/';
			// $this->product_path      = $this->setting_path  . $this->_product      . '/';
			
			// Framework File Paths
			
			// $this->advanced_framework = $this->framework_path     . $this->_advanced . '/';
			// $this->advanced_framework = $this->advanced_framework . $this->_load;
			
			// $this->advanced_framework_settings = $this->framework_path . $this->_advanced_settings . '/';
			// $this->advanced_framework_settings = $this->advanced_framework_settings . $this->_load;
			
			// $this->simple_framework = $this->framework_path   . $this->_simple . '/';
			// $this->simple_framework = $this->simple_framework . $this->_load;
			
			// $this->simple_framework_settings = $this->framework_path . $this->_simple_settings . '/';
			// $this->simple_framework_settings = $this->simple_framework_settings . $this->_load;
			
			// $this->framework_assets = $this->setting_path     . $this->_asset . '/';
			// $this->framework_assets = $this->framework_assets . $this->_load;
			
			// Page Builder Paths
			
			// $this->bb_path = $this->page_builder_path . $this->beaver_builder  . '/';
			// $this->cs_path = $this->page_builder_path . $this->cornerstone     . '/';
			// $this->db_path = $this->page_builder_path . $this->divi_builder    . '/';
			// $this->fb_path = $this->page_builder_path . $this->fusion_builder  . '/';
			// $this->kc_path = $this->page_builder_path . $this->king_composer   . '/';
			// $this->vc_path = $this->page_builder_path . $this->visual_composer . '/';
			
			// If our $this_location variable does exist
			
			return (!empty($this_location)) ? plugin_dir_path($this_location) : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->url()
		// @Description
		//  This function 
		// @Usage
		//  
		// @Todo->add_support_for_all_shorthand_paths
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_url = 2.0;
		public function url($file_location) {
			
			// Set our $this_location variable
			
			$this_location = $file_location;
			
			// Set our $this_location developer's filter
			// @AutoHook->Filter->CustomTheme-function-path
			
			$this_location = theme()->hook->filter('function-' . __FUNCTION__, TRUE, $this_location);
			
			// Asset Paths
			
			// $this->asset_path   = __DIR__ . '/' . $this->_asset   . '/';
			// $this->core_path    = $this->asset_path   . $this->_core    . '/';
			// $this->browser_path = $this->core_path    . $this->_browser . '/';
			
			// Content Paths
			
			// $this->output_path  = __DIR__ . '/' . $this->_output  . '/';
			// $this->template_path = $this->content_path . $this->_template . '/';
			
			// Setting Paths
			
			// $this->setting_path      = __DIR__ . '/'  . $this->_setting      . '/';
			// $this->framework_path    = $this->setting_path  . $this->_framework    . '/';
			// $this->install_path      = $this->setting_path  . $this->_install      . '/';
			// $this->options_path      = $this->setting_path  . $this->_options      . '/';
			// $this->global_path       = $this->options_path  . $this->_global       . '/';
			// $this->backend_path      = $this->options_path  . $this->_backend      . '/';
			// $this->frontend_path     = $this->options_path  . $this->_frontend     . '/';
			// $this->page_builder_path = $this->setting_path  . $this->_page_builder . '/';
			// $this->product_path      = $this->setting_path  . $this->_product      . '/';
			
			// Framework File Paths
			
			// $this->advanced_framework = $this->framework_path     . $this->_advanced . '/';
			// $this->advanced_framework = $this->advanced_framework . $this->_load;
			
			// $this->advanced_framework_settings = $this->framework_path . $this->_advanced_settings . '/';
			// $this->advanced_framework_settings = $this->advanced_framework_settings . $this->_load;
			
			// $this->simple_framework = $this->framework_path   . $this->_simple . '/';
			// $this->simple_framework = $this->simple_framework . $this->_load;
			
			// $this->simple_framework_settings = $this->framework_path . $this->_simple_settings . '/';
			// $this->simple_framework_settings = $this->simple_framework_settings . $this->_load;
			
			// $this->framework_assets = $this->setting_path     . $this->_asset . '/';
			// $this->framework_assets = $this->framework_assets . $this->_load;
			
			// Page Builder Paths
			
			// $this->bb_path = $this->page_builder_path . $this->beaver_builder  . '/';
			// $this->cs_path = $this->page_builder_path . $this->cornerstone     . '/';
			// $this->db_path = $this->page_builder_path . $this->divi_builder    . '/';
			// $this->fb_path = $this->page_builder_path . $this->fusion_builder  . '/';
			// $this->kc_path = $this->page_builder_path . $this->king_composer   . '/';
			// $this->vc_path = $this->page_builder_path . $this->visual_composer . '/';
			
			// If our $this_location variable does exist
			
			return (!empty($this_location)) ? plugin_dir_path($this_location) : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->file()
		// @Description
		//  This function provides an easy and readable way to path of a file via the host system or url
		//  This plays and looks well with our load_asset() function
		//  See asset related files for full examples
		// @Usage
		//  $this->file('url', __DIR__, 'folder-path');
		//  $this->file('url', __DIR__, 'folder-path');
		// @Todo->get_rid_of_double_check_consildate_to_path_and_url_functions_using_wordpress_plugin_dir_functions
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_file = 1.0;
		public function file($type = '', $dir = '', $folder = '', $dev_data = array()) {
			
			// If our type variable is path
			
			if ($type == 'path') {
				
				// Set our path variable
				
				$path = $dir . '/' . $folder;
				
			}
			
			// If our type variable is url
			
			elseif ($type == 'url') {
				
				// Set our path variable
				
				$path = substr($dir, strlen($_SERVER['DOCUMENT_ROOT'])) . '/' . $folder;
				
			}
			
			else {
				
				
				
			}
			
			// If our path variable is not empty let's return it otherwise FALSE
			
			return (!empty($path)) ? $path : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->load_file()
		// @Description
		//  This function simply checks if the file passed exists, if it does it performs a require_once()
		//   of the file and returns TRUE, if the file does not exists it returns FALSE.
		//  This just allows our code writing to be more simplified/readble/and lighter.
		// @Usage
		//  theme()->global->load_file($this->install, $this->load_install, wp_get_current_user()->user_email, 'noahj@fearlessmode.com');
		//  theme()->global->load_file($this->install, $this->load_install, wp_get_current_user()->user_email, 'noahj@fearlessmode.com');
		// @Todo->add_to_error_log_for_missing_files_just_in_case
		// @Todo->rename_to_file_replacing_previous_file_function
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_load_file = 3.0;
		public function load_file($tag = '', $file_path = '', $condition = NULL) {
			
			// If our $condition variable is TRUE
			// @Example_Usage
			//  is_admin(), TRUE // is TRUE when the Wordpress admin is attempting to display
			//  is_admin(), FALSE // NOTICE: this is still TRUE when the Wordpress admin is NOT attempting to display because FALSE is TRUE for this statement
			//  wp_get_current_user()->user_email, 'name@email.com' // is TRUE when the current user's email is "name@email.com"
			
			if (!is_null($condition)) {
				
				// If our $condition variable is TRUE
				
				if ($condition) {
					
					// Set our $this_file variable
					
					$this_file = $file_path;
					
					// Set our $this_file developer's filter
					// @AutoHook->Filter->CustomTheme-$tag
					
					$this_file = theme()->hook->filter($tag, FALSE, $this_file);
					
					// If our $this_file variable does exist
					
					if (file_exists($this_file)) {
						
						// Load our $this_file variable
						
						require_once($this_file);
						
					}
					
				}
				
			}
			
			// If our $condition variable is not callable
			
			else {
				
				// Set our $this_file variable
				
				$this_file = $file_path;
				
				// If our $this_file variable does exist
				
				if (file_exists($this_file)) {
					
					// Load our $this_file variable
					
					require_once($this_file);
					
				}
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->http_method()
		// @Description
		//  This function helps return our user's current http method
		// @Usage
		//  $this->http_method();
		//  outputs... https// or http://
		// @Todo->make_theme_option_to_hard_set_in_case_of_future_issues
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_http_method = 1.0;
		public function http_method()  {
			
			// Set our $http_method variable
			
			$http_method = (isset($_SERVER['HTTPS']) and filter_var($_SERVER['HTTPS'], FILTER_VALIDATE_BOOLEAN)) ? 'https://' : 'http://';
			
			// If our $http_method variable is not empty let's return it otherwise FALSE
			
			return (!empty($http_method));
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->indexes()
		// @Description
		//  This function allows us to simply pass a string of index values that will be converted to an array
		//  * See function code for more details
		// @Usage
		//  $this->indexes('index_1, index_2, index_3');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_indexes = 1.0;
		public function indexes($indexes = '') {
			
			// If our indexes variable is a string
			
			if (is_string($indexes)) {
				
				// Our indexes variable is not empty
				// @Note
				//  We will call the key => values of our array later to determine the output of needed index options
				// @Note
				//  We can pass up to 10 indexes
				
				// @Note
				//  For conveinience an explnation of what happens is here, but this operation is taken
				
				// Clean our indexes variable by removing all spaces, if any
				// @Example @Input
				//  $indexes = 'Index_Value_1, Index_Value_2, Index_Value_3'
				// @Example @Ouput
				//  $sanitized_index = 'Index_Value_1,Index_Value_2,Index_Value_3'
				
				$sanitized_index = str_replace(' ', '', $indexes);
				
				// Convert our sanitized index variable to an array separating the key => values by the commas used
				// @Example @Input
				//  $sanitized_index = 'Index_Value_1,Index_Value_2,Index_Value_3'
				// @Example @Input
				//  $indexes = Array ( [0] => Index_Value_1 [1] => Index_Value_2 [2] => Index_Value_3 )
				
				$indexes = explode(',', $sanitized_index);
				
			}
			
			// If our indexes variable is not empty and is an array let's return it otherwise FALSE
			
			return (!empty($indexes) and is_array($indexes)) ? $indexes : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->var_check()
		// @Description
		//  This function 
		// @Reference
		//  https://codex.wordpress.org/Function_Reference/is_wp_error
		// @Todo->consider_adding_dev_report_error_as_well
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_var_check = 1.0;
		public function var_check($var_data = '') {
			
			// If our $var_data variable is not empty and is not a Wordpress error
			
			if (!empty($var_data) and (!is_wp_error($var_data))) {
				
				// return TRUE
				
				return TRUE;
				
			}
			
			// If our $var_data is not empty and has triggered a Wordpress error
			
			elseif (!empty($var_data) and (is_wp_error($var_data))) {
				
				// Set our $error_message variable
				// @Todo->check_if_this_works??_the_var_data_object??
				
				$error_message = $var_data->get_error_message();
				
				// Echo our error message
				
				echo '<div id="message" class="error"><p>' . (!empty($error_message)) . '</p></div>';
				
				// return FALSE
				
				return FALSE;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->class_type()
		// @Description
		//  This function 
		// @Usage
		//  $class = self::class_type($this);
		//  $class = self::class_type(__CLASS__);
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_class_type = 1.1;
		public function class_type($class) {
			
			// Set our $class_status variable
			
			$class_status = (is_object($class)) ? get_class($class) : $class;
			
			// If our $class_status variable is not empty return it otherwise FALSE
			
			return (!empty($class_status)) ? $class_status : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->is()
		// @Description
		//  This function is a simple validation function to determine if something is TRUE or not
		//   The difference is that this function considers "TRUE" and "true" as TRUE
		//   This was needed because a previous frameworks was saving TRUE as "TRUE" and for some reason that was returning FALSE
		// @Usage
		//  if ($this->is($var_data)) { Your_Code... } else { Your_Code... }
		// @Note
		//  Will probably use this is() for something else since this is not needed anymore
		// @Todo->update_code_elsewhere_to_reuse_this_function
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_is = 1.0;
		public function is($var_data) {
			
			// Set our active status variable
			
			$active_status = ($var_data or $var_data == 'TRUE' or $var_data == '1') ? TRUE : FALSE;
			
			// If our active status variable is not empty let's return it otherwise FALSE
			
			return (!empty($active_status)) ? $active_status : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->context_is()
		// @Description
		//  This function is a simple wrapper for various Wordpress functions
		//   Instead of trying to remember the unique function name for each context, just remember one function
		//   and pass the appropiate context for your need
		// @Usage
		//  if (self::context_is('backend')) { Your_Code... } else { Your_Code... }
		//  if (self::context_is('frontend')) { Your_Code... } else { Your_Code... }
		//  if (self::context_is('logged_in')) { Your_Code... } else { Your_Code... }
		//  if (self::context_is('logged_out')) { Your_Code... } else { Your_Code... }
		//  if (self::context_is('multi_site')) { Your_Code... } else { Your_Code... }
		//  * See function code for more details
		// @Todo->probably_consildate_to_is_function
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_context_is = 2.0;
		public function context_is($context = '') {
			
			// If context variable is CustomOption
			// @Todo->improve
			
			if ($context == 'CustomOption' or $context == 'customoption' or $context == 'Custom-Option' or $context == 'custom-option' or $context == 'Custom_Option' or $context == 'custom_option') {
				
				// Set our theme settings variable
				
				$theme_settings = get_option('__settings__');
				
				// If our theme settings variable is set to TRUE
				
				if (!empty($theme_settings)) {
					
					// Set our custom option status variable
					//  @Todo
					//   Set this variable to a $this->theme_setting() function so if we need to update, this will be in that central update automatically
					
					$custom_option_status = (isset($theme_settings['custom_option']['option']['true'])) ? TRUE : FALSE;
					
					// Set our context status variable
					
					$context_status = $custom_option_status;
					
				}
				
				// Else return
				
				else { return; }
				
			}
			
			// If context variable is backend
			
			elseif ($context == 'backend' or $context == 'Backend' or $context == 'admin') {
				
				// Set our context status variable
				
				$context_status = is_admin();
				
			}
			
			// If context variable is frontend
			
			elseif ($context == 'frontend' or $context == 'Frontend' or $context == 'public') {
				
				// Set our context status variable
				
				$context_status = !is_admin();
				
			}
			
			// If context variable is logged_in
			
			elseif ($context == 'logged_in' or $context == 'logged-in') {
				
				// Set our context status variable
				
				$context_status = is_user_logged_in();
				
			}
			
			// If context variable is logged_out
			
			elseif ($context == 'logged_out' or $context == 'logged-out') {
				
				// Set our context status variable
				
				$context_status = !is_user_logged_in();
				
			}
			
			// If context variable is multi_site
			
			elseif ($context == 'multi_site' or $context == 'multisite' or $context == 'multi-site') {
				
				// Set our context status variable
				
				$context_status = is_multisite();
				
			}
			
			// If context variable is not_multi_site
			
			elseif ($context == 'not_multi_site' or $context == 'not_multisite' or $context == 'not-multi-site') {
				
				// Set our context status variable
				
				$context_status = !is_multisite();
				
			}
			
			// If context variable is kc_live_editor
			
			elseif ($context == 'kc_live_editor' or $context == 'kc-live-editor') {
				
				// Set our context status variable
				
				$context_status = (isset($_GET['kc_action']) and !empty($_GET['kc_action']));
				
			}
			
			// If our context variable is install plugins
			
			elseif ($context == 'install_plugins' or $context == 'install-plugins') {
				
				// Set our context status variable
				
				$context_status = current_user_can('install_plugins');
				
			}
			
			// If our function name variable is site_url
			
			elseif ($context == 'site_url') {
				
				// Set our context status variable
				
				$context_status = site_url();
				
			}
			
			// If our function name variable is includes_url
			
			elseif ($context == 'includes_url') {
				
				// Set our context status variable
				
				$context_status = includes_url();
				
			}
			
			// If our function name variable is admin_url
			
			elseif ($context == 'admin_url') {
				
				// Set our context status variable
				
				$context_status = admin_url();
				
			}
			
			// If our context status variable is not empty let's return it otherwise FALSE
			
			return (!empty($context_status)) ? $context_status : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->post_type()
		// @Description
		//  This function simply runs a routine to determine what the current post type is
		//   @ https://developer.wordpress.org/reference/functions/get_post_type/
		// @Usage
		//  if (theme()->global->post_type() == 'your-post-type') { Your_Code... } else { Your_Code... }
		// @Contributor
		//  @ http://krautcoding.com/
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_post_type = 2.0;
		public function post_type() {
			
			// Set our various global variables
			
			global $post, $typenow, $current_screen;
			
			// We have a post so we can just get the post type from that
			
			if ($post and $post->post_type) {
				
				// Set our $post_type_status variable
				
				$post_type_status = $post->post_type;
				
			}
			
			// Check the global $typenow
			
			elseif ($typenow) {
				
				// Set our $post_type_status variable
				
				$post_type_status = $typenow;
				
			}
			
			// Check the global $current_screen Object
			
			elseif ($current_screen and $current_screen->post_type) {
				
				// Set our $post_type_status variable
				
				$post_type_status = $current_screen->post_type;
				
			}
			
			// Check the Post Type QueryString
			
			elseif (isset($_REQUEST['post_type'])) {
				
				// Set our $post_type_status variable
				
				$post_type_status = sanitize_key($_REQUEST['post_type']);
				
			}
			
			// Try to get via get_post(); Attempt A
			
			elseif (empty($typenow) and !empty($_GET['post'])) {
				
				// Set our post variable
				
				$post = get_post($_GET['post']);
				
				// Set our $post_type_status variable
				
				$post_type_status = $post->post_type;
				
			}
			
			// Try to get via get_post(); Attempt B
			
			elseif (empty($typenow) and !empty($_POST['post_ID'])) {
				
				// Set our post variable
				
				$post = get_post($_POST['post_ID']);
				
				// Set our $post_type_status variable
				
				$post_type_status = $post->post_type;
				
			}
			
			// Try to get via get_current_screen()
			
			elseif (function_exists('get_current_screen')) {
				
				// Set our current variable
				
				$current = get_current_screen();
				
				// If our current variable is set, is not FALSE and is the current post type
				
				if (isset($current) and ($current !== FALSE) and ($current->post_type)) {
					
					// Set our $post_type_status variable
					
					$post_type_status = $current->post_type;
					
				}
				
			}
			
			// If our $post_type_status variable is not empty let's return it otherwise FALSE
			
			return (!empty($post_type_status)) ? $post_type_status : FALSE;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->load()
		// @Description
		//  This function determines if our default action or our developer's custom action should load
		//  This allows developer to replace our actions at the same time our default action would fire using the add_action() call
		// @Note
		//  This function is a wrapper to connect our developer's replacement action with our default Wordpress action
		//  Our Worpress add_action() and add_filter() functions are running conditionally
		//   through our developer's trigger() function instead
		//  This allows extending custom action and filters to be ran automatically from our trigger() function
		//  Each trigger() allows the action, priority and div_container to be modified by an add_filter()
		//  * See our CustomTheme->trigger() for more details
		// @Reference
		//  https://codex.wordpress.org/Plugin_API/Action_Reference
		//  https://developer.wordpress.org/reference/functions/add_action/
		// @Usage
		//  For use as $object->function_name();
		// self::load('action', $this, $action, $function, $priority);
		// self::load('action', $this, 'init', 'func_name', 99);
		//  For use as $class::function_name();
		// self::load('action', __CLASS__, $action, $function, $priority);
		// self::load('action', __CLASS__, 'init', 'func_name', 99);
		//  For use as function_name();
		// $api->load('action', my-tag-here', add_action($your_normal_args_here));
		// $api->load('action', my-tag-here', add_action($your_normal_args_here), 99);
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_load = 8.0;
		public function load($method, $class, $action, $function = '', $priority = 10) {
			
			// Set our $this_class variable
			// @Note
			//  self::class_type() will return the string of the object class or the class string if not an object
			//  This means wether an object or not we will be referencing the class string
			// @Todo->think_about_moving_class_type_to_tag_function_to_automatically_calculate_and_reduce_code_needed_throughout_all_methods
			// @Todo->Important
			
			$this_class = self::class_type($class);
			
			// Set our $tag_1 and $tag_2 variables
			
			$tag_1 = $this_class;
			$tag_2 = $function;
			
			// If our developer's custom action does exist
			
			if (theme()->hook->has_action($tag_1, $tag_2, TRUE)) {
				
				// Load our developer's custom action
				// @AutoHook->Action->Class_Name_Here-$tag_2
				// @Note->this will replace the action from runnning with your action/function()
				
				theme()->hook->do_action($tag_1, $tag_2, TRUE);
				
			}
			
			// If our developer's custom action does not exist
			
			else {
				
				// If our $this_class class exists
				// @Note
				//  Currently we are trying to differentiate between a valid class passed and unique tag passed
				//  A forseen issue is that if the unique tag is a class but is not meant to be this won't call the correct function
				// @Todo->consider_preventive_fix
				
				if (class_exists($this_class)) {
					
					// Set our $method_status variable
					// @Note
					//  This determines wether to call this class function using $this or statically using $class::
					// via the Wordpress action api
					// @Reference
					//  https://codex.wordpress.org/Plugin_API/Action_Reference
					
					// If our $this_class is an object
					// @Note
					//  This calls the function as an object via the Wordpress add_action()
					// @Reference
					//  http://php.net/manual/en/language.oop5.basic.php
					
					// If our $this_class is not an object, ie: a string ie: __CLASS__
					// @Note
					//  This calls the function statically via the Wordpress add_action()
					// @Reference
					//  http://php.net/manual/en/language.constants.predefined.php
					
					// @Note
					//  array($class, $function) -> is equilavent to -> array($this, 'Function_Name')
					//   ie: add_action('init', array($this, 'Function_Name'));
					//  $class . "::$function"   -> is equilavent to -> __CLASS__ . "::Function_Name";
					//   ie: add_action('init', __CLASS__ . '::Function_Name');
					
					$method_status = (is_object($class)) ? array($class, $function) : $this_class . "::$function";
					
				}
				
				// If $this_class does not exist
				// @Note
				//  This (should) mean that our extending developer is not using classes but using this hookable load action
				//   and we need to load a function/add_action() not within a class
				//   so let's load the action only as they have passed it
				//  This allow the developer's custom action to be hooked automatically
				//   while not using functions within classes
				// @Note
				//  The benefit I see to this is developer's who are not use to classes or OOP
				//   and plugins already not using classes to still easily add support/contribute to our Custom Theme
				
				else {
					
					// Set our $method_status variable
					
					$method_status = $function;
					
				}
				
				// Set our $context variable
				//  @AutoHook->Filter->Class_Name_Here-$function-$method
				//  @AutoHook->Filter->Class_Name_Here-$function-action
				//  @AutoHook->Filter->Class_Name_Here-$function-filter
				// @Note
				//  This filter allows all of our actions and extending developer actions
				//   to change the action applied via the filter
				//  * See documenation for further application
				
				$context = theme()->hook->filter($this_class, "{$function}-{$method}", $action, TRUE);
				
				// Set our $priority variable
				//  @AutoHook->Filter->Class_Name_Here-$function-priority
				// @Note
				//  This filters allows all of our action priorites and extending developer action priorites
				//   to change the action priority applied via the filter
				//  * See documenation for further application
			
				$priority = theme()->hook->filter($this_class, "{$function}-priority", $priority, TRUE);
				
				// Load our developer's custom action hook
				// @Hook->Action->Class_Name_Here-before-$function
				
				theme()->hook->do_action($this_class, "before-{$function}", TRUE);
				
				// Set our $action_type variable
				// @Note
				//  This becomes add_action or add_filter
				
				$action_type = "add_$method";
				
				// Load our $action_type
				
				$action_type($context, $method_status, $priority);
				
				// Load our developer's custom action hook
				// @Hook->Action->Class_Name_Here-after-$function
				
				theme()->hook->do_action($this_class, "after-{$function}", TRUE);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->load_asset()
		// @Description
		//  This function simple consildation of Wordpress's wp_enqueue_* features with other relative asset features
		//   such as loading .min version, inline vs. enqueue
		//   @ https://developer.wordpress.org/reference/functions/wp_enqueue_script/
		// @Usage
		//  $asset_url = $this->file('url', __DIR__, 'folder-path');
		//  $this->load_asset('css-id-here', 'css', 'inline', 'live', $asset_url, '', 'all');
		//  $this->load_asset('css-id-here', 'css', 'enqueue', 'local', $asset_url, '', 'all');
		//  $asset_path = $this->file('path', __DIR__, 'folder-path');
		//  $this->load_asset('js-id-here', 'js', 'inline', 'live', $asset_path, '', 'all');
		//  $this->load_asset('js-id-here', 'js', 'enqueue', 'local', $asset_path, '', 'all', 'in_footer');
		// @Todo->create_sass_support
		//  $asset_path = $this->file('path', __DIR__, 'folder-path');
		//  $this->load_asset('scss-id-here', 'scss', 'inline', 'live', $asset_path, '', 'all');
		//  $this->load_asset('scss-id-here', 'scss', 'enqueue', 'local', $asset_path, '', 'all', 'in_footer');
		// @Note
		//  All asset id's will be prepended with "custom-theme-your-asset-id"
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_load_asset = 4.0;
		public function load_asset($name = '', $type = '', $method = '', $version = '', $path = '', $dependencies = array(), $media_screen = '', $in_footer_status = '', $localize = array()) {
			
			// If our method variable is inline or echo
			
			if ($method == 'inline' or $method == 'echo') {
				
				// If our version variable is live, min, ugly or prod (production)
				
				if ($version == 'live' or $version == 'min' or $version == 'ugly' or $version == 'prod') {
					
					// Set our file variable as .min.*file_xtension
					
					$file = file_get_contents($path . $this->min . '.' . $type);
					
				}
				
				// If our version variable is local, clean, pretty or dev (development)
				
				elseif ($version == 'local' or $version == 'clean' or $version == 'pretty' or $version == 'dev') {
					
					// Set our file variable as .*file_xtension
					
					$file = file_get_contents($path . '.' . $type);
					
				}
				
				// Change our file variable name to asset content to keep our language usage in context
				
				$asset_content = $file;
				
				// Load our developer's custom action hook
				// @Hook->Action->CustomTheme-before-$style_id_name
				
				theme()->hook->do_action("before-$style_id_name", FALSE, TRUE);
				
				// If type variable is css
				
				if ($type == 'css') {
					
					// Open our css output identifying container
					
					self::html('css', 'open', $name);
					
					// Echo our asset content
					
					echo $asset_content;
					
					// Close our css output identifying container
					
					self::html('css', 'close', $name);
					
				}
				
				// If type variable is js
				
				elseif ($type == 'js') {
					
					// Open our js output identifying container
					
					self::html('js', 'open', $name);
					
					// Echo our asset content
					
					echo $asset_content;
					
					// Close our js output identifying container
					
					self::html('js', 'close', $name);
					
				}
				
				// Load our developer's custom action hook
				// @Hook->Action->CustomTheme-after-$style_id_name
				
				theme()->hook->do_action("after-$style_id_name", FALSE, TRUE);
				
			}
			
			// If our method variable is enqueue or http
			
			elseif ($method == 'enqueue' or $method == 'http') {
				
				// Set our variable public name variables
				
				$public_name = strtoupper($name);
				$public_name = str_replace('-', ' ', $public_name);
				$public_name = str_replace('_', ' ', $public_name);
				
				// @Note
				//  If our media screen variable is empty
				//   let's set our media screen status variable to "all" which will output 
				//   <link rel="stylesheet" id="*id-name-css" href="*file_path" media="all" >
				//   Otherwise let's output the developer's custom media screen value 
				
				$media_screen_status = (empty($media_screen)) ? 'all' : $media_screen;
				
				// If our version variable is live, min, ugly or prod (production)
				
				if ($version == 'live' or $version == 'min' or $version == 'ugly' or $version == 'prod') {
					
					// Set our file variable as .min.*file_xtension
					
					$file = $path . $this->min . '.' . $type;
					
				}
				
				// If our version variable is local, clean, pretty or dev (development)
				
				elseif ($version == 'local' or $version == 'clean' or $version == 'pretty' or $version == 'dev') {
					
					// Set our file variable as .*file_xtension
					
					$file = $path . '.' . $type;
					
				}
				
				// Change our file variable name to asset to keep our language usage in context
				
				$asset = $file;
				
				// Set our variable style id name variables
				
				$style_id_name = strtolower($name);
				$style_id_name = str_replace(' ', '-', $style_id_name);
				$style_id_name = str_replace('_', '-', $style_id_name);
				
				// Load our developer's custom action hook
				// @Hook->Action->CustomTheme-before-$style_id_name
				
				theme()->hook->do_action("before-$style_id_name", FALSE, TRUE);
				
				// If asset variable is css
				
				if ($type == 'css') {
					
					// Enqueue this css asset
					
					wp_register_style("custom-theme-$style_id_name", $asset, $dependencies, '', $media_screen_status);
					wp_enqueue_style("custom-theme-$style_id_name");
					
				}
				
				// If asset variable is scss
				
				elseif ($type == 'scss') {
					
					// Enqueue this scss asset
					
					wp_register_style("custom-theme-$style_id_name", $asset, $dependencies, '', $media_screen_status);
					wp_enqueue_style("custom-theme-$style_id_name");
					
				}
				
				// If asset variable is js
				
				elseif ($type == 'js') {
					
					// If our in footer status variable is in_footer, in-footer or footer
					
					if ($in_footer_status == 'in_footer' or $in_footer_status == 'in-footer' or $in_footer_status == 'footer') {
						
						// Set our footer status variable to TRUE
						
						$footer_status = TRUE;
						
					}
					
					// If our in footer status variable is empty
					
					else {
						
						// Set our $footer_status to FALSE
						
						$footer_status = FALSE;
						
					}
					
					// Register this script
					
					wp_register_script("custom-theme-$style_id_name", $asset, $dependencies, '', $footer_status);
					
					// If our $localize variable is not empty
					
					if (!empty($localize)) {
						
						// Set our $localized_object_name
						// @Note
						//  This should be a string
						// @Reference
						//  https://developer.wordpress.org/reference/functions/wp_localize_script/
						//  https://codex.wordpress.org/Function_Reference/wp_localize_script
						
						$localized_object_name = $localize[0];
						
						// Set our $localized_data
						// @Note
						//  This can be an array
						// @Reference
						//  https://developer.wordpress.org/reference/functions/wp_localize_script/
						//  https://codex.wordpress.org/Function_Reference/wp_localize_script
						
						$localized_data = $localize[1];
						
						// Localize our script
						
						wp_localize_script("custom-theme-$style_id_name", $localized_object_name, $localized_data);
						
					}
					
					// Enqueue our script
					
					wp_enqueue_script("custom-theme-$style_id_name");
					
				}
				
				// Load our developer's custom action hook
				// @Hook->Action->CustomTheme-after-$style_id_name
				
				theme()->hook->do_action("after-$style_id_name", FALSE, TRUE);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->remove_asset()
		// @Description
		//  This function is a simple consildation of Wordpress's wp_dequeue_*
		//   This is easier to remember/simpler to use.
		//   @ https://developer.wordpress.org/reference/functions/wp_dequeue_style/
		//   @ https://developer.wordpress.org/reference/functions/wp_dequeue_script/
		// @Usage
		//  $this->remove_asset('asset-id', 'css');
		//  $this->remove_asset('asset-id', 'js');
		//  $this->remove_asset('asset-id', 'scss');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_remove_asset = 2.0;
		public function remove_asset($name = '', $type = '') {
			
			// If our type variable is css
			// @Todo->Add_scss_support
			
			if ($type == 'css' or $type == 'scss') {
				
				// Dequeue and deregister this css asset
				
				wp_dequeue_style($name);
				wp_deregister_style($name);
				
			}
			
			// If our type variable is js
			
			elseif ($type == 'js') {
				
				// Dequeue and deregister this js asset
				
				wp_dequeue_script($name);
				wp_deregister_script($name);
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->html()
		// @Description
		//  This function is a simple helper for outputting HMTL (comments, css and js tags)
		//  This allows us to write in PHP instead of PHP and HTML causing ugly code writing and inconsistencies
		// @Usage
		//  $this->html('comment', 'start', 'My_Custom_HTML_Comment_Here');
		//  $this->html('comment', 'end', 'My_Custom_HTML_Comment_Here');
		//  $this->html('css', 'open', 'My_Custom_Css_ID_Name_Here');
		//  $this->html('css', 'close', 'My_Custom_Css_ID_Name_Here');
		//  $this->html('js', 'open', 'My_Custom_Js_ID_Name_Here');
		//  $this->html('js', 'close', 'My_Custom_Js_ID_Name_Here');
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_html = 3.0;
		public function html($type = '', $operation_type = '', $name = '', $var_1 = '') {
			
			// If our type variable is comment
			
			if ($type == 'comment') {
				
				// Rename our name variable to comment for more appropiate launguage use
				
				$comment = $name;
				
				// If our operation variable is start
				
				if ($operation_type == 'start') {
					
					// Output our comment div
					
					echo PHP_EOL . "<!-- - - - - - - - - - - - {$comment} - - - - - - - - - - - -->" . PHP_EOL;
					
				}
				
				// If our operation variable is end
				
				elseif ($operation_type == 'end') {
					
					// Output our comment div
					
					echo PHP_EOL . "<!-- {$comment} -->" . PHP_EOL;
					
				}
				
			}
			
			// If our type variable is css
			
			elseif ($type == 'css') {
				
				// If our name variable is not empty
				
				if (!empty($name)) {
					
					// Set our variable public name variables
					
					$public_name = strtoupper($name);
					$public_name = str_replace('-', ' ', $public_name);
					$public_name = str_replace('_', ' ', $public_name);
					
					// Set our variable style id name variables
					
					$style_id_name = strtolower($name);
					$style_id_name = str_replace(' ', '-', $style_id_name);
					$style_id_name = str_replace('_', '-', $style_id_name);
					
					// If our operation type variable is open
					
					if ($operation_type == 'open') {
						
						// Load our open css container
						
						echo PHP_EOL;
						
						echo "<!-- - - - - - - - - - - - START \"CUSTOM THEME ". $public_name ."\" STYLES - - - - - - - - - - - -->";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<style type=\"text/css\" id=\"custom-theme-". $style_id_name ."-css\">";
						
						echo PHP_EOL;
						
					}
					
					// If our operation type variable is close
					
					elseif ($operation_type == 'close') {
						
						// Close our open css container
						
						echo PHP_EOL;
						
						echo "</style>";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<!-- END \"CUSTOM THEME ". $public_name ."\" STYLES -->";
						
						echo PHP_EOL.PHP_EOL;
						
					}
					
				}
				
			}
			
			// If our type variable is js
			
			elseif ($type == 'js') {
				
				// If our name variable is not empty
				
				if (!empty($name)) {
					
					// Set our variable public name variables
					
					$public_name = strtoupper($name);
					$public_name = str_replace('-', ' ', $public_name);
					$public_name = str_replace('_', ' ', $public_name);
					
					// Set our variable style id name variables
					
					$style_id_name = strtolower($name);
					$style_id_name = str_replace(' ', '-', $style_id_name);
					$style_id_name = str_replace('_', '-', $style_id_name);
					
					// If our operation type variable is open
					
					if ($operation_type == 'open') {
						
						// Load our open js container
						
						echo PHP_EOL;
						
						echo "<!-- - - - - - - - - - - - START \"CUSTOM THEME ". $public_name ."\" SCRIPTS - - - - - - - - - - - -->";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<script type=\"text/javascript\" id=\"custom-theme-". $style_id_name ."-js\">";
						
						echo PHP_EOL;
						
					}
					
					// If our operation type variable is close
					
					elseif ($operation_type == 'close') {
						
						// Close our open js container
						
						echo PHP_EOL;
						
						echo "</script>";
						
						echo PHP_EOL.PHP_EOL;
						
						echo "<!-- END \"CUSTOM THEME ". $public_name ."\" SCRIPTS -->";
						
						echo PHP_EOL.PHP_EOL;
						
					}
					
				}
				
			}
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
