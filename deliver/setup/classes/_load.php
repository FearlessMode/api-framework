<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme class extending our CustomTheme_API class
	// @Note
	//  This is extending our CustomTheme_API which loads all our path related variables
	//   into our cunstructor. We are doing that simply for organization so this core class can focus
	//   only on loading our needed files (ie: initializing/loading our theme) and so that all our path related
	//   variables can be managed separate to also be used by our path() shorthand function for devs needed it
	// * See CustomTheme_Global_Paths_API for more details
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme extends CustomTheme_API {
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct($auto_tag = '', $auto_path = '', $auto_url = '') {
			
			// Load our parent constructor
			// @Note
			//  __CLASS__ creates a class string for $this->tag, $this->hook, $this->class
			//  __FILE__ creates $this->path relative to this file path
			//  __DIR__ create $this->url relative to this file url
			
			parent::__construct($auto_tag, $auto_path, $auto_url);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->initialize()
		// @Description
		//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_initialize = 1.0;
		public function initialize() {
			
			// Theme Load
			// @Hook->Filter->CustomTheme-install
			
			theme()->global->load_file($this->install, $this->load_install);
			
			// Load our CustomTheme_Global_Options class
			// @Hook->Filter->CustomTheme-global
			
			theme()->global->load_file($this->global, $this->load_global_options);
			
			// If this is the Wordpress frontend
			
			if (is()->frontend) {
				
				// Load our CustomTheme_Frontend_Options class
				// @Hook->Filter->CustomTheme-frontend
				
				// theme()->global->load_file('frontend', $this->load_frontend_options);
				
			}
			
			// If this is the Wordpress backend
			
			else {
				
				// Load our Product methods
				// @Todo->!IMPORTANT_not_sure_what_this_does_right_now_:D
				
				// theme()->product->initialize();
				// do_action('CustomTheme-product_initialized');
				
				// Load our CustomTheme_Framework class
				// @Hook->Filter->CustomTheme-framework
				
				theme()->global->load_file($this->framework, $this->load_framework);
				
				// Load our CustomTheme_Backend_Options class
				// @Hook->Filter->CustomTheme-backend
				
				// theme()->global->load_file($this->backend, $this->load_backend_options);
				
				// add_action('save_post',                   __CLASS__ . '::save_post', 10, 3);
				// add_action('save_post_post-type-layout',  __CLASS__ . '::save_post_type_layout', 10, 3);
				// add_action('save_post_category-layout',   __CLASS__ . '::save_category_layout',  10, 3);
				// add_action('post_submitbox_misc_actions', __CLASS__ . '::adjust_edit_layout_button');
				
			}
			
		}
		
	}

}

// If Wordpress is defined as not running stop execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
