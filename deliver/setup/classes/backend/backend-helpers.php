<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Backend_Helpers_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Backend_Helpers_API extends CustomTheme {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Load our parent constructor
			// @Note
			//  __CLASS__ creates a class string for $this->tag, $this->hook, $this->class
			//  __FILE__ creates $this->path relative to this file path
			//  __DIR__ create $this->url relative to this file url
			
			parent::__construct(__CLASS__);
			
			// Set our various variables
			
			// $this->var = 'Value_Here';
			
			// Load our initialize methods
			
			// self::initialize();
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->notice()
		// @Description
		//  This function adds a notice to the stored notices to be displayed the next time the admin_notices action runs.
		// @Note
		// @Usage
		// @Reference
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_notice = 1.0;
		public function notice($type, $message) {
			
			// Set our $classes variable
			
			$classes = "$this->theme_slug notice notice-$type is-dismissible";
			
			// Print our formatted notice type string
			
			printf('<div class="%1$s"><p>%2$s</p></div>', $classes, $message);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->menu()
		// @Description
		//  @Todo->description
		// @Usage
		// $menu_title       = 'Theme Settings'; // Menu Title
		// $menu_page_title  = $menu_title; // Menu Page Title
		// $menu_slug        = 'admin.php?page=custom-theme-theme-settings'; // Menu URL
		// $menu_icon        = 'dashicons-admin-site'; // Menu Icon - Defaults to NULL
		// $menu_position    = 2;     // Menu position - Defaults to NULL
		// $menu_text_domain = FALSE; // Defaults to our text domain
		// $menu_capability  = FALSE; // Defaults to 'edit_theme_options'
		// $menu_function    = FALSE; // Defaults to NULL
		// $this->menu($menu_title, $menu_page_title, $menu_slug, $menu_icon, $menu_position, $menu_text_domain, $menu_capability, $menu_function);
		
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_menu = 1.0;
		public function menu($menu_title = '', $menu_page_title = '', $menu_slug = '', $menu_icon = NULL, $menu_position = NULL, $text_domain = '', $menu_capability = '', $menu_function = '') {
			
			// @Note
			//  This is the order I think the Wordpress add_submenu_page() should go in
			//  It makes more sense to me in it's order so this is why our $this->menu() has it's $args place accordingly
			// add_menu_page($menu_title, $menu_page_title, $menu_url, $menu_icon, $menu_position, $menu_text_domain, $menu_capability, $menu_function);
			
			// Set our $text_domain_status variable
			
			$text_domain_status = (!empty($text_domain)) ? $text_domain : theme()->dev->text_domain();
			
			// Menu Page Title
			
			{
				
				// Set our $menu_title_status variable
				// @Note
				//  If our menu title is not set this will attempt to get the page title of the parent menu and use that instead
				//  So the page title does not have ot be specified
				// @Reference
				//  https://developer.wordpress.org/reference/functions/add_submenu_page/#more-information
				
				$menu_page_title_status = (!empty($menu_page_title)) ? $menu_page_title : get_admin_page_title();
				
			}
			
			// Menu Title
			
			{
				
				// Set our $menu_title_status variable
				// @Example
				//  Theme Settings would become Theme Settings
				
				$default_menu_title = strtoupper($menu_page_title_status);
				
				// Set our $menu_title_status variable
				
				$menu_title_status = (!empty($menu_title)) ? $menu_title : $default_menu_title;
				
			}
			
			// Menu Slug
			
			{
				
				// Set our $menu_slug_status variable
				// @Note
				//  The default submenu slug will be the menu page title lowercase, hyphenated with -submenu appended
				// @Example
				//  Theme Settings would become theme-settings-submenu
				
				$default_menu_slug = str_replace(' ', '-', strtolower("$menu_page_title_status-" . __FUNCTION__));
				
				// Set our $menu_slug_status variable
				
				$menu_slug_status = (!empty($menu_slug)) ? $menu_slug : $default_menu_slug;
				
			}
			
			// Menu Icon
			
			{
				
				// Set our $menu_icon_status variable
				// @Note
				//  The default menu icon will be https://developer.wordpress.org/resource/dashicons/#admin-generic
				
				$default_menu_icon = 'dashicons-admin-generic';
				
				// Set our $menu_icon_status variable
				
				$menu_icon_status = (!empty($menu_icon)) ? $menu_icon : $default_menu_icon;
				
			}
			
			// Set our $menu_capability_status variable
			
			$menu_capability_status = (!empty($menu_capability)) ? $menu_capability : 'edit_theme_options';
			
			// Set our $menu_function_status variable
			
			$menu_function_status = (!empty($menu_function)) ? $menu_function : FALSE;
			
			// Set our various menu variables
			
			$menu_page_title_status = __($menu_page_title_status, $text_domain_status);
			$menu_title_status      = __($menu_title_status,      $text_domain_status);
			$menu_slug_status       = $menu_slug_status;
			$menu_icon_status       = $menu_icon_status;
			$menu_capability_status = $menu_capability_status;
			$menu_function_status   = $menu_function_status;
			
			// Load our Wordpress admin submenu
			
			add_menu_page(
				$menu_page_title_status,
				$menu_title_status,
				$menu_capability_status,
				$menu_slug_status,
				$menu_function_status,
				$menu_icon_status,
				$menu_position
			);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->submenu()
		// @Description
		//  @Todo->description
		// @Usage
		// $menu_title       = 'Theme Settings'; // Menu Title
		// $menu_page_title  = $menu_title;      // Menu Page Title
		// $menu_slug        = 'admin.php?page=custom-theme-theme-settings'; // Menu slug
		// $menu_parent      = 'admin.php?page=custom-theme-theme-settings'; // Menu Parent slug
		// $menu_text_domain = FALSE; // Defaults to our text domain
		// $menu_capability  = FALSE; // Defaults to 'edit_theme_options'
		// $menu_function    = FALSE; // Defaults to NULL
		// $this->submenu($menu_title, $menu_page_title, $menu_slug, $menu_parent, $menu_text_domain, $menu_capability, $menu_function);
		// @Todo->!IMPORTANT_double_check_variable_needs
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_submenu = 1.0;
		public function submenu($menu_title = '', $menu_page_title = '', $menu_slug = '', $menu_parent = '', $text_domain = '', $menu_capability = '', $menu_function = '') {
			
			// @Note
			//  This is the order I think the Wordpress add_submenu_page() should go in
			//  It makes more sense to me in it's order so this is why our $this->sub_menu() has it's $args place accordingly
			// add_submenu_page($menu_title, $menu_page_title, $menu_slug, $menu_parent, $menu_text_domain, $menu_capability, $menu_function);
			
			// Set our $text_domain_status variable
			
			$text_domain_status = (!empty($text_domain)) ? $text_domain : theme()->dev->text_domain();
			
			// Menu Parent
			
			{
				
				// Set our $menu_parent_status variable
				// @Note
				//  If our menu parent is not set this will attempt to get the parent menu and use that instead
				//  So the page parent does not have ot be specified??
				// @Reference
				//  https://developer.wordpress.org/reference/functions/add_submenu_page/#more-information
				// @Todo->!IMPORTANT_TEST_AND_CONFIRM_OR_FIX
				
				$menu_parent_status = (!empty($menu_parent)) ? $menu_parent : get_admin_page_parent();
				
			}
			
			// Menu Page Title
			
			{
				
				// Set our $menu_title_status variable
				// @Note
				//  If our menu title is not set this will attempt to get the page title of the parent menu and use that instead
				//  So the page title does not have ot be specified
				// @Reference
				//  https://developer.wordpress.org/reference/functions/add_submenu_page/#more-information
				
				$menu_page_title_status = (!empty($menu_page_title)) ? $menu_page_title : get_admin_page_title();
				
			}
			
			// Menu Title
			
			{
				
				// Set our $menu_title_status variable
				// @Example
				//  Theme Settings would become Theme Settings
				
				$default_menu_title = strtoupper($menu_page_title_status);
				
				// Set our $menu_title_status variable
				
				$menu_title_status = (!empty($menu_title)) ? $menu_title : $default_menu_title;
				
			}
			
			// Menu Slug
			
			{
				
				// Set our $menu_slug_status variable
				// @Note
				//  The default submenu slug will be the menu page title lowercase, hyphenated with -submenu appended
				// @Example
				//  Theme Settings would become theme-settings-submenu
				
				$default_menu_slug = str_replace(' ', '-', strtolower("$menu_page_title_status-" . __FUNCTION__));
				
				// Set our $menu_slug_status variable
				
				$menu_slug_status = (!empty($menu_slug)) ? $menu_slug : $default_menu_slug;
				
			}
			
			// Set our $menu_capability_status variable
			
			$menu_capability_status = (!empty($menu_capability)) ? $menu_capability : 'edit_theme_options';
			
			// Set our $menu_function_status variable
			
			$menu_function_status = (!empty($menu_function)) ? $menu_function : FALSE;
			
			// Set our various menu variables
			
			$menu_page_title = __($menu_page_title_status, $text_domain_status);
			$menu_title      = __($menu_title_status,      $text_domain_status);
			$menu_slug       = $menu_slug_status;
			$menu_capability = $menu_capability_status;
			$menu_function   = $menu_function_status;
			
			// Load our Wordpress admin submenu
			
			add_submenu_page(
				$menu_parent_status,
				$menu_page_title,
				$menu_title,
				$menu_capability,
				$menu_slug,
				$menu_function
			);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->array_key()
		// @Description
		//  @Todo->description
		// @Usage
		//  @Todo->usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_array_key = 1.0;
		public function array_key($array_key_prefix = '', $func_name = '', $output_method = '') {
			
			// Set our $array_key_status variable
			
			$array_key_status = (!empty($array_key_prefix)) ? $array_key_prefix . '_' .$func_name : $func_name;
			
			// Load our data method
			
			theme()->dev->data($array_key_prefix, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->var_check()
		// @Description
		//  This function 
		// @Reference
		//  https://codex.wordpress.org/Function_Reference/is_wp_error
		// @Todo->consider_adding_dev_report_error_as_well
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_var_check = 1.0;
		public function var_check($var_data = '') {
			
			// If our $var_data variable is not empty and is not a Wordpress error
			
			if (!empty($var_data) and (!is_wp_error($var_data))) {
				
				// return TRUE
				
				return TRUE;
				
			}
			
			// If our $var_data is not empty and has triggered a Wordpress error
			
			elseif (!empty($var_data) and (is_wp_error($var_data))) {
				
				// Set our $error_message variable
				// @Todo->check_if_this_works??_the_var_data_object??
				
				$error_message = $var_data->get_error_message();
				
				// Echo our error message
				
				echo '<div id="message" class="error"><p>' . (!empty($error_message)) . '</p></div>';
				
				// return FALSE
				
				return FALSE;
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->post_type_title()
		// @BETA
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_post_type_title = 1.0;
		public function post_type_title() {
			
			// @Todo->polish_works_in_only_some_conditions_I_think_not_custom_post_types
			
			global $pagenow;
			
			if ($pagenow == 'post-new.php' || $pagenow == 'post.php') {
				
				$post_type_name = get_post_type_object(theme()->global->post_type());
				
				$title = $post_type_name->labels->singular_name;
				
				return "Theme Settings > $title Settings";
				
			}
			
			return "Layout Settings";
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->public_post_types()
		// @Description
		// @Note
		// @Usage
		// @Reference
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_public_post_types = 1.0;
		public function public_post_types($output_method = '') {
			
			$options = array();
			
			$options = get_post_types(
				
				array(
					'_builtin' => TRUE,
					'public'   => TRUE
				)
				
			);
			
			sort($options, SORT_NATURAL);
			
			// Load our data method
			
			theme()->dev->data($options, $output_method);
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->registered_templates()
		// @Description
		// @Note
		// @Usage
		// @Reference
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_registered_templates = 1.0;
		public function registered_templates($path = '') {
			
			$theme = wp_get_theme($output_method = '');
			
			$options = array();
			
			$files = (array) $theme->get_files('php', 1);
			
			foreach ($files as $file => $full_path) {
				
				$headers = @get_file_data($full_path, array('Template Name' => 'Template Name'));
				
				if (empty($headers['Template Name'])) {
					
					$options[$file] = $headers['Template Name'];
					
				}
				
			}
			
			// Load our data method
			
			theme()->dev->data($options, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
