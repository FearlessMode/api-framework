<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Backend_Advanced_Array_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Backend_Advanced_Array_API {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Set our various variables
			
			
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Site
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->option_type_title()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_option_type_title = 1.0;
		public function option_type_title($title = '', $func_name = '', $func_name_exclude = '') {
			
			// Set our title variable
			
			$title = str_replace($func_name_exclude, '', $func_name);
			$title = str_replace('_', ' ', $func_name);
			$title = ucwords($title);
			
			// Load our data method
			
			return (!empty($title)) ? $title : 'Error: Title_Empty';
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->group()
		//  A function that merges our array functions by the group/concept type
		//   This allows more easily to write our array options in our framework
		// @Todo->add_description_add_usage
		// @Usage
		// @Note
		//  This make use of our developer friendly array key prefix usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_group = 1.0;
		public function group($group_type = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Reset our $text_domain variable
			
			$text_domain = theme()->dev->text_domain($text_domain);
			
			// If our group type variable is site
			
			if ($group_type == 'site') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::site_layout(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::background_color(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::site_width(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::content_width(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::content_position(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::desktop_breakpoint(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::mobile_breakpoint(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::phone_breakpoint(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// If our group type variable is header
			
			elseif ($group_type == 'header') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::__header_layout(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::main_header(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::above_header(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::below_header(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::header_width(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::header_background_color(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::header_shadow(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// If our group type variable is body
			
			elseif ($group_type == 'body') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::title(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::subtitle(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::__body_layout(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::above_body(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::below_body(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::left_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::above_left_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::below_left_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::right_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::above_right_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::below_right_sidebar(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// If our group type variable is footer
			
			elseif ($group_type == 'footer') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::main_footer(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::copyright(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::above_footer(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::below_footer(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// If our group type variable is font styles
			
			elseif ($group_type == 'font_styles') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::font_family(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_color(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_size(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_style(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_weight(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// If our group type variable is font styles advanced
			
			elseif ($group_type == 'font_styles_advanced') {
				
				// Set our array variable
				
				$array = array_merge(
					
					self::font_height(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_spacing(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_stretch(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_variant(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_transform(FALSE, $array_key_prefix, $text_domain, $output_method),
					
					self::font_overflow(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_align(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_decoration(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_shadow(FALSE, $array_key_prefix, $text_domain, $output_method),
					self::font_align_last(FALSE, $array_key_prefix, $text_domain, $output_method)
					
				);
				
			}
			
			// Load our data method
			
			// // theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Site
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->site_layout()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_site_layout = 1.0;
		public function site_layout($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					'fullwidth'    => 'Fullwidth',
					'boxed'        => 'Boxed',
					'boxed_shadow' => 'Boxed Shadow',
				),
				
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->background_color()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_background_color = 1.0;
		public function background_color($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'rgba-color-picker',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->site_width()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_site_width = 1.0;
		public function site_width($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->content_width()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_content_width = 1.0;
		public function content_width($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->content_position()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_content_position = 1.0;
		public function content_position($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					'left'    => 'Left',
					'center'  => 'Center',
					'right'   => 'Right',
				)
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->desktop_breakpoint()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_desktop_breakpoint = 1.0;
		public function desktop_breakpoint($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->mobile_breakpoint()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_mobile_breakpoint = 1.0;
		public function mobile_breakpoint($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->phone_breakpoint()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_phone_breakpoint = 1.0;
		public function phone_breakpoint($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Header
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->__header_layout()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func___header_layout = 1.0;
		public function __header_layout($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::select_header_type(FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->main_header()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_main_header = 1.0;
		public function main_header($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_header, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->above_header()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_above_header = 1.0;
		public function above_header($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_header, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->below_header()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_below_header = 1.0;
		public function below_header($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_header, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->header_width()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_header_width = 1.0;
		public function header_width($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->header_background_color()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_header_background_color = 1.0;
		public function header_background_color($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'rgba-color-picker',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->header_shadow()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_header_shadow = 1.0;
		public function header_shadow($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'text',
				'header'  => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'    => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 4,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Body
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->title()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_title = 1.0;
		public function title($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type('theme-title', FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->subtitle()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_subtitle = 1.0;
		public function subtitle($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type('theme-title', FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->__body_layout()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func___body_layout = 1.0;
		public function __body_layout($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'center',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::sidebar_layout(TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->above_body()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_above_body = 1.0;
		public function above_body($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_content, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->below_body()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_below_body = 1.0;
		public function below_body($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_content, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->left_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_left_sidebar = 1.0;
		public function left_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->above_left_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_above_left_sidebar = 1.0;
		public function above_left_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->below_left_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_below_left_sidebar = 1.0;
		public function below_left_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->right_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_right_sidebar = 1.0;
		public function right_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->above_right_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_above_right_sidebar = 1.0;
		public function above_right_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->below_right_sidebar()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_below_right_sidebar = 1.0;
		public function below_right_sidebar($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 12,
					// 'push'       => 6,
					'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_sidebar, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Footer
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->main_footer()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_main_footer = 1.0;
		public function main_footer($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_footer, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->copyright()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_copyright = 1.0;
		public function copyright($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_footer, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->above_footer()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_above_footer = 1.0;
		public function above_footer($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_footer, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->below_footer()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_below_footer = 1.0;
		public function below_footer($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__) => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 3,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				// 'choices' => 
				// $framework::post_type($this->$theme_footer, FALSE, TRUE),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Font->Normal
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_family()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_family = 1.0;
		public function font_family($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'typography-v2',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'center',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'components' => 
				array(
					'family'         => TRUE,
					'weight'         => FALSE,
					'size'           => FALSE,
					'line-height'    => FALSE,
					'letter-spacing' => FALSE,
					'color'          => FALSE,
					'style'          => FALSE,
					'variation'      => FALSE,
					'subset'         => FALSE,
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_color()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_color = 1.0;
		public function font_color($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'rgba-color-picker',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_size()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_size = 1.0;
		public function font_size($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'text',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_style()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_style = 1.0;
		public function font_style($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					'normal'  => 'Normal',
					'italic'  => 'Italic',
					'oblique' => 'Oblique',
					'inherit' => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_weight()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_weight = 1.0;
		public function font_weight($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					'normal'  => 'Normal',
					'inherit' => 'Inherit',
					'bold'    => 'Bold',
					'bolder'  => 'Bolder',
					'lighter' => 'Lighter',
					'100'     => '100',
					'200'     => '200',
					'300'     => '300',
					'400'     => '400',
					'500'     => '500',
					'600'     => '600',
					'700'     => '700',
					'800'     => '800',
					'900'     => '900',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ====================================================================================================
		// 
		// @Section
		//  Font->Normal
		// 
		// ====================================================================================================
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_height()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_height = 1.0;
		public function font_height($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'text',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_spacing()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_spacing = 1.0;
		public function font_spacing($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'text',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'  => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_stretch()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_stretch = 1.0;
		public function font_stretch($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''                => 'Default',
					'normal'          => 'Normal',
					'condensed'       => 'Condensed',
					'semi-condensed'  => 'Semi Condensed',
					'extra-condensed' => 'Extra Condensed',
					'ultra-condensed' => 'Ultra Condensed',
					'expanded'        => 'Expanded',
					'semi-expanded'   => 'Semi Expanded',
					'extra-expanded'  => 'Extra Expanded',
					'ultra-expanded'  => 'Ultra xpanded',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_variant()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_variant = 1.0;
		public function font_variant($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''           => 'Default',
					'normal'     => 'Normal',
					'small-caps' => 'Small Caps',
					'inherit'    => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_transform()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_transform = 1.0;
		public function font_transform($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''           => 'Default',
					'capitalize' => 'Capitalize',
					'uppercase'  => 'Uppercase',
					'lowercase'  => 'Lowercase',
					'initial'    => 'Initial',
					'inherit'    => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_overflow()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_overflow = 1.0;
		public function font_overflow($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''         => 'Default',
					'clip'     => 'Clip',
					'ellipsis' => 'Ellipsis',
					'string'   => 'String',
					'initial'  => 'Initial',
					'inherit'  => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_align()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_align = 1.0;
		public function font_align($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''        => 'Default',
					'left'    => 'Left',
					'center'  => 'Center',
					'right'   => 'Right',
					'justify' => 'Justify',
					'initial' => 'Initial',
					'inherit' => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_decoration()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_decoration = 1.0;
		public function font_decoration($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''             => 'Default',
					'underline'    => 'Underline',
					'overline'     => 'Overline',
					'line-through' => 'Line / Strike Through',
					'initial'      => 'Initial',
					'inherit'      => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_shadow()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_shadow = 1.0;
		public function font_shadow($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'text',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					// 'grid_close' => TRUE,
				),
				'value'   => '',
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font_align_last()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		 
		// @Version
		public $func_font_align_last = 1.0;
		public function font_align_last($title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// Set our array variable
			
			$array = array(theme()->backend->array_key($array_key_prefix, __FUNCTION__, 'font_') => 
			
			array(
				'type'   => 'multi-select',
				'header' => __(self::option_type_title($title, __FUNCTION__, 'font_'), theme()->dev->text_domain($text_domain)),
				'grid'   => 
				array(
					// 'grid_open'  => TRUE,
					// 'pos'        => 'left',
					// 'nest'       => 6,
					'span'       => 20,
					// 'push'       => 6,
					// 'nest'       => 'close',
					'grid_close' => TRUE,
				),
				'limit'   => 1,
				'value'   => '',
				'choices' => 
				array(
					''        => 'Default',
					'auto'    => 'Auto',
					'left'    => 'Left',
					'center'  => 'Center',
					'right'   => 'Right',
					'justify' => 'Justify',
					'start'   => 'Start',
					'end'     => 'End',
					'initial' => 'Initial',
					'inherit' => 'Inherit',
				),
			));
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->font()
		// @Todo->add_description_add_usage
		// @Usage
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_font = 1.0;
		public function font($css_selector = '', $title = '', $array_key_prefix = '', $text_domain = '', $output_method = '') {
			
			// If our title variable is empty
			
			if (empty($title)) {
				
				// Set our title variable
				
				$title = str_replace('_', ' ', $css_selector);
				$title = str_replace('-', ' ', $title);
				$title = ucwords($title);
				
			}
			
			// Set our array variable
			
			$array = array($css_selector . '_tab' => 
			
			array(
				'title'   => __($title, theme()->dev->text_domain($text_domain)),
				'type'    => 'tab',
				'attr'    => array(),
				'options' => 
				array(
					
					// @Note
					//  Stores data in array key "$css_selector" key
					
					$css_selector => array('type' => 'multi', 'label' => FALSE, 'inner-options' => array(
						
						// ----------------------------------------------------------------------------------------------------
						// Normal Tab
						// ----------------------------------------------------------------------------------------------------
						
						'normal' => 
						array(
							'title'   => __('Basic', theme()->dev->text_domain($text_domain)),
							'type'    => 'tab',
							'options' => 
							array(
								
								self::group('font_styles'),
								
							),
						),
						
						// ----------------------------------------------------------------------------------------------------
						// Advanced Tab
						// ----------------------------------------------------------------------------------------------------
						
						'advanced' => 
						array(
							'title'   => __('Advanced', theme()->dev->text_domain($text_domain)),
							'type'    => 'tab',
							'options' => 
							array(
								
								self::group('font_styles_advanced'),
								
							),
						),
						
					),),
					
				),
			),
				
			);
			
			// Load our data method
			
			// theme()->dev->data($array, $output_method);
			return $array;
			
		}
		
	}
	
}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
