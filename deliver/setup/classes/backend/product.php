<?php

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// ----------------------------------------------------------------------------------------------------
	// Create our CustomTheme_Backend_Product_API class
	// ----------------------------------------------------------------------------------------------------
	
	class CustomTheme_Backend_Product_API extends CustomTheme {
		
		// ----------------------------------------------------------------------------------------------------
		// Set our visibility for our various variables and their defaults
		// ----------------------------------------------------------------------------------------------------
		
		public $is_white_label;
		public $is_developer;
		
		// ----------------------------------------------------------------------------------------------------
		// __construct()
		// @Description
		//  This function handles auto loading any methods, variables and constructors upon class instantiation
		// @Reference
		//   @ http://php.net/manual/en/language.oop5.decon.php
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_construct = 1.0;
		public function __construct() {
			
			// Load our parent constructor
			
			parent::__construct();
			
			// Set our various variables
			// @Todo->!IMPORTANT_test_and_update_product_variables_to_their_true_methods_like_below_them_shows
			
			// @Usage
			// if (product()->is_developer) {}
			
			$this->is_developer = TRUE;
			// $this->is_developer = $this->status('is_developer');
			
			// @Usage
			// if (product()->is_white_label) {}
			
			$this->is_white_label = TRUE;
			// $this->is_white_label = $this->status('is_white_label');
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// ->initialize()
		// @Description
		//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
		// ----------------------------------------------------------------------------------------------------
		
		// @Version
		public $func_initialize = 3.0;
		public function initialize($output_method = '') {
			
			// Create our global variable for easy SDK access.
			
			global $this_product_status;
			
			// If our $this_product_status variable is not set
			
			if (!isset($this_product_status)) {
				
				// Load our Product SDK.
				
				require_once($this->load_product);
				
				// Set our various product developer filter variables
				// $Hook->Filter->CustomTheme-product-has_premium
				// $Hook->Filter->CustomTheme-product-has_addons
				// $Hook->Filter->CustomTheme-product-has_paid_plans
				// $Hook->Filter->CustomTheme-product-has_support
				// @Todo->consider_making_more_filters_for_product__options_ie_menu_settings_slugs_etc
				
				$is_premium     = theme()->hook->filter($this, 'product-is_premium',     FALSE);
				$has_premium    = theme()->hook->filter($this, 'product-has_premium',    TRUE);
				$has_addons     = theme()->hook->filter($this, 'product-has_addons',     TRUE);
				$has_paid_plans = theme()->hook->filter($this, 'product-has_paid_plans', TRUE);
				$has_support    = theme()->hook->filter($this, 'product-has_support',    TRUE);
				$has_wp_org     = theme()->hook->filter($this, 'product-has_wp_org',     FALSE);
				$trial_days     = theme()->hook->filter($this, 'product-trial_days',     90);
				
				// Set our $this_product_status variable
				
				$this_product_status = fs_dynamic_init(
				  
					array(
						'id'                  => '1568',
						'slug'                => '-custom-theme',
						'type'                => 'plugin',
						'public_key'          => 'pk_2b531f497fff828da1b700477c54f',
						'is_premium'          => $is_premium,
						'has_premium_version' => $has_premium,
						'has_addons'          => $has_addons,
						'has_paid_plans'      => $has_paid_plans,
						'is_org_compliant'    => FALSE,
						'has_affiliation'     => 'selected',
						'trial' => 
						array(
							'days'               => $trial_days,
							'is_require_payment' => FALSE,
						),
						'menu' => 
						array(
              'slug'       => 'custom-theme-theme-settings',
              // 'first-path' => 'admin.php?page=custom-theme-theme-settings',
              'support'    => TRUE,
						),
						// @Todo->!IMPORTANT_remove_for_official_production
						'secret_key' => 'sk_-_${0[~N@a$I9]yb96*eUKw4k>}]Z',
					)
					
				);
				
			}
			
			// Load our data method
			
			theme()->dev->data($this_product_status, $output_method);
			
		}
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
