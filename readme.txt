=== * Custom Theme - Core Framework ===

Requires at least: 4.5
Tested up to: 5.0
Requires PHP: 5.6
Stable tag: trunk

** This is required for your custom theme to work. This is your theme\'s framework which holds your theme\'s features and settings. This will automatically be deactivated when your custom theme is not activated. When this is deactivated and your custom theme is activated your site will display a maintenance mode page. When this is first activated and your custom theme is activated a maintenance mode page will display until you save your desired settings for your website.

== Frequently Asked Questions ==

See https://custom-theme.com/faq for more frequently asked questions.

== Changelog ==

1.0 - Initial release.