<?php

// Set our namespace name

namespace CustomTheme;

// If Wordpress is defined as running

if (defined('ABSPATH')) {
	
	// Set our various class variables
	
	$this_class = 'CustomTheme_Plugin';
	
	// If our $this_class variable does not exist
	
	if (!class_exists($this_class)) {
		
		// ----------------------------------------------------------------------------------------------------
		// Create our CustomTheme_Plugin class
		// ----------------------------------------------------------------------------------------------------
		
		class CustomTheme_Plugin {
			
			// ----------------------------------------------------------------------------------------------------
			// ::__construct()
			// @Description
			//  This function is our constructor function that handles auto loading any
			//   functions, variables and constructors upon class instantiation
			// @Reference
			//   http://php.net/manual/en/language.oop5.decon.php
			// ----------------------------------------------------------------------------------------------------
			
			// @Version
			public $func_construct = 1.0;
			public function __construct() {
				
				// Load our theme setup into the Wordpress plugins_loaded action
				
				add_action('plugins_loaded', array($this, 'initialize'), 9999999999);
				
			}
			
			// ----------------------------------------------------------------------------------------------------
			// ->initialize()
			// @Description
			//  This function is for loading any methods we need upon class instantiation including any Wordpress actions
			// ----------------------------------------------------------------------------------------------------
			
			// @Version
			public $func_initialize = 1.0;
			public function initialize() {
				
				// Load our _validate file
				
				require_once('_validate.php');
				
			}
			
		}
		
		// ----------------------------------------------------------------------------------------------------
		// Instantiate our $this_class
		// ----------------------------------------------------------------------------------------------------
		
		new $this_class();
		
	}

}

// If Wordpress is not defined as running stop code execution and throw a 403 Forbidden status

else { exit(header('HTTP/1.0 403 Forbidden')); }
